using System;
using System.Collections;
using System.Collections.Generic;
using _Game.Scripts.Utils;
using _Game.Scripts.Utils.UI_Utils;
using UnityEngine;

public class UIGridLegend : MonoBehaviour
{
    private readonly LazyObjectFinder<GridManager> _gridReference = new LazyObjectFinder<GridManager>();

    [SerializeField] private UIFloatValueTextSetter minorGridTextSetter;
    [SerializeField] private UIFloatValueTextSetter majorGridTextSetter;

    private void Start()
    {
        minorGridTextSetter.SetReference(_gridReference.Get().MinorGridIncrement, "");
        majorGridTextSetter.SetReference(_gridReference.Get().MajorGridIncrement, "");
    }
}
