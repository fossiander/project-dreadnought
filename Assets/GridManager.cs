using System;
using System.Collections;
using System.Collections.Generic;
using _Game.Scripts.Utils.UI_Utils;
using Shapes;
using TMPro;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    [SerializeField] private int numberOfGridlines = 30;
    [SerializeField] private int frequencyOfMajorLines = 5;
    [SerializeField] private float baseGridIncrement = 1f;
    [SerializeField] private float baseOrthographicSize = 10f;
    [SerializeField] private float gridDifferenceMultiple = 5f;
    [SerializeField] private Color lineColor = Color.white;
    [SerializeField] private float majorLineWidth = 3f;
    [SerializeField] private float minorLineWidth = 1f;

    private Camera _mainCamera;
    private Transform _mainCameraTransform; 
        
    private float _currentOrthographicSize;
    private float _currentGridIncrement;

    public EventVariable<float> MinorGridIncrement { get; }= new EventVariable<float>();
    public EventVariable<float> MajorGridIncrement { get; }= new EventVariable<float>();

    private Vector2 _gridCenter;

    private readonly LinkedList<GridLine> _horizontalLines = new LinkedList<GridLine>();
    private readonly LinkedList<GridLine> _verticalLines = new LinkedList<GridLine>();

    private float _minValueX;
    private float _maxValueX;
    private float _minValueY;
    private float _maxValueY;
    
    private int _indexOffset;
    private float _valueOffset;

    private void Awake()
    {
        _mainCamera = Camera.main;
        _mainCameraTransform = _mainCamera.transform;
        _currentOrthographicSize = _mainCamera.orthographicSize;
        _currentGridIncrement = baseGridIncrement;

        _indexOffset = numberOfGridlines / 2;
        _valueOffset = _indexOffset * _currentGridIncrement;

        UpdatePublicGridIncrementVariables();
        SpawnGrid();
    }

    private void LateUpdate()
    {
        RecalculateGridScale();
        RecalculateMinMaxPositions();
        ApplyGridLineShifts();
        UpdateAllGridLinePositions();
    }

    private void RecalculateGridScale()
    {
        if (Mathf.Abs(_mainCamera.orthographicSize - _currentOrthographicSize) > float.Epsilon)
        {
            float newGridIncrement = CalculateGridIncrement();
            if (Mathf.Abs(newGridIncrement - _currentGridIncrement) > float.Epsilon)
            {
                _currentOrthographicSize = newGridIncrement / _currentGridIncrement * baseOrthographicSize;
                _currentGridIncrement = newGridIncrement;
                _valueOffset = _indexOffset * _currentGridIncrement;

                UpdatePublicGridIncrementVariables();
                UpdateAllGridLineValues();
                UpdateAllGridLinePositions();
            }
        }
    }

    private void UpdatePublicGridIncrementVariables()
    {
        MinorGridIncrement.Value = _currentGridIncrement;
        MajorGridIncrement.Value = _currentGridIncrement * frequencyOfMajorLines;
    }

    private void RecalculateMinMaxPositions()
    {
        float offsetWithBuffer = _valueOffset + _currentGridIncrement;
        
        _minValueX = _mainCameraTransform.position.x - offsetWithBuffer;
        _maxValueX = _mainCameraTransform.position.x + offsetWithBuffer;
        _minValueY = _mainCameraTransform.position.y - offsetWithBuffer;
        _maxValueY = _mainCameraTransform.position.y + offsetWithBuffer;
    }

    private void ApplyGridLineShifts()
    {
        int count = 0;
        while (_verticalLines.Last.Value.value >= _maxValueX)
        {
            GridLine shiftedLine = ShiftGridLine(_verticalLines, false);
            shiftedLine.UpdateValue(_indexOffset, _currentGridIncrement);
            
            count++;

            if (count > 100)
            {
                Debug.LogError("Stopped because of endless loop");
                break;
            } 
        }
        
        while (_verticalLines.First.Value.value <= _minValueX)
        {
            GridLine shiftedLine = ShiftGridLine(_verticalLines, true);
            shiftedLine.UpdateValue(_indexOffset, _currentGridIncrement);
            count++;

            if (count > 100)
            {
                Debug.LogError("Stopped because of endless loop");
                break;
            } 
        }
        
        while (_horizontalLines.Last.Value.value >= _maxValueY)
        {
            GridLine shiftedLine = ShiftGridLine(_horizontalLines, false);
            shiftedLine.UpdateValue(_indexOffset, _currentGridIncrement);
            count++;

            if (count > 100)
            {
                Debug.LogError("Stopped because of endless loop");
                break;
            } 
        }
        
        while (_horizontalLines.First.Value.value <= _minValueY)
        {
            GridLine shiftedLine = ShiftGridLine(_horizontalLines, true);
            shiftedLine.UpdateValue(_indexOffset, _currentGridIncrement);
            count++;

            if (count > 100)
            {
                Debug.LogError("Stopped because of endless loop");
                break;
            } 
        }
        
    }
    
    private void SpawnGrid()
    {
        for (int i = 0; i < numberOfGridlines; i++)
        {
            Line line = new GameObject($"Horizontal Line {i}").AddComponent<Line>();
            line.transform.SetParent(transform, false);
            
            line.Color = lineColor;
            line.ThicknessSpace = ThicknessSpace.Pixels;
            line.Thickness = i % frequencyOfMajorLines == 0 ? majorLineWidth : minorLineWidth;
            GridLine gridLine = new GridLine(GridLine.GridLineType.Horizontal, i, line);
            gridLine.UpdateValue(_indexOffset, _currentGridIncrement);
            gridLine.UpdateLinePosition(_valueOffset, _mainCameraTransform.position.x);
            _horizontalLines.AddLast(gridLine);
        }
        
        for (int i = 0; i < numberOfGridlines; i++)
        {
            Line line = new GameObject($"Vertical Line {i}").AddComponent<Line>();
            line.transform.SetParent(transform, false);
            
            line.Color = lineColor;
            line.ThicknessSpace = ThicknessSpace.Pixels;
            line.Thickness = i % frequencyOfMajorLines == 0 ? majorLineWidth : minorLineWidth;
            GridLine gridLine = new GridLine(GridLine.GridLineType.Vertical, i, line);
            gridLine.UpdateValue(_indexOffset, _currentGridIncrement);
            gridLine.UpdateLinePosition(_valueOffset, _mainCameraTransform.position.y);
            _verticalLines.AddLast(gridLine);
        }
    }

    private void UpdateAllGridLineValues()
    {
        foreach (GridLine gridLine in _horizontalLines)
        {
            gridLine.UpdateValue(_indexOffset, _currentGridIncrement);
        }

        foreach (GridLine gridLine in _verticalLines)
        {
            gridLine.UpdateValue(_indexOffset, _currentGridIncrement);
        }
    }

    private void UpdateAllGridLinePositions()
    {
        foreach (GridLine gridLine in _horizontalLines)
        {
            gridLine.UpdateLinePosition(_valueOffset,  _mainCameraTransform.position.x);
        }

        foreach (GridLine gridLine in _verticalLines)
        {
            gridLine.UpdateLinePosition(_valueOffset, _mainCameraTransform.position.y);
        }
    }

    private float CalculateGridIncrement()
    {
        float orthographicRatio = _mainCamera.orthographicSize / baseOrthographicSize;
        float gridScalar = Mathf.Pow(gridDifferenceMultiple, Mathf.Round(Mathf.Log(orthographicRatio, gridDifferenceMultiple)));
        return gridScalar * baseGridIncrement;
    }

    private static GridLine ShiftGridLine(LinkedList<GridLine> lineList, bool moveUp)
    {
        GridLine lowestLine = lineList.First.Value;
        GridLine highestLine = lineList.Last.Value;
        
        switch (moveUp)
        {
            case true:
                lowestLine.index = highestLine.index + 1;
                lineList.RemoveFirst();
                lineList.AddLast(lowestLine);
            
                return lowestLine;
            case false:
                highestLine.index = lowestLine.index - 1;
                lineList.RemoveLast();
                lineList.AddFirst(highestLine);
            
                return highestLine;
        }
    }

    class GridLine
    {
        public readonly GridLineType lineType;
        public int index;
        public float value = float.NaN;
        public readonly Line line;

        public GridLine(GridLineType lineType, int index, Line line)
        {
            this.lineType = lineType;
            this.index = index;
            this.line = line;
        }

        public void UpdateValue(int indexOffset, float valueIncrement)
        {
            value = (index - indexOffset) * valueIncrement;
        }

        public void UpdateLinePosition(float valueOffset, float centerOffset)
        {
            if (lineType == GridLineType.Horizontal)
            {
                line.Start = new Vector3(centerOffset -valueOffset, value, 0);
                line.End = new Vector3(centerOffset + valueOffset, value, 0);
            }
            else
            {
                line.Start = new Vector3(value,centerOffset -valueOffset,  0);
                line.End = new Vector3(value, centerOffset + valueOffset, 0);
            }
        }

        public enum GridLineType
        {
            Horizontal,
            Vertical
        }
    }
}
