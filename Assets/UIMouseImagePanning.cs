using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMouseImagePanning : MonoBehaviour
{
    [SerializeField] private RectTransform rectTransform;
    [SerializeField] private Vector2 maxPanning;
    [SerializeField] private AnimationCurve panningCurve = AnimationCurve.Linear(0,0,1,1);

    private Vector2 _startingPosition;
    
    private void Start()
    {
        _startingPosition = rectTransform.localPosition;
    }

    private void LateUpdate()
    {
        UpdatePan();
    }

    private void UpdatePan()
    {
        Vector2 mouseOffset = 2 * new Vector2(
            Mathf.Clamp01(Input.mousePosition.x / Screen.width),
            Mathf.Clamp01(Input.mousePosition.y / Screen.height)) - Vector2.one;

        Vector2 panAmount = -new Vector2(
            Mathf.Sign(mouseOffset.x) * panningCurve.Evaluate(Mathf.Abs(mouseOffset.x)),
            Mathf.Sign(mouseOffset.y) * panningCurve.Evaluate(Mathf.Abs(mouseOffset.y)));

        rectTransform.transform.localPosition = _startingPosition + maxPanning * panAmount;
    }
}
