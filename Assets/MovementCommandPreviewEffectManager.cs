using System;
using System.Collections;
using System.Collections.Generic;
using BattleMode.Control;
using UnityEngine;

public class MovementCommandPreviewEffectManager : MonoBehaviour
{
    [SerializeField] private MovementPreviewEffect movementPreviewEffectPrefab = null;

    private MovementPreviewEffect _previewEffect;

    private void Awake()
    {
        _previewEffect = Instantiate(movementPreviewEffectPrefab, transform);
    }

    private void OnDestroy()
    {
        Destroy(_previewEffect.gameObject);
    }

    private void OnEnable()
    {
        MovementCommandDragStarted.AddListener(OnStartDrag);
        MovementCommandDragging.AddListener(OnDragging);
        MovementCommandDragCompleted.AddListener(OnCompleteDrag);
    }

    private void OnDisable()
    {
        MovementCommandDragStarted.RemoveListener(OnStartDrag);
        MovementCommandDragging.RemoveListener(OnDragging);
        MovementCommandDragCompleted.RemoveListener(OnCompleteDrag);
    }

    private void OnStartDrag(MovementCommandDragStarted eventParams)
    {
        _previewEffect.Show(eventParams.currentPosition, eventParams.movePosition);
    }

    private void OnDragging(MovementCommandDragging eventParams)
    {
        _previewEffect.UpdateValues(eventParams.currentPosition, eventParams.movePosition, eventParams.velocityPosition, eventParams.targetVelocity);
    }

    private void OnCompleteDrag(MovementCommandDragCompleted eventParams)
    {
        _previewEffect.Hide();
    }
}
