using System;
using System.Collections;
using System.Collections.Generic;
using BattleMode.Control;
using BattleMode.UnitConversion;
using UnityEngine;

public class MovementPreviewEffect : MonoBehaviour
{
    [SerializeField] private Transform movePoint;
    [SerializeField] private Transform velocityPoint;
    [SerializeField] private LineRenderer movementLine;

    public void Show(Vector2 startPosition, Vector2 movePosition)
    {
        gameObject.SetActive(true);
        UpdateValues(startPosition, movePosition, Vector2.zero, null);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
    
    public void UpdateValues(Vector2 startPosition, Vector2 movePosition, Vector2 velocityPosition, VelocityAmount? velocityAmount)
    {
        gameObject.SetActive(true);
        
        movementLine.SetPosition(0, startPosition);
        movementLine.SetPosition(1, movePosition);
        movementLine.SetPosition(2, velocityPosition);

        movePoint.position = movePosition;
        velocityPoint.position = velocityPosition;
    }
}
