﻿using System;
using _Game.Scripts.Utils;
using UnityEngine;
using UnityEngine.Pool;

namespace BattleMode.Effects
{
    public abstract class EffectSpawner : MonoBehaviour
    {
        [SerializeField] private Transform effectParent = null;
        [SerializeField] private Effect effectPrefab = null;

        [Header("Pooling")]
        [SerializeField] private int defaultPoolSize = 10;
        [SerializeField] private int maxPoolSize = 100;
        
        private SimpleComponentPool<Effect> _effectPool;
        
        private void Awake()
        {
            _effectPool = new SimpleComponentPool<Effect>(effectPrefab, effectParent, defaultPoolSize, maxPoolSize);
        }

        protected Effect SpawnEffect(Vector2 position, Vector2 direction, float scale = 1)
        {
            if (effectPrefab == null)
            {
                Debug.LogError($"No effect prefab set; cannot spawn effect for {GetType()}!");
                return null;
            }
            
            Effect effect = _effectPool.Get();
            effect.transform.position = position;
            effect.transform.forward = direction;
            effect.transform.localScale = Vector3.one * scale;
            
            effect.SetAction(() => _effectPool.Release(effect));

            return effect;
        }

        private void OnValidate()
        {
            if (effectParent == null) effectParent = transform;
        }
    }
}