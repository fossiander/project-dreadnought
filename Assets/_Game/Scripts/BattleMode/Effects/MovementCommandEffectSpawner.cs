using BattleMode.Combat;
using BattleMode.Movement;
using UnityEngine;

namespace BattleMode.Effects
{
    public class MovementCommandEffectSpawner : EffectSpawner
    {
        private void OnEnable()
        {
            MovementCommandReceivedEvent.AddListener(SpawnHitEffect);
        }

        private void OnDisable()
        {
            MovementCommandReceivedEvent.RemoveListener(SpawnHitEffect);
        }

        private void SpawnHitEffect(MovementCommandReceivedEvent eventParams)
        {
            SpawnEffect(eventParams.position, Vector2.up,1f);
        }
    }
}
