﻿using _Game.Scripts.Utils;
using BattleMode.Combat;
using UnityEngine;

namespace BattleMode.Effects
{
    public class ShipDestroyedEffectSpawner : EffectSpawner
    {
        [SerializeField] private SizeScaler sizeScaler;
        
        private void OnEnable()
        {
            ShipDestroyedEvent.AddListener(SpawnExplosion);
        }

        private void OnDisable()
        {
            ShipDestroyedEvent.RemoveListener(SpawnExplosion);
        }

        private void SpawnExplosion(ShipDestroyedEvent eventParams)
        {
            SpawnEffect(eventParams.position, Vector2.up, sizeScaler.GetOutput(eventParams.shipTotalHullPoints));
        }
    }
}