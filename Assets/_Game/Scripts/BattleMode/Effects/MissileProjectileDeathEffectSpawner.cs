using System;
using _Game.Scripts.Utils;
using BattleMode.Combat.Projectiles;
using UnityEngine;

namespace BattleMode.Effects
{
    public class MissileProjectileDeathEffectSpawner : EffectSpawner
    {
        [SerializeField] private SizeScaler sizeScaler;
        
        private void OnEnable()
        {
            MissileDestroyedEvent.AddListener(SpawnExplosion);
        }

        private void OnDisable()
        {
            MissileDestroyedEvent.RemoveListener(SpawnExplosion);
        }

        private void SpawnExplosion(MissileDestroyedEvent eventParams)
        {
            SpawnEffect(eventParams.position, Vector2.up, sizeScaler.GetOutput(eventParams.damageRemoved));
        }
    }
}
