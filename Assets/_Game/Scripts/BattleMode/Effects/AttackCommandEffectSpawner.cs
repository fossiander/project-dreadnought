using BattleMode.Combat;
using UnityEngine;

namespace BattleMode.Effects
{
    public class AttackCommandEffectSpawner : EffectSpawner
    {
        private void OnEnable()
        {
            AttackCommandReceivedEvent.AddListener(SpawnHitEffect);
        }

        private void OnDisable()
        {
            AttackCommandReceivedEvent.RemoveListener(SpawnHitEffect);
        }

        private void SpawnHitEffect(AttackCommandReceivedEvent eventParams)
        {
            SpawnEffect(eventParams.position, Vector2.up,1f);
        }
    }
}
