using System.Collections.Generic;
using _Game.Scripts.Utils;
using _Game.Scripts.Utils.Audio_Utils;
using BattleMode.Combat.Projectiles;
using BattleMode.Combat.Weapons;
using UnityEngine;

namespace BattleMode.Effects
{
    public class WeaponFiredEffectSpawner : EffectSpawner
    {
        private void OnEnable()
        {
            WeaponFiredEvent.AddListener(SpawnWeaponFiredEffect);
        }

        private void OnDisable()
        {
            WeaponFiredEvent.RemoveListener(SpawnWeaponFiredEffect);
        }

        private void SpawnWeaponFiredEffect(WeaponFiredEvent eventParams)
        {
            Dictionary<AudioSound, int> shotsBySound = new Dictionary<AudioSound, int>();

            foreach (KeyValuePair<WeaponClass,int> weaponAttacks in eventParams.shotsByWeaponClass)
            {
                AudioSound weaponFiredSound = weaponAttacks.Key.weaponFireAudioSound;
                
                if (weaponFiredSound == null) continue;

                if (!shotsBySound.ContainsKey(weaponFiredSound)) shotsBySound.Add(weaponFiredSound, weaponAttacks.Value);
                else shotsBySound[weaponFiredSound] += weaponAttacks.Value;
            }

            foreach (KeyValuePair<AudioSound,int> soundCount in shotsBySound)
            {
                Effect effect = SpawnEffect(eventParams.startPosition, Vector2.up,1f);
                if (effect.TryGetComponent(out AudioSoundTrigger audioSoundTrigger))
                {
                    audioSoundTrigger.SetAudioSound(soundCount.Key);
                    audioSoundTrigger.PlaySound(soundCount.Value);
                }
            }
        }
    }
}
