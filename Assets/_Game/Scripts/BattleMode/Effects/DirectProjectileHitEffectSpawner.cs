using _Game.Scripts.Utils;
using BattleMode.Combat.Projectiles;
using UnityEngine;

namespace BattleMode.Effects
{
    public class DirectProjectileHitEffectSpawner : EffectSpawner
    {
        [SerializeField] private SizeScaler sizeScaler;
        
        private void OnEnable()
        {
            DirectProjectileHitEvent.AddListener(SpawnHitEffect);
        }

        private void OnDisable()
        {
            DirectProjectileHitEvent.RemoveListener(SpawnHitEffect);
        }

        private void SpawnHitEffect(DirectProjectileHitEvent eventParams)
        {
            SpawnEffect(eventParams.position, eventParams.direction,sizeScaler.GetOutput(eventParams.damageDone));
        }
    }
}
