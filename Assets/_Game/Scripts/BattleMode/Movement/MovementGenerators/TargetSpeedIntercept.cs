﻿using System.Linq;
using _Game.Scripts.Utils;
using BattleMode.Movement.Movements;
using UnityEngine;

namespace BattleMode.Movement.MovementGenerators
{
    public class TargetSpeedIntercept : IMovementGenerator
    {
        public void TriggerMovement(MovableEntity movableEntity, Vector2 targetPosition, Vector2 targetVelocity, float acceleration, bool continueOnPath = false)
        {
            movableEntity.SetMovementList(GenerateMovement(
                movableEntity, 
                movableEntity.Position.Value,
                movableEntity.Velocity.Value,
                targetPosition,
                targetVelocity,
                acceleration,
                continueOnPath));
        }
        
        public IMovement[] GenerateMovement(MovableEntity movableEntity, Vector2 startPosition, Vector2 startVelocity, Vector2 targetPosition, Vector2 targetVelocity, float acceleration, bool continueOnPath = false)
        {
            SimInput simInput = new SimInput()
            {
                startPosition = startPosition,
                startVelocity = startVelocity,
                targetPosition = targetPosition,
                targetVelocity = targetVelocity,
                acceleration = acceleration
            };

            float lowerBoundDuration = CalcLowerBoundDuration(startPosition, startVelocity, targetPosition, targetVelocity, acceleration);
            float upperBoundDuration = CalcUpperBoundDuration(startPosition, startVelocity, targetPosition, targetVelocity, acceleration);
            
            SimOutput simOutputEstimate = IntervalBisection.Estimate<SimInput,SimOutput>(
                simInput, 
                RunSimulation, 
                lowerBoundDuration, 
                upperBoundDuration,
                IntervalBisection.ErrorWeightedIntersection);

            //Ensures that target velocity and position are matched perfectly
            SimOutput simOutput = CalculateFinalOutput(simInput, simOutputEstimate);
            
            IMovement[] movementArray =
            {
                new ConstantAcceleration(movableEntity, simOutput.accelerationVector1, simOutput.duration1),
                new ConstantAcceleration(movableEntity, simOutput.accelerationVector2, simOutput.duration2)
            };

            if (continueOnPath)
            {
                movementArray = movementArray.Append(new ConstantVelocity(movableEntity, Mathf.Infinity)).ToArray();
            }

            return movementArray;
        }

        private SimOutput CalculateFinalOutput(SimInput simInput, SimOutput simOutputEstimate)
        {
            float duration1 = simOutputEstimate.duration1 / (simOutputEstimate.duration1 + simOutputEstimate.duration2) * simOutputEstimate.totalDuration;
            float duration2 = simOutputEstimate.duration2 / (simOutputEstimate.duration1 + simOutputEstimate.duration2) * simOutputEstimate.totalDuration;
            float totalDuration = duration1 + duration2;
            
            Vector2 targetMovement = simInput.targetPosition - simInput.startPosition;

            Vector2 accelerationVector = CalculateFinalAcceleration(targetMovement, simInput.startVelocity, simInput.targetVelocity, duration1, duration2);
            Vector2 decelerationVector = CalculateFinalDeceleration(targetMovement, simInput.startVelocity, simInput.targetVelocity, duration1, duration2);

            return new SimOutput()
            {
                totalDuration = totalDuration,
                duration1 = duration1,
                duration2 = duration2,
                accelerationVector1 = accelerationVector,
                accelerationVector2 = decelerationVector
            };
        }

        private Vector2 CalculateFinalAcceleration(Vector2 distance, Vector2 velocity1, Vector2 velocity2, float duration1, float duration2)
        {
            return (-2 * duration1 * velocity1 - duration2 * (velocity1 + velocity2) + 2 * distance) /
                   (duration1 * (duration1 + duration2));
        }

        private Vector2 CalculateFinalDeceleration(Vector2 distance, Vector2 velocity1, Vector2 velocity2, float duration1, float duration2)
        {
            return (2 * duration2 * velocity2 + duration1 * (velocity1 + velocity2) - 2 * distance) /
                   (duration2 * (duration1 + duration2));
        }
        
        private static float RunSimulation(float duration, SimInput simInput, out SimOutput simOutput)
        {
            Vector2 targetMovement = simInput.targetPosition - simInput.startPosition;

            Vector2 accelerationForVelocityChange = (simInput.targetVelocity - simInput.startVelocity) / duration;
            if (accelerationForVelocityChange.magnitude >= simInput.acceleration)
            {
                simOutput.totalDuration = duration;
                simOutput.duration1 = Mathf.Infinity;
                simOutput.duration2 = Mathf.Infinity;
                simOutput.accelerationVector1 = Vector2.zero;
                simOutput.accelerationVector2 = Vector2.zero;

                return Mathf.NegativeInfinity;
            }
            
            Vector2 movementDuringVelocityChange = simInput.startVelocity * duration + 0.5f * duration * duration * accelerationForVelocityChange;
            Vector2 requiredAdditionalMovement = targetMovement - movementDuringVelocityChange;
            float additionalMovementMagnitude = requiredAdditionalMovement.magnitude;
            
            Vector2 residualAccelerationVector = VectorUtils.GetResidualAccelerationVector(requiredAdditionalMovement, accelerationForVelocityChange, simInput.acceleration, true);
            Vector2 residualDecelerationVector = VectorUtils.GetResidualAccelerationVector(requiredAdditionalMovement, accelerationForVelocityChange, simInput.acceleration, false);

            float aVectorMagnitude = residualAccelerationVector.magnitude;
            float dVectorMagnitude = residualDecelerationVector.magnitude;

            float accelerationDuration = CalculateAccelerationDuration(additionalMovementMagnitude, aVectorMagnitude, dVectorMagnitude);
            float decelerationDuration = CalculateDecelerationDuration(additionalMovementMagnitude, aVectorMagnitude, dVectorMagnitude);

            float durationError = duration - accelerationDuration - decelerationDuration;

            simOutput.totalDuration = duration;
            simOutput.duration1 = accelerationDuration;
            simOutput.duration2 = decelerationDuration;
            simOutput.accelerationVector1 = accelerationForVelocityChange + residualAccelerationVector;
            simOutput.accelerationVector2 = accelerationForVelocityChange + residualDecelerationVector;

            return durationError;
        }

        
        private static float CalculateAccelerationDuration(float distance, float acceleration, float deceleration)
        {
            return Mathf.Sqrt(2 * deceleration * distance / (acceleration * (acceleration + deceleration)));
        }
        
        private static float CalculateDecelerationDuration(float distance, float acceleration, float deceleration)
        {
            return CalculateAccelerationDuration(distance, deceleration, acceleration);
        }

        private static float CalcLowerBoundDuration(Vector2 startPosition, Vector2 startVelocity, Vector2 targetPosition, Vector2 targetVelocity, float acceleration)
        {
            //Assumes all speed orthogonal to target movement is removed immediately
            Vector2 targetMovement = targetPosition - startPosition;

            float originalSpeedInDirectionOfTarget = Vector2.Dot(startVelocity, targetMovement.normalized);
            float finalSpeedInDirectionOfTarget = Vector2.Dot(targetVelocity, targetMovement.normalized);
            float targetMovementDistance = targetMovement.magnitude;

            float component1 = Mathf.Sqrt(2 * (2 * acceleration * targetMovementDistance +
                                               originalSpeedInDirectionOfTarget * originalSpeedInDirectionOfTarget +
                                               finalSpeedInDirectionOfTarget * finalSpeedInDirectionOfTarget)) / acceleration;
            float component2 = (originalSpeedInDirectionOfTarget + finalSpeedInDirectionOfTarget) / acceleration;

            float timeToReachTarget = component1 - component2;

            return Mathf.Max(timeToReachTarget, Mathf.Epsilon);
        }

        private static float CalcUpperBoundDuration(Vector2 startPosition, Vector2 startVelocity, Vector2 targetPosition, Vector2 targetVelocity, float acceleration)
        {
            //Assumes full break before accelerating and decelerating, the stopping and accelerating to reach velocity and target
            float timeToStop = startVelocity.magnitude / acceleration;
            Vector2 locationAfterStopping = startPosition + 0.5f * acceleration * timeToStop * timeToStop * startVelocity.normalized;
            float timeToReachFinalVelocity = targetVelocity.magnitude / acceleration;
            Vector2 locationBeforeReaccelerating = targetPosition - 0.5f * acceleration * timeToReachFinalVelocity * timeToReachFinalVelocity * targetVelocity.normalized;

            Vector2 targetMovement = locationBeforeReaccelerating - locationAfterStopping;
            float targetMovementDistance = targetMovement.magnitude;

            float timeToAccelerateAndDecelerate = 2 * Mathf.Sqrt(targetMovementDistance / acceleration);

            return timeToStop + timeToAccelerateAndDecelerate + timeToReachFinalVelocity;
        }

        private struct SimOutput
        {
            public float totalDuration;
            public Vector2 accelerationVector1;
            public float duration1;
            public Vector2 accelerationVector2;
            public float duration2;
        }

        private struct SimInput
        {
            public Vector2 startPosition;
            public Vector2 startVelocity;
            public Vector2 targetPosition;
            public Vector2 targetVelocity;
            public float acceleration;
        }
    }
}