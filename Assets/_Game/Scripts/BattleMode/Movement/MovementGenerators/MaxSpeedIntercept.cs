﻿using System.Linq;
using _Game.Scripts.Utils;
using BattleMode.Movement.Movements;
using UnityEngine;

namespace BattleMode.Movement.MovementGenerators
{
    public class MaxSpeedIntercept : IMovementGenerator
    {
        public void TriggerMovement(MovableEntity movableEntity, Vector2 targetPosition, Vector2 targetVelocity, float acceleration, bool continueOnPath = false)
        {
            movableEntity.SetMovementList(GenerateMovement(
                    movableEntity, 
                    movableEntity.Position.Value,
                    movableEntity.Velocity.Value,
                    targetPosition,
                    targetVelocity,
                    acceleration,
                    continueOnPath));
        }

        public IMovement[] GenerateMovement(MovableEntity movableEntity, Vector2 startPosition, Vector2 startVelocity, Vector2 targetPosition,Vector2 targetVelocity, float acceleration, bool continueOnPath = false)
        {
            SimInput simInput = new SimInput()
            {
                startPosition = startPosition,
                startVelocity = startVelocity,
                targetPosition = targetPosition,
                acceleration = acceleration
            };

            float lowerBoundDuration = CalcLowerBoundDuration(startPosition, startVelocity, targetPosition, acceleration);
            float upperBoundDuration = CalcUpperBoundDuration(startPosition, startVelocity, targetPosition, acceleration);
            
            SimOutput simOutput = IntervalBisection.Estimate<SimInput,SimOutput>(
                simInput, 
                RunSimulation, 
                lowerBoundDuration, 
                upperBoundDuration);

            IMovement[] movementArray =
            {
                new ConstantAcceleration(movableEntity, simOutput.accelerationVector, simOutput.duration)
            };

            if (continueOnPath)
            {
                movementArray = movementArray.Append(new ConstantVelocity(movableEntity, Mathf.Infinity)).ToArray();
            }

            return movementArray;
        }

        private static float RunSimulation(float duration, SimInput simInput, out SimOutput simOutput)
        {
            Vector2 targetMovement = simInput.targetPosition - simInput.startPosition;
            Vector2 accelerationVector = duration == 0 ? Vector2.one.normalized * simInput.acceleration : 
                2 * (targetMovement - simInput.startVelocity * duration) / (duration * duration);

            float error = accelerationVector.magnitude - simInput.acceleration;

            Vector2 endLocation = simInput.startPosition +
                                  simInput.startVelocity * duration +
                                  0.5f * duration * duration * accelerationVector;
            
            if (Vector2.SqrMagnitude(simInput.targetPosition - endLocation) > 0.1f) Debug.LogError($"Result {endLocation} is materially away from target location {simInput.targetPosition}");
            
            simOutput.accelerationVector = accelerationVector;
            simOutput.duration = duration;

            return error;
        }

        private static float CalcLowerBoundDuration(Vector2 startPosition, Vector2 startVelocity, Vector2 target, float acceleration)
        {
            //Assumes all speed orthogonal to target movement is removed immediately
            Vector2 targetMovement = target - startPosition;

            float originalSpeedInDirectionOfTarget = Vector2.Dot(startVelocity, targetMovement); 
            float targetMovementDistance = targetMovement.magnitude;

            float timeToReachTarget =
                (Mathf.Sqrt(2 * acceleration * targetMovementDistance + originalSpeedInDirectionOfTarget * originalSpeedInDirectionOfTarget) - originalSpeedInDirectionOfTarget) 
                / acceleration;

            return Mathf.Max(timeToReachTarget, Mathf.Epsilon);
        }

        private static float CalcUpperBoundDuration(Vector2 startPosition, Vector2 startVelocity, Vector2 target, float acceleration)
        {
            //Assumes full break before accelerating towards target
            float timeToStop = startVelocity.magnitude / acceleration;
            Vector2 locationAfterStopping = startPosition +  0.5f * acceleration * timeToStop * timeToStop * startVelocity.normalized;
            Vector2 targetMovement = target - locationAfterStopping;
            float targetMovementDistance = targetMovement.magnitude;

            float timeToAccelerate = Mathf.Sqrt(2 * targetMovementDistance / acceleration);

            return timeToStop + timeToAccelerate;
        }

        private struct SimOutput
        {
            public Vector2 accelerationVector;
            public float duration;
        }

        private struct SimInput
        {
            public Vector2 startPosition;
            public Vector2 startVelocity;
            public Vector2 targetPosition;
            public float acceleration;
        }
    }
}