﻿using System.Linq;
using BattleMode.Movement.Movements;
using UnityEngine;

namespace BattleMode.Movement.MovementGenerators
{
    public class MaxSpeedIntercept2Step : IMovementGenerator
    {
        public void TriggerMovement(MovableEntity movableEntity, Vector2 targetPosition, Vector2 targetVelocity, float acceleration, bool continueOnPath = false)
        {
            movableEntity.SetMovementList(GenerateMovement(
                movableEntity, 
                movableEntity.Position.Value,
                movableEntity.Velocity.Value,
                targetPosition,
                targetVelocity,
                acceleration, 
                continueOnPath));
        }
        
        public IMovement[] GenerateMovement(MovableEntity movableEntity, Vector2 startPosition, Vector2 startVelocity, Vector2 targetPosition,Vector2 targetVelocity, float acceleration, bool continueOnPath = false)
        {
            SimOutput simOutput = CalculateOffsetMovement(startPosition, startVelocity, targetPosition, acceleration, out SimInput simInput);

            IMovement[] accelerationMovement = new MaxSpeedIntercept().GenerateMovement(movableEntity,
                simInput.startPosition, simInput.startVelocity, simInput.targetPosition, Vector2.zero, simInput.acceleration);
            
            IMovement[] movementArray =
            {
                new ConstantAcceleration(movableEntity, simOutput.accelerationVector, simOutput.duration),
                accelerationMovement[0]
            };

            if (continueOnPath)
            {
                movementArray = movementArray.Append(new ConstantVelocity(movableEntity, Mathf.Infinity)).ToArray();
            }

            return movementArray;
        }

        private static SimOutput CalculateOffsetMovement(Vector2 startPosition, Vector2 startVelocity, Vector2 targetPosition, float acceleration, out SimInput nextSimInput)
        {
            Vector2 targetMovement = targetPosition - startPosition;
            Vector2 orthogonalMovement = startVelocity - (Vector2)Vector3.Project(startVelocity, targetMovement);
            float offsetDuration = orthogonalMovement.magnitude / acceleration;
            Vector2 offsetVector = -acceleration * orthogonalMovement.normalized;

            Vector2 positionAfterOffset = startPosition + startVelocity * offsetDuration + 0.5f * offsetDuration * offsetDuration * offsetVector;
            Vector2 velocityAfterOffset = startVelocity + offsetVector * offsetDuration;

            nextSimInput = new SimInput()
            {
                startPosition = positionAfterOffset,
                startVelocity = velocityAfterOffset,
                targetPosition = targetPosition,
                acceleration = acceleration,
            };
            
            return new SimOutput()
            {
                accelerationVector = offsetVector,
                duration = offsetDuration
            };
        }

        private struct SimOutput
        {
            public Vector2 accelerationVector;
            public float duration;
        }

        private struct SimInput
        {
            public Vector2 startPosition;
            public Vector2 startVelocity; 
            public Vector2 targetPosition;
            public float acceleration;
        }
    }
}