﻿using BattleMode.Movement.Movements;
using UnityEngine;

namespace BattleMode.Movement.MovementGenerators
{
    public interface IMovementGenerator
    {
        void TriggerMovement(MovableEntity movableEntity, Vector2 targetPosition, Vector2 targetVelocity, float acceleration, bool continueOnPath = false);
        IMovement[] GenerateMovement(MovableEntity movableEntity, Vector2 startPosition, Vector2 startVelocity, Vector2 targetPosition, Vector2 targetVelocity, float acceleration, bool continueOnPath = false);
    }
}