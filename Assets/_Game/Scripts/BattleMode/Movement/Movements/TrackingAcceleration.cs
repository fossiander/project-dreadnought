﻿using _Game.Scripts.Utils;
using UnityEngine;

namespace BattleMode.Movement.Movements
{
    public class TrackingAcceleration : IMovement
    {
        private readonly MovableEntity _movableEntity;
        
        private Vector2 _position;
        private Vector2 _velocity;
        private Vector2 _accelerationVector;
        private readonly float _acceleration;

        private readonly MovableEntity _target;
        
        private readonly float _duration;
        private float _elapsedTime;
        private readonly float _hitDistance;

        public TrackingAcceleration(MovableEntity movableEntity, MovableEntity target, float hitDistance, float duration = Mathf.Infinity)
        {
            _movableEntity = movableEntity;
            _target = target;
            _acceleration = movableEntity.MaxAcceleration;
            _hitDistance = hitDistance;
            _duration = duration;
            
            ResetPositionAndVelocity();
        }
        
        public Vector2 GetPositionAtTime(float deltaTime)
        {
            if (deltaTime < 0) return _position;
            
            float cappedElapsedTime = Mathf.Clamp(deltaTime, 0, EstimateTimeToTarget());
            float timeOverhang = deltaTime - cappedElapsedTime;

            Vector2 regularMovement = _velocity * cappedElapsedTime + 0.5f * cappedElapsedTime * cappedElapsedTime * _accelerationVector;
            Vector2 overhangMovement = timeOverhang > 0 ? timeOverhang * GetFinalVelocity() : Vector2.zero;

            return _position + regularMovement + overhangMovement;
        }

        public Vector2 GetCurrentPosition() => _position;

        public Vector2 GetFinalPosition() => _target.Position.Value;

        public Vector2 GetVelocityAtTime(float deltaTime)
        {
            if (deltaTime < 0) return _velocity;
            
            float cappedElapsedTime = Mathf.Clamp(deltaTime, 0, EstimateTimeToTarget());
            return _velocity + _accelerationVector * cappedElapsedTime;
        }

        public Vector2 GetCurrentVelocity() => _velocity;

        public Vector2 GetFinalVelocity() => GetVelocityAtTime(Mathf.Infinity);

        public Vector2 GetCurrentAcceleration() => _accelerationVector;

        public float GetRemainingDuration() => _duration - _elapsedTime;

        public bool UpdateMovement(float deltaTime, out float timeOverhang)
        {
            if (_elapsedTime == 0) ResetPositionAndVelocity();
            
            _elapsedTime += deltaTime;
            timeOverhang = Mathf.Max(_elapsedTime - _duration, 0);
            _elapsedTime -= timeOverhang;
            
            UpdateAcceleration();
            UpdatePositionAndVelocity(deltaTime, timeOverhang);
            
             return timeOverhang > 0;
        }

        public void SetStartPositionAndVelocity(Vector2 position, Vector2 velocity)
        {
            _position = position;
            _velocity = velocity;
        }
        
        private void ResetPositionAndVelocity()
        { 
            _position = _movableEntity.Position.Value;
            _velocity = _movableEntity.Velocity.Value;
        }

        private void UpdateAcceleration()
        {
            float estimatedTimeToTarget = EstimateTimeToTarget();
            Vector2 vectorToTarget = _target.Position.Value - _position;
            Vector2 orthogonalVelocity = _velocity - (Vector2)Vector3.Project(_velocity, vectorToTarget.normalized);

            Vector2 offsetAcceleration = -orthogonalVelocity / estimatedTimeToTarget;
            Vector2 residualAcceleration = VectorUtils.GetResidualAccelerationVector(vectorToTarget, offsetAcceleration, _acceleration, true);

            _accelerationVector = offsetAcceleration + (float.IsNaN(residualAcceleration.x) ? Vector2.zero : residualAcceleration);
        }

        private void UpdatePositionAndVelocity(float totalDeltaTime, float timeOverhang)
        {
            float accelerationTime = totalDeltaTime - timeOverhang;
            _position += _velocity * totalDeltaTime + 0.5f * accelerationTime * accelerationTime * _accelerationVector +
                         accelerationTime * timeOverhang * _accelerationVector;
            _velocity += _accelerationVector * accelerationTime;
        }

        private float EstimateTimeToTarget()
        {
            Vector2 vectorToTarget = _target.Position.Value - _position;
            float distanceToTarget = vectorToTarget.magnitude;
            Vector2 directionToTarget = vectorToTarget / distanceToTarget;
            float velocityInDirectionOfTarget = Vector2.Dot(_velocity, directionToTarget);

            return (Mathf.Sqrt(2 * _acceleration * distanceToTarget + 
                               velocityInDirectionOfTarget * velocityInDirectionOfTarget) - 
                    velocityInDirectionOfTarget) 
                   / _acceleration;
        }

        public bool HasReachedTarget(float deltaTime) => Vector2.Distance(_position, _target.Position.Value) <=
                                          Mathf.Max(_hitDistance, _velocity.magnitude * deltaTime);

        public bool IsCompleted() => _elapsedTime >= _duration;
    }
}