﻿using UnityEngine;

namespace BattleMode.Movement.Movements
{
    public class ConstantAcceleration : IMovement
    {
        private readonly MovableEntity _movableEntity;
        
        private Vector2 _startPosition;
        private Vector2 _startVelocity;
        private readonly Vector2 _acceleration;
        private readonly float _duration;
        private float _elapsedTime;

        public ConstantAcceleration(MovableEntity movableEntity, Vector2 acceleration, float duration = Mathf.Infinity)
        {
            _movableEntity = movableEntity;
            _acceleration = acceleration;
            _duration = duration;
            
            ResetPositionAndVelocity();
        }

        public Vector2 GetPositionAtTime(float deltaTime)
        {
            return GetPositionAfterTimeSinceStart(_elapsedTime + deltaTime);
        }

        public Vector2 GetCurrentPosition()
        {
            return GetPositionAfterTimeSinceStart(_elapsedTime);
        }

        public Vector2 GetFinalPosition()
        {
            return GetPositionAfterTimeSinceStart(_duration);
        }

        private Vector2 GetPositionAfterTimeSinceStart(float elapsedTime)
        {
            if (elapsedTime < 0) return _startPosition;
            
            float cappedElapsedTime = Mathf.Clamp(elapsedTime, 0, _duration);
            float timeOverhang = elapsedTime - cappedElapsedTime;

            Vector2 regularMovement = _startVelocity * cappedElapsedTime + 0.5f * cappedElapsedTime * cappedElapsedTime * _acceleration;
            Vector2 overhangMovement = timeOverhang > 0 ? timeOverhang * GetFinalVelocity() : Vector2.zero;

            return _startPosition + regularMovement + overhangMovement;
        }

        public Vector2 GetVelocityAtTime(float deltaTime)
        {
            return GetVelocityAfterTimeSinceStart(_elapsedTime + deltaTime);
        }

        public Vector2 GetCurrentVelocity()
        {
            return GetVelocityAfterTimeSinceStart(_elapsedTime);
        }

        public Vector2 GetFinalVelocity()
        {
            return GetVelocityAfterTimeSinceStart(_duration);
        }

        public Vector2 GetCurrentAcceleration()
        {
            return _acceleration;
        }

        public float GetRemainingDuration()
        {
            return _duration - _elapsedTime;
        }

        private Vector2 GetVelocityAfterTimeSinceStart(float elapsedTime)
        {
            if (elapsedTime < 0) return _startVelocity;
            
            float cappedElapsedTime = Mathf.Clamp(elapsedTime, 0, _duration);
            return _startVelocity + _acceleration * cappedElapsedTime;
        }

        private void ResetPositionAndVelocity()
        {
            _startPosition = _movableEntity.Position.Value;
            _startVelocity = _movableEntity.Velocity.Value;
        }

        public bool UpdateMovement(float deltaTime, out float timeOverhang)
        {
            if (_elapsedTime == 0) ResetPositionAndVelocity();
            
            _elapsedTime += deltaTime;
            timeOverhang = Mathf.Max(_elapsedTime - _duration, 0);
            _elapsedTime -= timeOverhang;
            return timeOverhang > 0;
        }

        public void SetStartPositionAndVelocity(Vector2 position, Vector2 velocity)
        {
            _startPosition = position;
            _startVelocity = velocity;
        }

        public bool IsCompleted() => _elapsedTime >= _duration;
    }
}