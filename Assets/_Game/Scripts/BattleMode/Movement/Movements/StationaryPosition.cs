﻿using UnityEngine;

namespace BattleMode.Movement.Movements
{
    public class StationaryPosition : IMovement
    {
        private readonly MovableEntity _movableEntity;

        private Vector2 _position;
        private readonly float _duration;

        private float _elapsedTime;

        public StationaryPosition(MovableEntity movableEntity, float duration)
        {
            _movableEntity = movableEntity;
            _duration = duration;
            
            ResetPosition();
        }
        
        public void SetStartingPositionAndVelocity(Vector2 startPosition, Vector2 startVelocity)
        {
            _position = startPosition;
        }

        public Vector2 GetPositionAtTime(float deltaTime) => _position;
        public Vector2 GetCurrentPosition() => _position;
        public Vector2 GetFinalPosition() => _position;
        public Vector2 GetVelocityAtTime(float deltaTime) => Vector2.zero;
        public Vector2 GetCurrentVelocity() => Vector2.zero;
        public Vector2 GetFinalVelocity() => Vector2.zero;
        public Vector2 GetCurrentAcceleration() => Vector2.zero;
        public float GetRemainingDuration() => _duration - _elapsedTime;

        private void ResetPosition()
        {
            _position = _movableEntity.Position.Value;
        }

        public bool UpdateMovement(float deltaTime, out float timeOverhang)
        {
            if (_elapsedTime == 0) ResetPosition();

            _elapsedTime += deltaTime;
            timeOverhang = Mathf.Max(_elapsedTime - _duration, 0);
            _elapsedTime -= timeOverhang;
            return timeOverhang > 0;
        }
        
        public void SetStartPositionAndVelocity(Vector2 position, Vector2 velocity)
        {
            _position = position;
        }
        
        public bool IsCompleted() => _elapsedTime >= _duration;
    }
}