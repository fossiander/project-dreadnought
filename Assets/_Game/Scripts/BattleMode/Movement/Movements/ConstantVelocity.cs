﻿using UnityEngine;

namespace BattleMode.Movement.Movements
{
    public class ConstantVelocity : IMovement
    {
        private readonly MovableEntity _movableEntity;

        private Vector2 _startPosition;
        private Vector2 _velocity;
        private readonly float _duration;
        private float _elapsedTime;

        public ConstantVelocity(MovableEntity movableEntity, float duration)
        {
            _movableEntity = movableEntity;

            _duration = duration;
            
            ResetPositionAndVelocity();
        }
        
        public void SetStartingPositionAndVelocity(Vector2 startPosition, Vector2 startVelocity)
        {
            _startPosition = startPosition;
            _velocity = startVelocity;
        }

        public Vector2 GetPositionAtTime(float deltaTime)
        {
            return GetPositionAfterTimeSinceStart(_elapsedTime + deltaTime);
        }

        public Vector2 GetCurrentPosition()
        {
            return GetPositionAfterTimeSinceStart(_elapsedTime);
        }

        public Vector2 GetFinalPosition()
        {
            return GetPositionAfterTimeSinceStart(Mathf.Infinity);
        }

        private Vector2 GetPositionAfterTimeSinceStart(float elapsedTime)
        {
            if (elapsedTime < 0) return _startPosition;
            return _startPosition + _velocity * elapsedTime;
        }

        public Vector2 GetVelocityAtTime(float deltaTime)
        {
            return GetVelocityAfterTimeSinceStart(_elapsedTime + deltaTime);
        }

        public Vector2 GetCurrentVelocity()
        {
            return GetVelocityAfterTimeSinceStart(_elapsedTime);
        }

        public Vector2 GetFinalVelocity()
        {
            return GetVelocityAfterTimeSinceStart(_duration);
        }

        public Vector2 GetCurrentAcceleration()
        {
            return Vector2.zero;
        }
        
        public float GetRemainingDuration()
        {
            return _duration - _elapsedTime;
        }

        private Vector2 GetVelocityAfterTimeSinceStart(float elapsedTime)
        {
            return _velocity;
        }

        private void ResetPositionAndVelocity()
        {
            _startPosition = _movableEntity.Position.Value;
            _velocity = _movableEntity.Velocity.Value;
        }

        public bool UpdateMovement(float deltaTime, out float timeOverhang)
        {
            if (_elapsedTime == 0) ResetPositionAndVelocity();

            _elapsedTime += deltaTime;
            timeOverhang = Mathf.Max(_elapsedTime - _duration, 0);
            _elapsedTime -= timeOverhang;
            return timeOverhang > 0;
        }
        
        public void SetStartPositionAndVelocity(Vector2 position, Vector2 velocity)
        {
            _startPosition = position;
            _velocity = velocity;
        }
        
        public bool IsCompleted() => _elapsedTime >= _duration;
    }
}