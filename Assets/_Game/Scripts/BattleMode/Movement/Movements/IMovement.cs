﻿using UnityEngine;

namespace BattleMode.Movement.Movements
{
    public interface IMovement
    {
        Vector2 GetPositionAtTime(float deltaTime);
        Vector2 GetCurrentPosition();
        Vector2 GetFinalPosition();
        Vector2 GetVelocityAtTime(float deltaTime);
        Vector2 GetCurrentVelocity();
        Vector2 GetFinalVelocity();
        Vector2 GetCurrentAcceleration();
        float GetRemainingDuration();
        bool UpdateMovement(float deltaTime, out float timeOverhang);
        void SetStartPositionAndVelocity(Vector2 position, Vector2 velocity);
        bool IsCompleted();
    }
}