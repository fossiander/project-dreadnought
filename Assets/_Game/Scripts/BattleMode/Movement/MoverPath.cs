using BattleMode.Core;
using Shapes;
using UnityEngine;

namespace BattleMode.Movement
{
    public class MoverPath : MonoBehaviour, IColorChanger
    {
        [SerializeField] private Polyline polyline;
        [SerializeField] private float timeStepLength = 1f;
        [SerializeField] private int timeSteps = 10;

        private MovableEntity _movableEntity;
        private Vector2[] _path;
        
        private void Awake()
        {
            _path = new Vector2[timeSteps + 1];
        }

        public void SetColor(Color color)
        {
            polyline.Color = color;
        }

        private void Update()
        {
            if (_movableEntity == null)
            {
                Destroy(gameObject);
                return;
            }
            
            int totalSteps = timeSteps + 1;
        
            if (polyline.points.Count != totalSteps) polyline.points.Clear();
            {
                for (int i = 0; i < totalSteps; i++) polyline.AddPoint(Vector3.zero);
            }
        
            _movableEntity.GetPath(ref _path, timeStepLength);
            for (int i = 0; i < _path.Length; i++)
            {
                polyline.points[i] = new PolylinePoint(_path[i]);
            }
        }

        public void SetReference(MovableEntity referenceEntity)
        {
            _movableEntity = referenceEntity;
        }
    }
}
