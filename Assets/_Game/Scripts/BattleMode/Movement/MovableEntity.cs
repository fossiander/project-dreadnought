using System;
using System.Collections.Generic;
using _Game.Scripts.Utils;
using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Movement.Movements;
using BattleMode.Spawning;
using BattleMode.UnitConversion;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Movement
{
    public class MovableEntity : NetworkBehaviour
    {
        [SerializeField] private NetworkVariable<float> maxAcceleration = new NetworkVariable<float>(1);
        [SerializeField] private bool spawnPath = false;

        public NetworkVariable<Vector2> Position { get; private set; } = new NetworkVariable<Vector2>();
        public NetworkVariable<Vector2> Velocity { get; private set; } = new NetworkVariable<Vector2>();
        public NetworkVariable<Vector2> Acceleration { get; private set; } = new NetworkVariable<Vector2>();
        public NetworkVariable<Vector2> Direction { get; private set; } = new NetworkVariable<Vector2>();

        private readonly NetworkVariable<bool> _isMoving = new NetworkVariable<bool>();

        public float MaxAcceleration => maxAcceleration.Value;
        public NetworkEventVariable<bool> IsMoving { get; }

        private IMovement _currentMovement;
        private readonly LinkedList<IMovement> _movementQueue = new LinkedList<IMovement>();

        private readonly LazyObjectFinder<PathSpawner> _pathSpawner = new LazyObjectFinder<PathSpawner>();

        public MovableEntity()
        {
            IsMoving = new NetworkEventVariable<bool>(_isMoving);
        }
        
        private void Awake()
        {
            Position.Value = transform.position;
            Velocity.Value = Vector2.zero;
            Acceleration.Value = Vector2.zero;
            Direction.Value = transform.up;
            
            FreezePosition();
        }

        private void Start()
        {
            UpdateTransform();
        }

        public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();
            
            if (spawnPath) _pathSpawner.Get().SpawnPath(this);
        }

        private void OnEnable()
        {
            Position.OnValueChanged += OnPositionChanged;
            Direction.OnValueChanged += OnDirectionChanged;
        }
        
        private void OnDisable()
        {
            Position.OnValueChanged -= OnPositionChanged;
            Direction.OnValueChanged -= OnDirectionChanged;
        }

        private void OnPositionChanged(Vector2 previousValue, Vector2 newValue)
        {
            UpdateTransform();
        }

        private void OnDirectionChanged(Vector2 previousValue, Vector2 newValue)
        {
            UpdateTransform();
        }

        private void Update()
        {
            if (!IsServer) return;

            _isMoving.Value = _currentMovement != null && _currentMovement.GetType() != typeof(StationaryPosition);
            UpdateMovement(Time.deltaTime);
        }

        private void UpdateTransform()
        {
            transform.position = Position.Value;
            transform.up = Direction.Value;
        }

        public bool UpdateMovement(float deltaTime, bool freezeOnCompletion = false)
        {
            while (deltaTime > 0)
            {
                bool completed = _currentMovement.UpdateMovement(deltaTime, out float timeOverhang);
                Position.Value = _currentMovement.GetCurrentPosition();
                Velocity.Value = _currentMovement.GetCurrentVelocity();
                Acceleration.Value = _currentMovement.GetCurrentAcceleration();

                if (Acceleration.Value.magnitude > 0) Direction.Value = Acceleration.Value.normalized;
                
                deltaTime = timeOverhang;

                if (completed)
                {
                    bool movementCompleted = !GetNextMovement();
                    if (movementCompleted)
                    {
                        if (freezeOnCompletion) FreezePosition();
                        return true;
                    }
                }
            }

            return false;
        }

        public void SetPositionVelocityAndDirection(Vector2 position, Vector2 velocity, Vector2 direction)
        {
            Position.Value = position;
            Velocity.Value = velocity;
            Acceleration.Value = Vector2.zero;
            Direction.Value = direction;
            
            FreezePosition();
            UpdateTransform();
        }
        
        public void SetMovement(IMovement movement)
        {
            _movementQueue.Clear();

            if (movement == null)
            {
                CancelMovement();
                return;
            }
            
            movement.SetStartPositionAndVelocity(Position.Value, Velocity.Value);
            _currentMovement = movement;
        }

        public void SetMovementList(IReadOnlyList<IMovement> movements)
        {
            if (movements.Count == 0)
            {
                CancelMovement();
                return;
            }

            SetMovement(movements[0]);
            for (int i = 1; i < movements.Count; i++)
            {
                movements[i].SetStartPositionAndVelocity(movements[i - 1].GetFinalPosition(), movements[i - 1].GetFinalVelocity());
                _movementQueue.AddLast(movements[i]);
            }
        }
        
        private void FreezePosition()
        {
            SetMovement(new StationaryPosition(this, float.PositiveInfinity));
        }
        
        public void CancelMovement()
        {
            SetMovement(new ConstantVelocity(this, float.PositiveInfinity));
        }
        
        private bool GetNextMovement()
        {
            if (_movementQueue.Count == 0)
            {
                BreakToStop();
                return false;
            }
            
            _currentMovement = _movementQueue.First.Value;
            _movementQueue.RemoveFirst();
            return true;
        }
        
        public void BreakToStop()
        {
            if (Mathf.Approximately(Velocity.Value.magnitude, 0))
            {
                FreezePosition();
                return;
            }
            
            Vector2 stopDirection = -Velocity.Value.normalized;
            float timeToStop = Velocity.Value.magnitude / MaxAcceleration;
        
            SetMovement(new ConstantAcceleration(this, MaxAcceleration * stopDirection, timeToStop));
        }
        
        public void GetPath(ref Vector2[] path, float incrementLength, bool estimated = false)
        {
            if (estimated)
            {
                for (int i = 0; i < path.Length; i++)
                {
                    float time = i * incrementLength;
                    path[i] = Position.Value + Velocity.Value * time + 0.5f * time * time * Acceleration.Value;
                }
                
                return;
            }
            
            LinkedListNode<IMovement> movement = null;
            float previousMovementDuration = 0;

            for (int i = 0; i < path.Length; i++)
            {
                float time = i * incrementLength;

                if (time <= _currentMovement.GetRemainingDuration())
                {
                    path[i] = _currentMovement.GetPositionAtTime(time);
                    continue;
                }

                if (_movementQueue.Count == 0)
                {
                    path[i] = _currentMovement.GetFinalPosition();
                    continue;
                }

                if (movement == null)
                {
                    previousMovementDuration = _currentMovement.GetRemainingDuration();
                    movement = _movementQueue.First;
                }
                else if (time > previousMovementDuration + movement.Value.GetRemainingDuration())
                {
                    if (movement.Next == null)
                    {
                        path[i] = movement.Value.GetFinalPosition();
                        continue;
                    }

                    previousMovementDuration = movement.Value.GetRemainingDuration();
                    movement = movement.Next;
                }

                float incrementalTime = time - previousMovementDuration;
                path[i] = movement.Value.GetPositionAtTime(incrementalTime);
            }
        }

        public void SetMaxAcceleration(AccelerationAmount accelerationAmount)
        {
            maxAcceleration.Value = accelerationAmount.GetValueInCoreUnits();
        }
    }
}