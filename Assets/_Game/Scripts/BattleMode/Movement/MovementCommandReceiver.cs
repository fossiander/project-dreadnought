﻿using _Game.Scripts.Utils.Event_Utils;
using BattleMode.Movement.MovementGenerators;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Events;

namespace BattleMode.Movement
{
    public class MovementCommandReceiver : NetworkBehaviour
    {
        [SerializeField] private MovableEntity movableEntity;

        public MovableEntity MovableEntity => movableEntity;
        public UnityEvent OnMovementCommandReceived { get; } = new UnityEvent();
        public UnityEvent OnStopCommandReceived { get; } = new UnityEvent();
        
        private readonly IMovementGenerator _targetSpeedInterceptGenerator = new TargetSpeedIntercept();
        private readonly IMovementGenerator _maxSpeedInterceptGenerator = new MaxSpeedIntercept();
        
        public void BreakToStop()
        {
            BreakToStopServerRpc();
            TriggerStopCommandReceivedEvent();
        }
        
        [ServerRpc]
        private void BreakToStopServerRpc()
        {
            movableEntity.BreakToStop();
        }
        
        public void CancelMovement()
        {
            CancelMovementServerRpc();
            TriggerStopCommandReceivedEvent();
        }
        
        [ServerRpc]
        private void CancelMovementServerRpc()
        {
            movableEntity.CancelMovement();
        }

        public void MoveToLocationAtTargetVelocity(Vector2 targetPosition, Vector2 targetVelocity)
        {
            MoveToLocationAtTargetVelocityServerRpc(targetPosition, targetVelocity);
            TriggerMovementCommandReceivedEvent();
        }

        [ServerRpc]
        private void MoveToLocationAtTargetVelocityServerRpc(Vector2 targetPosition, Vector2 targetVelocity)
        {
            _targetSpeedInterceptGenerator.TriggerMovement(
                movableEntity,
                targetPosition, 
                targetVelocity, 
                movableEntity.MaxAcceleration,
                true);
            
            EventSystem.TriggerEvent(new MovementCommandReceivedEvent()
            {
                position = targetPosition,
                velocity = targetVelocity
            });
        }
        
        public void MoveToLocationAtMaximumVelocity(Vector2 targetPosition)
        {
            MoveToLocationAtMaximumVelocityServerRpc(targetPosition);
            TriggerMovementCommandReceivedEvent();
        }

        [ServerRpc]
        private void MoveToLocationAtMaximumVelocityServerRpc(Vector2 targetPosition)
        {
            _maxSpeedInterceptGenerator.TriggerMovement(
                movableEntity,
                targetPosition, 
                Vector2.zero, 
                movableEntity.MaxAcceleration,
                true);

            EventSystem.TriggerEvent(new MovementCommandReceivedEvent()
            {
                position = targetPosition,
                velocity = Vector2.positiveInfinity
            });
        }

        private void TriggerMovementCommandReceivedEvent()
        {
            OnMovementCommandReceived?.Invoke();
        }
        
        private void TriggerStopCommandReceivedEvent()
        {
            OnStopCommandReceived?.Invoke();
        }
    }

    public class MovementCommandReceivedEvent : Event<MovementCommandReceivedEvent>
    {
        public Vector2 position;
        public Vector2 velocity;
    }
}