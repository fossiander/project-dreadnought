using System.Collections.Generic;
using _Game.Scripts.Utils;
using BattleMode.Movement;
using BattleMode.Movement.MovementGenerators;
using BattleMode.Movement.Movements;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BattleMode.Simulation
{
    public class SimulationManager : MonoBehaviour
    {
        [SerializeField] private Vector2 startingPosition = Vector2.zero;
        [SerializeField] private Vector2 startingVelocity = Vector2.zero;
        [SerializeField] private Vector2 targetPosition = Vector2.one;
        [SerializeField] private Vector2 targetVelocity = Vector2.one;
        [SerializeField] private float acceleration = 1f;

        [SerializeField] private int maxNumberOfIncrements = 1000;
        [SerializeField] private float incrementLength = 0.1f;
        [SerializeField] private bool stopOnCompletion = false;

        [SerializeField] private LineChart lineChart = null;
        
        [Button]
        public void Simulate()
        {
            lineChart.Clear();

            IMovementGenerator[] movementGenerators = new IMovementGenerator[]
            {
                new MaxSpeedIntercept(),
                new MaxSpeedIntercept2Step(),
                new TargetSpeedIntercept()
            };

            List<MovableEntity> movementControllers = new List<MovableEntity>();
            List<IMovement[]> movementProfiles = new List<IMovement[]>();

            for (int i = 0; i < movementGenerators.Length; i++)
            {
                MovableEntity newMovableEntity = new GameObject($"Movement Controller {i}").AddComponent<MovableEntity>();
                newMovableEntity.SetPositionVelocityAndDirection(startingPosition, startingVelocity, Vector2.up);
                
                movementControllers.Add(newMovableEntity);
                movementProfiles.Add(
                    movementGenerators[i].GenerateMovement(movementControllers[i], startingPosition, startingVelocity, targetPosition, targetVelocity, acceleration));
            }
            
            for (int j = 0; j < movementProfiles.Count; j++)
            {
                movementControllers[j].SetMovementList(movementProfiles[j]);
                List<Vector2> path = new List<Vector2>();
                List<SimData> simData = new List<SimData>();

                bool alreadyCompleted = false;
                for (int i = 0; i < maxNumberOfIncrements + 1; i++)
                {
                    bool completed = false;
                    
                    if (i > 0) completed = movementControllers[j].UpdateMovement(incrementLength, stopOnCompletion);
                    path.Add(movementControllers[j].Position.Value);
                    simData.Add(new SimData()
                    {
                        time = i * incrementLength,
                        position = movementControllers[j].Position.Value,
                        velocity = movementControllers[j].Velocity.Value,
                        acceleration = movementControllers[j].Acceleration.Value
                    });
                    
                    if (completed && !alreadyCompleted) Debug.Log($"Movement {j} completed after {i} steps / {i * incrementLength} units of time.");
                    if (completed && stopOnCompletion) break;
                    
                    alreadyCompleted = alreadyCompleted || completed;
                }
                
                lineChart.AddPath(path.ToArray());
            }
            
            lineChart.AddPoint(startingPosition);
            lineChart.AddPoint(targetPosition);
        }

        private struct SimData
        {
            public float time;
            public Vector2 position;
            public Vector2 velocity;
            public Vector2 acceleration;
        }
    }
}