using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BattleMode.Networking
{
    public class NetworkGUI : MonoBehaviour
    {
        [SerializeField] private bool hostOnStart = false;

        private void Start()
        {
#if (UNITY_EDITOR)
            if (hostOnStart) NetworkManager.Singleton.StartHost();
#endif
        }

        void OnGUI()
        {
            GUILayout.BeginArea(new Rect(Screen.width - 160, 10, 150, 300));
            if (!NetworkManager.Singleton.IsClient && !NetworkManager.Singleton.IsServer)
            {
                StartButtons();
            }
            else
            {
                ManageButtons();
                StatusLabels();
            }

            GUILayout.EndArea();
        }

        static void StartButtons()
        {
            if (GUILayout.Button("Host")) NetworkManager.Singleton.StartHost();
            if (GUILayout.Button("Client")) NetworkManager.Singleton.StartClient();
            if (GUILayout.Button("Server")) NetworkManager.Singleton.StartServer();
        }

        static void ManageButtons()
        {
            if (NetworkManager.Singleton.IsServer)
            {
                if (GUILayout.Button("Stop Server")) 
                    Shutdown();
                return;
            }
        
            if (NetworkManager.Singleton.IsClient)
            {
                if (GUILayout.Button("Disconnect Client")) 
                    Shutdown();
            }
        }

        private static void Shutdown()
        {
            NetworkManager.Singleton.Shutdown();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        static void StatusLabels()
        {
            var mode = NetworkManager.Singleton.IsHost ? "Host" :
                NetworkManager.Singleton.IsServer ? "Server" : "Client";

            GUILayout.Label("Transport: " +
                            NetworkManager.Singleton.NetworkConfig.NetworkTransport.GetType().Name);
            GUILayout.Label("Mode: " + mode);
        }
    }
}
