using BattleMode.Core;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Networking
{
    public class PlayerSpawner : NetworkBehaviour
    {
        [SerializeField] private Player aiPlayerPrefab = null;

        private void OnGUI()
        {
            GUILayout.BeginArea(new Rect(Screen.width - 320, 10, 150, 300));
            if (NetworkManager.Singleton.IsClient || NetworkManager.Singleton.IsServer)
            {
                SpawnPlayerButtons();
            }

            GUILayout.EndArea();
        }

        private void SpawnPlayerButtons()
        {
            if (GUILayout.Button("Add AI player"))
            {
                if (NetworkManager.Singleton.IsServer) SpawnAIPlayer();
                else SpawnAIPlayerServerRpc();
            }
        }
    
        [ServerRpc]
        private void SpawnAIPlayerServerRpc()
        {
            SpawnAIPlayer();
        }

        private void SpawnAIPlayer()
        {
            Player playerInstance = Instantiate(aiPlayerPrefab);
            playerInstance.GetComponent<NetworkObject>().Spawn(true);
        }
    }
}
