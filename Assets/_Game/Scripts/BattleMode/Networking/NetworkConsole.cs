﻿using QFSW.QC;
using Unity.Netcode;

namespace BattleMode.Networking
{
    public static class NetworkConsole
    {
        [Command("start-host")]
        private static void StartHost()
        {
            NetworkManager.Singleton.StartHost();
        }
        
        [Command("start-client")]
        private static void StartClient()
        {
            NetworkManager.Singleton.StartClient();
        }
        
        [Command("start-server")]
        private static void StartServer()
        {
            NetworkManager.Singleton.StartServer();
        }

        [Command("network-shutdown")]
        private static void NetworkShutdown()
        {
            NetworkManager.Singleton.Shutdown();
        }
    }
}