﻿using Unity.Netcode;

namespace BattleMode.Networking
{
    public static class NetworkUtils
    {
        public static void Log(string logMessage, NetworkObject logger, bool isServer, bool isClient)
        {
            string label;
            if (isServer && isClient) label = "Host";
            else if (isServer) label = "Server";
            else if (isClient) label = "Client";
            else label = "Disconnected";

            NetworkLog.LogInfoServer($"#{logger.NetworkObjectId} ({label}):{logMessage}");
        }
    }
}