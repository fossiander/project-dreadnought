﻿using System;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BattleMode.Networking
{
    [RequireComponent(typeof(NetworkManager))]
    public class RandomPositionPlayerSpawner: MonoBehaviour
    {
        private NetworkManager _networkManager;

        int _roundRobinIndex = 0;
    
        [SerializeField]
        private SpawnMethod spawnMethod;
    
        [SerializeField]
        private List<Transform> spawnPositions = new List<Transform>();

        private Transform GetNextSpawn()
        {
            switch (spawnMethod)
            {
                case SpawnMethod.Random:
                    var index = Random.Range(0, spawnPositions.Count);
                    return spawnPositions[index];
                case SpawnMethod.RoundRobin:
                    _roundRobinIndex = (_roundRobinIndex + 1) % spawnPositions.Count;
                    return spawnPositions[_roundRobinIndex];
                default:
                    throw new NotImplementedException();
            }
        }
     
        private void Awake()
        {
            var networkManager = gameObject.GetComponent<NetworkManager>();
            networkManager.ConnectionApprovalCallback += ConnectionApprovalWithRandomSpawnPos;
        }

        void ConnectionApprovalWithRandomSpawnPos(byte[] payload, ulong clientId, NetworkManager.ConnectionApprovedDelegate callback)
        {
            Transform spawn = GetNextSpawn();
            
            callback(true, null, true, spawn.position, spawn.rotation);
        }
    }

    enum SpawnMethod
    {
        Random = 0,
        RoundRobin = 1,
    }

}