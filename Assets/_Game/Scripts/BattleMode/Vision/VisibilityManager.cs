﻿using System.Linq;
using BattleMode.Combat;
using BattleMode.Core;
using UnityEngine;

namespace BattleMode.Vision
{
    public class VisibilityManager : MonoBehaviour
    {
        public AttackableObject[] GetVisibleEnemies(AttackableType attackableType, int ownerID)
        {
            //TODO: Change this to be more efficient
            var attackableObjects = AttackableObject.AllAttackableObjects;
            
            return attackableObjects.Where(ao => ao.OwnerID != ownerID && (attackableType == AttackableType.Any || ao.AttackableType == attackableType)).ToArray();
        }
    }
}