﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BattleMode.UnitConversion
{
    [System.Serializable]
    [InlineProperty]
    public struct VelocityAmount : IEquatable<VelocityAmount>
    {
        public const VelocityAmount.Unit CORE_UNIT = VelocityAmount.Unit.KilometersPerMinute;
        
        [HorizontalGroup][HideLabel][SerializeField] private float valueInOwnUnit;
        [HorizontalGroup][HideLabel][SerializeField] private Unit valueUnit;

        public VelocityAmount(float valueInOwnUnit, Unit valueUnit)
        {
            this.valueInOwnUnit = valueInOwnUnit;
            this.valueUnit = valueUnit;
        }

        public float GetValueIn(Unit returnValueUnit)
        {
            return valueInOwnUnit * GetBaseUnitsPer1Unit(valueUnit) / GetBaseUnitsPer1Unit(returnValueUnit);
        }
        
        public float GetValueInCoreUnits()
        {
            return GetValueIn(CORE_UNIT);
        }

        public VelocityAmount AddAmount(VelocityAmount addedAmount)
        {
            float addedAmountInOriginalUnit = ConvertAmountToUnit(addedAmount.valueInOwnUnit, addedAmount.valueUnit, valueUnit);
            return new VelocityAmount(valueInOwnUnit + addedAmountInOriginalUnit, valueUnit);
        }
        
        public VelocityAmount RemoveAmount(VelocityAmount removedAmount)
        {
            float removedAmountInOriginalUnit = ConvertAmountToUnit(removedAmount.valueInOwnUnit, removedAmount.valueUnit, valueUnit);
            return new VelocityAmount(valueInOwnUnit - removedAmountInOriginalUnit, valueUnit);
        }
        
        public VelocityAmount MultiplyAmountWith(float multiplier)
        {
            return new VelocityAmount(valueInOwnUnit * multiplier, valueUnit);
        }
        
        public VelocityAmount DivideAmountBy(float divider)
        {
            return MultiplyAmountWith(1 / divider);
        }

        private static float ConvertAmountToUnit(float amount, Unit amountUnit, Unit outputUnit)
        {
            return amount * GetBaseUnitsPer1Unit(amountUnit) / GetBaseUnitsPer1Unit(outputUnit);
        }

        private static float GetBaseUnitsPer1Unit(Unit unit)
        {
            return unit switch
            {
                Unit.KilometersPerMinute => 1f,
                Unit.KilometersPerSecond => 60f,
                Unit.KilometersPerHour => 1 / 60f,
                Unit.MilesPerHour => 1.60934f / 60f,
                _ => throw new ArgumentOutOfRangeException(nameof(unit), unit, null)
            };
        }

        public enum Unit
        {
            KilometersPerMinute,
            KilometersPerSecond,
            KilometersPerHour,
            MilesPerHour
        }

        public bool Equals(VelocityAmount other)
        {
            return valueInOwnUnit.Equals(other.valueInOwnUnit) && valueUnit == other.valueUnit;
        }

        public override bool Equals(object obj)
        {
            return obj is VelocityAmount other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(valueInOwnUnit, (int)valueUnit);
        }
    }
}