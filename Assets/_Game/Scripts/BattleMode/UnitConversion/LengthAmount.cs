﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BattleMode.UnitConversion
{
    [System.Serializable]
    [InlineProperty]
    public struct LengthAmount : IEquatable<LengthAmount>
    {
        public const LengthAmount.Unit CORE_UNIT = LengthAmount.Unit.Kilometers;

        [HorizontalGroup][HideLabel][SerializeField] private float valueInOwnUnit;
        [HorizontalGroup][HideLabel][SerializeField] private Unit valueUnit;

        public LengthAmount(float valueInOwnUnit, Unit valueUnit)
        {
            this.valueInOwnUnit = valueInOwnUnit;
            this.valueUnit = valueUnit;
        }
        
        public float GetValueIn(Unit returnValueUnit)
        {
            return valueInOwnUnit * GetBaseUnitsPer1Unit(valueUnit) / GetBaseUnitsPer1Unit(returnValueUnit);
        }
        
        public float GetValueInCoreUnits()
        {
            return GetValueIn(CORE_UNIT);
        }

        public LengthAmount AddAmount(LengthAmount addedAmount)
        {
            float addedAmountInOriginalUnit = ConvertAmountToUnit(addedAmount.valueInOwnUnit, addedAmount.valueUnit, valueUnit);
            return new LengthAmount(valueInOwnUnit + addedAmountInOriginalUnit, valueUnit);
        }
        
        public LengthAmount RemoveAmount(LengthAmount removedAmount)
        {
            float removedAmountInOriginalUnit = ConvertAmountToUnit(removedAmount.valueInOwnUnit, removedAmount.valueUnit, valueUnit);
            return new LengthAmount(valueInOwnUnit - removedAmountInOriginalUnit, valueUnit);
        }
        
        public LengthAmount MultiplyAmountWith(float multiplier)
        {
            return new LengthAmount(valueInOwnUnit * multiplier, valueUnit);
        }
        
        public LengthAmount DivideAmountBy(float divider)
        {
            return MultiplyAmountWith(1 / divider);
        }

        private static float ConvertAmountToUnit(float amount, Unit amountUnit, Unit outputUnit)
        {
            return amount * GetBaseUnitsPer1Unit(amountUnit) / GetBaseUnitsPer1Unit(outputUnit);
        }
        
        private static float GetBaseUnitsPer1Unit(Unit unit)
        {
            return unit switch
            {
                Unit.Kilometers => 1f,
                Unit.Meters => 0.001f,
                Unit.Miles => 1.60934f,
                _ => throw new ArgumentOutOfRangeException(nameof(unit), unit, null)
            };
        }
        
        public enum Unit
        {
            Kilometers,
            Meters,
            Miles
        }

        public bool Equals(LengthAmount other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return valueInOwnUnit.Equals(other.valueInOwnUnit) && valueUnit == other.valueUnit;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((LengthAmount)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(valueInOwnUnit, (int)valueUnit);
        }
    }
}