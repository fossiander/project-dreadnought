﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BattleMode.UnitConversion
{
    [System.Serializable]
    [InlineProperty]
    public struct AccelerationAmount : IEquatable<AccelerationAmount>
    {
        public const AccelerationAmount.Unit CORE_UNIT = AccelerationAmount.Unit.KilometersPerMinuteSquared;

        [HorizontalGroup][HideLabel][SerializeField] private float valueInOwnUnit;
        [HorizontalGroup][HideLabel][SerializeField] private Unit valueUnit;

        public AccelerationAmount(float valueInOwnUnit, Unit valueUnit)
        {
            this.valueInOwnUnit = valueInOwnUnit;
            this.valueUnit = valueUnit;
        }

        public float GetValueIn(Unit returnValueUnit)
        {
            return valueInOwnUnit * GetBaseUnitsPer1Unit(valueUnit) / GetBaseUnitsPer1Unit(returnValueUnit);
        }
        
        public float GetValueInCoreUnits()
        {
            return GetValueIn(CORE_UNIT);
        }

        public AccelerationAmount AddAmount(AccelerationAmount addedAmount)
        {
            float addedAmountInOriginalUnit = ConvertAmountToUnit(addedAmount.valueInOwnUnit, addedAmount.valueUnit, valueUnit);
            return new AccelerationAmount(valueInOwnUnit + addedAmountInOriginalUnit, valueUnit);
        }
        
        public AccelerationAmount RemoveAmount(AccelerationAmount removedAmount)
        {
            float removedAmountInOriginalUnit = ConvertAmountToUnit(removedAmount.valueInOwnUnit, removedAmount.valueUnit, valueUnit);
            return new AccelerationAmount(valueInOwnUnit - removedAmountInOriginalUnit, valueUnit);
        }
        
        public AccelerationAmount MultiplyAmountWith(float multiplier)
        {
            return new AccelerationAmount(valueInOwnUnit * multiplier, valueUnit);
        }
        
        public AccelerationAmount DivideAmountBy(float divider)
        {
            return MultiplyAmountWith(1 / divider);
        }

        private static float ConvertAmountToUnit(float amount, Unit amountUnit, Unit outputUnit)
        {
            return amount * GetBaseUnitsPer1Unit(amountUnit) / GetBaseUnitsPer1Unit(outputUnit);
        }

        private static float GetBaseUnitsPer1Unit(Unit unit)
        {
            return unit switch
            {
                Unit.KilometersPerMinuteSquared => 1f,
                Unit.MetersPerSecondSquared => 3.6f,
                Unit.G => 35.316f,
                _ => throw new ArgumentOutOfRangeException(nameof(unit), unit, null)
            };
        }
        
        public enum Unit
        {
            KilometersPerMinuteSquared,
            MetersPerSecondSquared,
            G
        }

        public bool Equals(AccelerationAmount other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return valueInOwnUnit.Equals(other.valueInOwnUnit) && valueUnit == other.valueUnit;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((AccelerationAmount)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(valueInOwnUnit, (int)valueUnit);
        }
    }
}