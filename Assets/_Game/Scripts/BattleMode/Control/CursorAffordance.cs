using System;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Control
{
    public class CursorAffordance : MonoBehaviour
    {
        [SerializeField] private PlayerFlotillaControl playerFlotillaControl = null;
        [SerializeField] private CursorDetails[] cursorList = null;

        private const CursorMode CURSOR_MODE = CursorMode.Auto;

        private readonly Dictionary<CursorHoverState, CursorDetails> _cursorDetailsByCursorHoverState = new Dictionary<CursorHoverState, CursorDetails>();

        private void Awake()
        {
            InitializeCursorDictionary();
        }

        private void InitializeCursorDictionary()
        {
            foreach (CursorDetails cursorDetails in cursorList)
            {
                _cursorDetailsByCursorHoverState.Add(cursorDetails.hoverState, cursorDetails);
            }
        }

        private void OnEnable()
        {
            SetCursorToState(playerFlotillaControl.CursorHoverState.Value);
            playerFlotillaControl.CursorHoverState.OnValueChanged += OnCursorHoverStateChanged;
        }

        private void OnDisable()
        {
            playerFlotillaControl.CursorHoverState.OnValueChanged -= OnCursorHoverStateChanged;
            ResetCursor();
        }

        private void OnDestroy()
        {
            ResetToUnityDefaultCursor();
        }

        private void OnCursorHoverStateChanged(CursorHoverState oldValue, CursorHoverState newValue)
        {
            SetCursorToState(newValue);
        }

        private void SetCursorToState(CursorHoverState state)
        {
            if (!playerFlotillaControl.IsOwner) return;
            
            CursorDetails cursorDetails = _cursorDetailsByCursorHoverState[state];
            Cursor.SetCursor(cursorDetails.cursorImage, cursorDetails.offset, CURSOR_MODE);
        }

        private void ResetCursor()
        {
            SetCursorToState(CursorHoverState.Default);
        }
        
        private void ResetToUnityDefaultCursor()
        {
            Cursor.SetCursor(null, Vector2.zero, CURSOR_MODE);
        }
        
        [System.Serializable]
        private struct CursorDetails
        {
            public CursorHoverState hoverState;
            public Texture2D cursorImage;
            public Vector2 offset;
        }
    }
    
    public enum CursorHoverState
    {
        Default = 0,
        SelectableFriendly = 1,
        SelectableEnemy = 4,
        Movable = 2,
        Attackable = 3
    }
}
