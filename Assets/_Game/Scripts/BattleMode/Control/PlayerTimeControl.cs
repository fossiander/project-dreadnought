using _Game.Scripts.Utils;
using BattleMode.Core;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Control
{
    public class PlayerTimeControl : NetworkBehaviour
    {
        private readonly LazyObjectFinder<BattleTimeManager> _battleTimeManager =
            new LazyObjectFinder<BattleTimeManager>();

        private void Update()
        {
            if (!IsLocalPlayer) return;
            if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) return;

            UpdateTimeManagement();
        }

        private void UpdateTimeManagement()
        {
            if (Input.GetKeyDown(KeyCode.Space) ||
                Input.GetKeyDown(KeyCode.Pause) ||
                Input.GetKeyDown(KeyCode.P))
                _battleTimeManager.Get().TogglePlayingServerRpc();

            if (Input.GetKeyDown(KeyCode.Plus) ||
                Input.GetKeyDown(KeyCode.KeypadPlus))
                _battleTimeManager.Get().ChangeTimeExponentServerRpc(1);

            if (Input.GetKeyDown(KeyCode.Minus) ||
                Input.GetKeyDown(KeyCode.KeypadMinus))
                _battleTimeManager.Get().ChangeTimeExponentServerRpc(-1);
        }
    }
}