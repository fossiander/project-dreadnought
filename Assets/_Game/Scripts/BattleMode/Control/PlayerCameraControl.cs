using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Control
{
    public class PlayerCameraControl : NetworkBehaviour
    {
        [SerializeField] private float cameraMoveSpeed = 2f;
        [SerializeField] private float cameraZoomSpeed = 20f;
        [SerializeField] private Vector3 cameraOffset = new Vector3(0,0,-10);

        private Camera _mainCamera;

        private void Awake()
        {
            _mainCamera = Camera.main;
        }

        private void Update()
        {
            if (!IsLocalPlayer) return;
            if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) return;
            
            UpdateCameraMovement();
        }

        private void UpdateCameraMovement()
        {
            float xMovement = Input.GetAxisRaw("Horizontal") * _mainCamera.orthographicSize * cameraMoveSpeed;
            float yMovement = Input.GetAxisRaw("Vertical") * _mainCamera.orthographicSize * cameraMoveSpeed;
            float zMovement = -Input.mouseScrollDelta.y * cameraZoomSpeed;
        
            _mainCamera.transform.Translate(new Vector3(xMovement, yMovement, 0) * Time.unscaledDeltaTime);
            _mainCamera.orthographicSize *= (1 + zMovement * Time.unscaledDeltaTime);
        }
        
        public void CenterCameraOnObject(Transform objectToCenter)
        {
            _mainCamera.transform.position = objectToCenter.position + cameraOffset;
        }
    }
}
