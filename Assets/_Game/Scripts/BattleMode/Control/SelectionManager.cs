﻿using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Core;

namespace BattleMode.Control
{
    public class SelectionManager
    {
        private Player _player;

        public EventVariable<SelectableObject> SelectedObject { get; } = new EventVariable<SelectableObject>();
        public EventVariable<SelectableObject> HighlightedObject { get; } = new EventVariable<SelectableObject>();

        public SelectionManager(Player player)
        {
            _player = player;
            SelectedObject.OnValueChanged += OnSelectionChanged;
            HighlightedObject.OnValueChanged += OnHighlightChanged;
        }

        public bool IsObjectSelected { get; private set; }
        public bool IsPlayerOwnedObjectSelected { get; private set; }

        private void OnSelectionChanged(SelectableObject oldValue, SelectableObject newValue)
        {
            if (oldValue != null) oldValue.Deselect();
            if (newValue != null) newValue.Select();
            UpdateSelectionStatuses();
        }

        private void OnHighlightChanged(SelectableObject oldValue, SelectableObject newValue)
        {
            if (oldValue != null) oldValue.Unhighlight();
            if (newValue != null) newValue.Highlight();
        }

        private void UpdateSelectionStatuses()
        {
            IsObjectSelected = SelectedObject.Value != null;
            IsPlayerOwnedObjectSelected = SelectedObject.Value.OwnerID == _player.playerId.Value;
        }

        public bool IsObjectPlayerOwned(SelectableObject selectableObject)
        {
            if (selectableObject == null) return false;
            return selectableObject.OwnerID == _player.playerId.Value;
        }
        
        public void SelectObject(SelectableObject obj)
        {
            SelectedObject.Value = obj;
        }

        public void DeselectObject()
        {
            SelectedObject.Value = null;
        }
        
        public void HighlightObject(SelectableObject obj)
        {
            HighlightedObject.Value = obj;
        }

        public void UnhighlightObject()
        {
            HighlightedObject.Value = null;
        }
    }
}