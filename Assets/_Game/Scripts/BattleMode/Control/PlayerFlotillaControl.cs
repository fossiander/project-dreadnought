using System;
using System.Collections.Generic;
using _Game.Scripts.Utils;
using _Game.Scripts.Utils.Event_Utils;
using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Combat;
using BattleMode.Combat.Projectiles;
using BattleMode.Core;
using BattleMode.Movement;
using BattleMode.UI;
using BattleMode.UnitConversion;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Control
{
    public class PlayerFlotillaControl : NetworkBehaviour
    {
        [SerializeField] private Player player;
        [SerializeField] private LayerMask selectionMask = ~0;
        [SerializeField] private float velocityDragFactor = 1f;
        
        private bool _isMovementDragging;
        private Vector2 _dragStartPosition;
        
        private Plane _movementPlane;
        private Camera _mainCamera;

        private readonly LazyObjectFinder<UIWindowManager> _uiWindowManager = new LazyObjectFinder<UIWindowManager>();
        
        public EventVariable<CursorHoverState> CursorHoverState { get; } = new EventVariable<CursorHoverState>();

        public SelectionManager SelectionManager { get; private set; }

        private Vector2 _mousePositionOnMovementPlane;
        private SelectableObject _objectUnderMouseCursor;
        
        private void Awake()
        {
            _movementPlane = new Plane(Vector3.up, Vector3.zero);
            _mainCamera = Camera.main;
            SelectionManager = new SelectionManager(player);
        }

        private void Update()
        {
            if (!IsLocalPlayer) return;
            if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) return;
            
            UpdateInput();
        }

        private void UpdateInput()
        {
            UpdatePositionAndObjectUnderMouseCursor();
            UpdateAffordance();
            
            if (Input.GetMouseButtonDown(1))
            {
                _isMovementDragging = false;
                SelectionManager.DeselectObject();
                return;
            }

            if (Input.GetKeyDown(KeyCode.Backspace))
            {
                _isMovementDragging = false;
                StopSelectedObject();
                return;
            }

            if (_isMovementDragging)
            {
                if (Input.GetMouseButtonUp(0)) CompleteMovementDragging();
                else UpdateMovementDragging();
            }

            if (_objectUnderMouseCursor != null)
            {
                if (Input.GetMouseButtonDown(0)) HandleClickOnObject();
            }
            else
            {
                if (SelectionManager.IsPlayerOwnedObjectSelected)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)) StartMoveOrderAtMaximumVelocity(_mousePositionOnMovementPlane);
                        else StartMovementDragging();
                    }
                }
            }
        }

        private void UpdatePositionAndObjectUnderMouseCursor()
        {
            var ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
            var hit = Physics.Raycast(ray, out var hitInfo, float.MaxValue, selectionMask);

            _movementPlane.Raycast(ray, out float distanceToMovementPlane);
            _mousePositionOnMovementPlane = ray.GetPoint(distanceToMovementPlane);

            if (hit && hitInfo.transform.TryGetComponent<SelectableObject>(out var mouseOverObject))
                _objectUnderMouseCursor = mouseOverObject;
            else _objectUnderMouseCursor = null;
        }

        private void UpdateAffordance()
        {
            SelectionManager.HighlightObject(_objectUnderMouseCursor);
            
            if (_objectUnderMouseCursor == null)
            {
                if (SelectionManager.IsPlayerOwnedObjectSelected) SetCursorHoverState(Control.CursorHoverState.Movable);
                else SetCursorHoverState(Control.CursorHoverState.Default);
            }
            else
            {
                if (SelectionManager.IsObjectPlayerOwned(_objectUnderMouseCursor)) SetCursorHoverState(Control.CursorHoverState.SelectableFriendly);
                else if (SelectionManager.IsObjectSelected) SetCursorHoverState(Control.CursorHoverState.Attackable);
                else SetCursorHoverState(Control.CursorHoverState.SelectableEnemy);
            }
        }
        
        private void StartMovementDragging()
        {
            _isMovementDragging = true;
            _dragStartPosition = _mousePositionOnMovementPlane;
            EventSystem.TriggerEvent(new MovementCommandDragStarted()
            {
                currentPosition = SelectionManager.SelectedObject.Value.transform.position,
                movePosition = _mousePositionOnMovementPlane
            });
        }

        private void UpdateMovementDragging()
        {
            Vector2 targetVelocity = (_mousePositionOnMovementPlane - _dragStartPosition) * velocityDragFactor;
            VelocityAmount targetVelocityAmount = new VelocityAmount(targetVelocity.magnitude, VelocityAmount.CORE_UNIT);

            EventSystem.TriggerEvent(new MovementCommandDragging()
            {
                currentPosition = SelectionManager.SelectedObject.Value.transform.position,
                movePosition = _dragStartPosition,
                velocityPosition = _mousePositionOnMovementPlane,
                targetVelocity = targetVelocityAmount
            });
        }

        private void CompleteMovementDragging()
        {
            Vector2 targetVelocity = (_mousePositionOnMovementPlane - _dragStartPosition) * velocityDragFactor;

            _isMovementDragging = false;
            StartMoveOrderAtTargetVelocity(_dragStartPosition, targetVelocity);
            EventSystem.TriggerEvent(new MovementCommandDragCompleted());
        }

        private void HandleClickOnObject()
        {
            if (SelectionManager.IsPlayerOwnedObjectSelected) SelectionManager.SelectObject(_objectUnderMouseCursor);
            else if (_objectUnderMouseCursor != SelectionManager.SelectedObject.Value)
            {
                if (SelectionManager.IsPlayerOwnedObjectSelected)
                {
                    TriggerAttackOnObject(_objectUnderMouseCursor);
                }
            }
        }

        private void TriggerAttackOnObject(SelectableObject target)
        {
            if (target.TryGetComponent(out Flotilla flotilla))
            {
                OpenSelectShipTargetMenu(flotilla);
                return;
            }

            if (target.TryGetComponent(out Projectile projectile))
            {
                TriggerAttackCommand(projectile.GetComponent<AttackableObject>());
            }
        }

        private void OpenSelectShipTargetMenu(Flotilla flotilla)
        {
            UIShipTargetSelectionMenu targetSelectionMenu = _uiWindowManager.Get().OpenWindow<UIShipTargetSelectionMenu>();
            targetSelectionMenu.SetReference(flotilla, ship => TriggerAttackCommand(ship.GetComponent<AttackableObject>()), null);
        }
        
        private void TriggerAttackCommand(AttackableObject attackableObject)
        {
            AttackCommandReceiver attackCommandReceiver = SelectionManager.SelectedObject.Value.GetComponent<AttackCommandReceiver>();
            attackCommandReceiver.SetAttackTarget(attackableObject);
        }
        
        private void StartMoveOrderAtTargetVelocity(Vector2 targetPosition, Vector2 targetVelocity)
        {
            var movementController = SelectionManager.SelectedObject.Value.GetComponent<MovementCommandReceiver>();
            movementController.MoveToLocationAtTargetVelocity(targetPosition, targetVelocity);
        }

        private void StartMoveOrderAtMaximumVelocity(Vector2 targetPosition)
        {
            var movementController = SelectionManager.SelectedObject.Value.GetComponent<MovementCommandReceiver>();
            movementController.MoveToLocationAtMaximumVelocity(targetPosition);
        }

        private void StopSelectedObject()
        {
            if (!SelectionManager.IsObjectSelected) return;

            MovementCommandReceiver movementCommandReceiver = SelectionManager.SelectedObject.Value.GetComponent<MovementCommandReceiver>();
            movementCommandReceiver.BreakToStop();
            
            AttackCommandReceiver attackCommandReceiver = SelectionManager.SelectedObject.Value.GetComponent<AttackCommandReceiver>();
            attackCommandReceiver.ClearAttackTarget();
        }

        private void SetCursorHoverState(CursorHoverState state)
        {
            CursorHoverState.Value = state;
        }
    }

    public class MovementCommandDragStarted : Event<MovementCommandDragStarted>
    {
        public Vector2 currentPosition;
        public Vector2 movePosition;
    }

    public class MovementCommandDragging : Event<MovementCommandDragging>
    {
        public Vector2 currentPosition;
        public Vector2 movePosition;
        public Vector2 velocityPosition;
        public VelocityAmount targetVelocity;
    }
    
    public class MovementCommandDragCompleted : Event<MovementCommandDragCompleted>
    {
    }
}
