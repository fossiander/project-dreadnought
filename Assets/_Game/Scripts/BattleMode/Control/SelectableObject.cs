using System;
using BattleMode.Core;
using UnityEngine;
using UnityEngine.Events;

namespace BattleMode.Control
{
    public class SelectableObject : MonoBehaviour
    {
        [SerializeField] private UnityEvent onSelected; 
        [SerializeField] private UnityEvent onDeselected;
        [SerializeField] private UnityEvent onStartHover;
        [SerializeField] private UnityEvent onEndHover;
        
        public UnityEvent OnSelected => onSelected;

        private OwnedObject _ownedObject;

        public int OwnerID => _ownedObject.OwnerID;

        private void Awake()
        {
            _ownedObject = GetComponent<OwnedObject>();
        }

        public void Select()
        {
            onSelected?.Invoke();
        }
    
        public void Deselect()
        {
            onDeselected?.Invoke();
        }

        public void Highlight()
        {
            onStartHover?.Invoke();
        }

        public void Unhighlight()
        {
            onEndHover?.Invoke();
        }
    }
}
