using DG.Tweening;
using UnityEngine;

namespace BattleMode.Cosmetics
{
    public class PulsingSize : MonoBehaviour
    {
        [SerializeField] private float sizeMax = 1.25f;
        [SerializeField] private float duration = 0.5f;

        private Vector3 _baseScale;

        private void Awake()
        {
            _baseScale = transform.localScale;
        }

        public void OnEnable()
        {
            transform.DOScale(_baseScale * sizeMax, duration).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
        }

        public void OnDisable()
        {
            transform.DOKill();
            transform.localScale = _baseScale;
        }
    }
}

