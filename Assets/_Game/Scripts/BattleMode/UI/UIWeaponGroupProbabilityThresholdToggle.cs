﻿using _Game.Scripts.Utils;
using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Combat;

namespace BattleMode.UI
{
    public class UIWeaponGroupProbabilityThresholdToggle : UIEnumToggleGroup<WeaponGroup.HitProbabilityThreshold>
    {
        private WeaponGroup _reference;
        private AttackCommandReceiver _attackCommandReceiver;
        
        public void SetReference(WeaponGroup reference)
        {
            _reference = reference;
            _attackCommandReceiver = _reference.AttackHandler.GetComponent<AttackCommandReceiver>();
            
            SetReference(reference.ProbabilityThreshold);
        }

        protected override void LeftButtonAction(WeaponGroup.HitProbabilityThreshold value, bool shift, bool ctrl)
        {
            _attackCommandReceiver.SetAutoTargetAggressiveness(value.NextItem(), new[] {_reference.WeaponGroupID});
        }
        
        protected override void RightButtonAction(WeaponGroup.HitProbabilityThreshold value, bool shift, bool ctrl)
        {
            _attackCommandReceiver.SetAutoTargetAggressiveness(value.PreviousItem(), new[] {_reference.WeaponGroupID});
        }
    }
}