using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Core;
using UnityEngine;

namespace BattleMode.UI
{
    public class UIFlotillaShipList : UIList<Ship>
    {
        public void SetReferenceFlotilla(Flotilla flotilla)
        {
            SetReference(flotilla.Ships);
        }
    }
}
