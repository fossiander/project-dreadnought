using System;
using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Core;
using TMPro;
using UnityEngine;

namespace BattleMode.UI
{
    public class UIShipTargetSelectionMenu : UIWindow
    {
        [SerializeField] private UIShipTargetSelectionList shipList = null;
        [SerializeField] private UIButton confirmButton = null;
        [SerializeField] private UIButton cancelButton = null;
        [SerializeField] private UIButton xButton = null;
        [SerializeField] private TextMeshProUGUI titleTextMesh = null;

        private Action<Ship> _actionOnConfirm;
        private Action _actionOnCancel;

        public EventVariable<Ship> selectedShip = new EventVariable<Ship>();
        
        public void SetReference(Flotilla flotilla, Action<Ship> actionOnConfirm, Action actionOnCancel)
        {
            if (flotilla.Ships.Count > 0) selectedShip.Value = flotilla.Ships[0];
            
            shipList.SetReferenceFlotilla(flotilla);

            foreach (var item in shipList.ActiveListItems)
            {
                if (item is not UIShipTargetSelectionListItem shipTargetSelectionListItem) continue;
                
                shipTargetSelectionListItem.SetLeftClickAction(ship => selectedShip.Value = ship);
                shipTargetSelectionListItem.SetSelectedShipReference(selectedShip);
            }

            titleTextMesh.text = $"Select target in {flotilla.FlotillaName.Value}";

            _actionOnConfirm = actionOnConfirm;
            _actionOnCancel = actionOnCancel;
        }

        private void Awake()
        {
            confirmButton.leftClickAction.AddListener(OnConfirmClicked);
            cancelButton.leftClickAction.AddListener(OnCancelClicked);
            xButton.leftClickAction.AddListener(OnCancelClicked);
        }

        private void OnConfirmClicked(bool shift, bool ctrl)
        {
            _actionOnConfirm?.Invoke(selectedShip.Value);
        }

        private void OnCancelClicked(bool shift, bool ctrl)
        {
            _actionOnCancel?.Invoke();
        }
    }
}
