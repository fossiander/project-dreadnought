﻿using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Combat;

namespace BattleMode.UI
{
    public class UIWeaponGroupTargetModeShowHide : UIValueStatusShowHideEffect<WeaponGroup.TargetMode>
    {
    }
}