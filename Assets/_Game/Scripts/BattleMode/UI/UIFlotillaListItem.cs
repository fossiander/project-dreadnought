﻿using System;
using _Game.Scripts.Utils;
using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Combat;
using BattleMode.Control;
using BattleMode.Core;
using BattleMode.Movement;
using UnityEngine;
using UnityEngine.Events;

namespace BattleMode.UI
{
    public class UIFlotillaListItem : UIListItem<Flotilla>
    {
        [SerializeField] private UIEventTextSetter flotillaNameSetter = null;
        [SerializeField] private UIFlotillaShipList flotillaShipList = null;
        [SerializeField] private UIButton flotillaSelectionButton = null;

        [Header("Status markers")] 
        [SerializeField] private UIBoolStatusShowHide flotillaStatusMoving = null;
        [SerializeField] private UIBoolStatusShowHide flotillaStatusAttacking = null;
        [SerializeField] private UIBoolStatusShowHide flotillaStatusUnderFire = null;
        
        private UnityAction<bool, bool> _selectAction;
        private UnityAction<bool, bool> _centerAction;
        private UnityAction _enterAction;
        private UnityAction _exitAction;

        private readonly LazyObjectFinder<PlayerFlotillaControl> _playerFlotillaControl = new LazyObjectFinder<PlayerFlotillaControl>();
        private readonly LazyObjectFinder<PlayerCameraControl> _playerCameraControl = new LazyObjectFinder<PlayerCameraControl>();

        public override void SetReference(Flotilla reference)
        {
            if(_selectAction != null) flotillaSelectionButton.leftClickAction.RemoveListener(_selectAction);
            if(_centerAction != null) flotillaSelectionButton.leftDoubleClickAction.RemoveListener(_centerAction);
            if(_enterAction != null) flotillaSelectionButton.enterAction.RemoveListener(_enterAction);
            if(_exitAction != null) flotillaSelectionButton.exitAction.RemoveListener(_exitAction);

            flotillaNameSetter.SetReference(reference.FlotillaName);
            flotillaShipList.SetReferenceFlotilla(reference);

            flotillaStatusMoving.SetReference(reference.GetComponent<MovableEntity>().IsMoving, true);
            flotillaStatusAttacking.SetReference(reference.GetComponent<AttackHandler>().IsAttacking, true);
            flotillaStatusUnderFire.SetReference(reference.GetComponent<FlotillaShipHealthTracker>().IsTakingDamage, true);
            
            _selectAction = (_, _) => _playerFlotillaControl.Get().SelectionManager.SelectObject(reference.GetComponent<SelectableObject>()); 
            _centerAction = (_,_)=> _playerCameraControl.Get().CenterCameraOnObject(reference.transform); 
            _enterAction = () => _playerFlotillaControl.Get().SelectionManager.HighlightObject(reference.GetComponent<SelectableObject>());
            _exitAction = () => _playerFlotillaControl.Get().SelectionManager.UnhighlightObject();
            
            flotillaSelectionButton.leftClickAction.AddListener(_selectAction);
            flotillaSelectionButton.leftDoubleClickAction.AddListener(_centerAction);
            flotillaSelectionButton.enterAction.AddListener(_enterAction);
            flotillaSelectionButton.exitAction.AddListener(_exitAction);
        }
    }
}