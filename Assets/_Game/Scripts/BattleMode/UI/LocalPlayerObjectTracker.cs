﻿using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Core;
using Unity.Netcode;

namespace BattleMode.UI
{
    public class LocalPlayerObjectTracker : NetworkBehaviour
    {
        public EventList<Flotilla> OwnedFlotillas { get; private set; } = new EventList<Flotilla>();

        private int _localPlayerID;

        public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();
            
            RefreshOwnedFlotillaList();
            Player.LocalPlayerID.OnValueChanged += OnLocalPlayerChanged;
            Flotilla.AllFlotillas.OnListChanged += RefreshOwnedFlotillaList;
        }

        public override void OnNetworkDespawn()
        {
            base.OnNetworkDespawn();
            
            Player.LocalPlayerID.OnValueChanged -= OnLocalPlayerChanged;
            Flotilla.AllFlotillas.OnListChanged -= RefreshOwnedFlotillaList;
        }

        private void OnLocalPlayerChanged(int oldValue, int newValue)
        {
            RefreshOwnedFlotillaList();
        }

        private void RefreshOwnedFlotillaList()
        {
            OwnedFlotillas.Clear();
            foreach (Flotilla flotilla in Flotilla.AllFlotillas)
            {
                if (!flotilla.TryGetComponent(out OwnedObject ownedObject)) continue;
                
                if (ownedObject.OwnerID == Player.LocalPlayerID.Value) OwnedFlotillas.Add(flotilla);
            }
        }
    }
}