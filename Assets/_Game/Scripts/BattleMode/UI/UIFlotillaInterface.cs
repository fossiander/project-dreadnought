using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Control;
using BattleMode.Core;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.UI
{
    public class UIFlotillaInterface : MonoBehaviour
    {
        [SerializeField] private UIFlotillaShipList shipList = null;
        [SerializeField] private UIFlotillaWeaponGroupList weaponGroupList = null;
        private PlayerFlotillaControl _localPlayerFlotillaControl;

        private void Start()
        {
            if (NetworkManager.Singleton.IsClient) Initialize();
            NetworkManager.Singleton.OnServerStarted += Initialize;
            
            gameObject.SetActive(false);
        }

        private void Initialize()
        {
            _localPlayerFlotillaControl = NetworkManager.Singleton.LocalClient.PlayerObject.GetComponent<PlayerFlotillaControl>();
            _localPlayerFlotillaControl.SelectionManager.SelectedObject.OnValueChanged += OnSelectionChanged;
            UpdateReferenceFlotilla(_localPlayerFlotillaControl.SelectionManager.SelectedObject.Value);
        }

        private void OnSelectionChanged(SelectableObject oldValue, SelectableObject newValue)
        {
            UpdateReferenceFlotilla(newValue);
        }

        private void UpdateReferenceFlotilla(SelectableObject selectedObject)
        {
            Flotilla flotilla = null;
            if (selectedObject != null && selectedObject.OwnerID == Player.LocalPlayerID.Value) flotilla = selectedObject.GetComponent<Flotilla>();

            gameObject.SetActive(flotilla != null);

            if (flotilla != null)
            {
                shipList.SetReferenceFlotilla(flotilla);
                weaponGroupList.SetReferenceFlotilla(flotilla);
            }
        }
    }
}
