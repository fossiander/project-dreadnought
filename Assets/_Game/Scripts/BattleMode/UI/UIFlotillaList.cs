using System;
using _Game.Scripts.Utils;
using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Core;
using BattleMode.Spawning;
using Unity.Netcode;

namespace BattleMode.UI
{
    public class UIFlotillaList : UIList<Flotilla>
    {
        private void Start()
        {
            if (NetworkManager.Singleton.IsClient) Initialize();
            
            NetworkManager.Singleton.OnServerStarted += Initialize;
        }

        private void Initialize()
        {
            var localPlayerObjectTracker = FindObjectOfType<LocalPlayerObjectTracker>();
            
            SetReference(localPlayerObjectTracker.OwnedFlotillas);
        }
    }
}
