﻿using _Game.Scripts.Utils;
using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Combat;

namespace BattleMode.UI
{
    public class UIWeaponGroupTargetModeToggle : UIEnumToggleGroup<WeaponGroup.TargetMode>
    {
        private WeaponGroup _reference;
        private AttackCommandReceiver _attackCommandReceiver;
        
        public void SetReference(WeaponGroup reference)
        {
            _reference = reference;
            _attackCommandReceiver = _reference.AttackHandler.GetComponent<AttackCommandReceiver>();
            
            SetReference(reference.AutoTargetMode);
        }

        protected override void LeftButtonAction(WeaponGroup.TargetMode value, bool shift, bool ctrl)
        {
            _attackCommandReceiver.SetAutoTargetMode(value.NextItem(), new[] {_reference.WeaponGroupID});
        }
        
        protected override void RightButtonAction(WeaponGroup.TargetMode value, bool shift, bool ctrl)
        {
            _attackCommandReceiver.SetAutoTargetMode(value.PreviousItem(), new[] {_reference.WeaponGroupID});
        }
    }
}