using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Combat;
using BattleMode.Control;
using TMPro;
using UnityEngine;

namespace BattleMode.UI
{
    public class UIFlotillaWeaponGroupListItem : UIListItem<WeaponGroup>
    {
        [SerializeField] private TextMeshProUGUI weaponGroupNameTextMesh = null;
        [SerializeField] private TextMeshProUGUI weaponGroupTargetTextMesh = null;
        [SerializeField] private UICheckBox weaponGroupSelectionCheckBox = null;
        [SerializeField] private UIBoolToggle autoTargetActiveToggle = null;
        [SerializeField] private UIBoolStatusShowHide autoTargetSettings = null;
        [SerializeField] private UIWeaponGroupTargetModeToggle targetModeToggle = null;
        [SerializeField] private UIWeaponGroupProbabilityThresholdToggle hitProbabilityThresholdToggle = null;

        private WeaponGroup _reference;
        
        public override void SetReference(WeaponGroup reference)
        {
            if (_reference != null)
            {
                _reference.WeaponGroupName.OnValueChanged -= OnNameChanged;
                _reference.ManualTarget.OnValueChanged -= OnManualTargetChanged;
            }

            _reference = reference;
            
            _reference.WeaponGroupName.OnValueChanged += OnNameChanged;
            _reference.ManualTarget.OnValueChanged += OnManualTargetChanged;
            
            UpdateNameDisplay();
            UpdateTargetDisplay();
            
            weaponGroupSelectionCheckBox.SetReference(reference.IsSelected);
            autoTargetActiveToggle.SetReference(reference.IsAutoTargetingActive, ToggleAutoTargetActive);
            autoTargetSettings.SetReference(reference.IsAutoTargetingActive, true);
            targetModeToggle.SetReference(reference);
            hitProbabilityThresholdToggle.SetReference(reference);
        }

        private void OnNameChanged(string oldValue, string newValue)
        {
            UpdateNameDisplay();
        }

        private void UpdateNameDisplay()
        {
            weaponGroupNameTextMesh.text = _reference.WeaponGroupName.Value;
        }

        private void OnManualTargetChanged(AttackableObject oldValue, AttackableObject newValue)
        {
            UpdateTargetDisplay();
        }

        private void UpdateTargetDisplay()
        {
            string targetName;

            if (_reference.ManualTarget.Value != null) targetName = _reference.ManualTarget.Value.name;
            else if (_reference.IsAutoTargetingActive.Value) targetName = "Auto targeting";
            else targetName = "None";

            weaponGroupTargetTextMesh.text = targetName;
        }

        private void ToggleAutoTargetActive(bool isActive, bool shift, bool ctrl)
        {
            _reference.AttackHandler.GetComponent<AttackCommandReceiver>().SetAutoTargetingActive(!isActive, new []{_reference.WeaponGroupID});
        }
    }
}
