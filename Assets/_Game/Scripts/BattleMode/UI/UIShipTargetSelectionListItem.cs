using System;
using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Core;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace BattleMode.UI
{
    public class UIShipTargetSelectionListItem : UIListItem<Ship>
    {
        [SerializeField] private UIButton uiButton = null;
        [SerializeField] private TextMeshProUGUI shipNameTextMesh = null;
        
        private Ship _reference;
        private UnityAction<bool,bool> _leftClickAction;
        private EventVariable<Ship> _selectedShipVariable;

        public override void SetReference(Ship reference)
        {
            _reference = reference;
            shipNameTextMesh.text = $"<b>{reference.shipName}</b> ({reference.shipClassName})";
        }

        public void SetLeftClickAction(Action<Ship> action)
        {
            if (_leftClickAction != null) uiButton.leftClickAction.RemoveListener(_leftClickAction);
            
            _leftClickAction = (_,_) => action(_reference);
            uiButton.leftClickAction.AddListener(_leftClickAction);
        }

        public void SetSelectedShipReference(EventVariable<Ship> selectedShip)
        {
            if (_selectedShipVariable != null) _selectedShipVariable.OnValueChanged -= OnSelectedShipChanged;
            _selectedShipVariable = selectedShip;
            _selectedShipVariable.OnValueChanged += OnSelectedShipChanged;
            UpdateButtonPressed();
        }

        private void OnSelectedShipChanged(Ship oldValue, Ship newValue)
        {
            UpdateButtonPressed();
        }

        private void UpdateButtonPressed()
        {
            uiButton.SetPressed(_selectedShipVariable.Value == _reference);
        }
    }
}
