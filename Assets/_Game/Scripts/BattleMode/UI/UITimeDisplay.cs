using System;
using _Game.Scripts.Utils;
using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Core;
using UnityEngine;
using TMPro;

namespace BattleMode.UI
{
    public class UITimeDisplay : MonoBehaviour
    {
        private readonly LazyObjectFinder<BattleTimeManager> _battleTimeManager = new LazyObjectFinder<BattleTimeManager>();

        [SerializeField] private TextMeshProUGUI timeDisplayTextMesh = null;
        
        [Header("Play/pause button")] 
        [SerializeField] private UIButton playPauseButton = null;
        [SerializeField] private UIBoolStatusShowHide playStatusImage = null;
        [SerializeField] private UIBoolStatusShowHide pauseStatusImage = null;

        [Header("Speed buttons")] 
        [SerializeField] private UIButton[] speedButtons;
        [SerializeField] private UIIntStatusPressButtonEffect[] speedButtonPressEffects;

        private void Awake()
        {
            playPauseButton.leftClickAction.AddListener(OnLeftClick);
            playStatusImage.SetReference(_battleTimeManager.Get().IsPlaying, true);
            pauseStatusImage.SetReference(_battleTimeManager.Get().IsPlaying, false);

            for (int i = 0; i < speedButtons.Length; i++)
            {
                int exponent = i + _battleTimeManager.Get().MinTimeExponent;
                speedButtons[i].leftClickAction.AddListener((_,_) => _battleTimeManager.Get().SetTimeExponentServerRpc(exponent));
                speedButtonPressEffects[i].SetReference(_battleTimeManager.Get().CurrentTimeExponent, exponent);
            }
        }

        private void OnLeftClick(bool shift, bool ctrl)
        {
            _battleTimeManager.Get().TogglePlayingServerRpc();
        }

        private void Update()
        {
            TimeSpan elapsedTimeSpan = _battleTimeManager.Get().ElapsedTimeSpan;
            
            timeDisplayTextMesh.text = $"D{(elapsedTimeSpan.Days + 1):0} {elapsedTimeSpan.Hours:00}:{elapsedTimeSpan.Minutes:00}";
        }
    }
}
