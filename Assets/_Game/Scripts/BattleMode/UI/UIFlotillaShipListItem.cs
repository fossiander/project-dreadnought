using System;
using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Combat;
using BattleMode.Core;
using UnityEngine;

namespace BattleMode.UI
{
    public class UIFlotillaShipListItem : UIListItem<Ship>
    {
        [SerializeField] private UIEventTextSetter shipNameSetter = null;
        [SerializeField] private UIFloatValueBarSetter armorValueSetter = null;
        [SerializeField] private UIFloatValueBarSetter hullValueSetter = null;

        private string refName;
        
        public override void SetReference(Ship reference)
        {
            refName = reference.shipName;
            
            ShipHealth shipHealth = reference.GetComponent<ShipHealth>();
            
            shipNameSetter.SetReference(reference.ShipName);
            armorValueSetter.SetReferences(shipHealth.Armor, shipHealth.MaxArmor);
            hullValueSetter.SetReferences(shipHealth.Hull, shipHealth.MaxHull);
        }
    }
}
