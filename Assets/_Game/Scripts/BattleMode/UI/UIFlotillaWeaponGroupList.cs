using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Combat;
using BattleMode.Core;
using UnityEngine;

namespace BattleMode.UI
{
    public class UIFlotillaWeaponGroupList : UIList<WeaponGroup>
    {
        public void SetReferenceFlotilla(Flotilla flotilla)
        {
            AttackHandler attackHandler = flotilla.GetComponent<AttackHandler>();
            
            SetReference(attackHandler.WeaponGroups);
        }
    }
}
