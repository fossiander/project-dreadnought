﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Control;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace BattleMode.Core
{
    public class Player : NetworkBehaviour
    {
        public static EventVariable<int> LocalPlayerID { get; } = new EventVariable<int>();
        public static EventList<Player> AllPlayers { get; } = new EventList<Player>();

        public static Dictionary<int, Player> ServerPlayersByIDs { get; } = new Dictionary<int, Player>();

        [SerializeField] private PlayerType playerType = PlayerType.Human;
        [SerializeField] private string displayName = "Player";

        public NetworkVariable<int> playerId = new NetworkVariable<int>();

        public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();

            if (IsServer)
            {
                playerId.Value = ServerPlayersByIDs.Count;
                ServerPlayersByIDs.Add(playerId.Value, this);
            }
            
            playerId.OnValueChanged += OnPlayerIdChanged;
            UpdateName();

            UpdateLocalPlayerID();
            
            AllPlayers.Add(this);
        }

        public override void OnNetworkDespawn()
        {
            base.OnNetworkDespawn();

            playerId.OnValueChanged -= OnPlayerIdChanged;

            if (IsServer) ServerPlayersByIDs.Remove(playerId.Value);

            if (IsLocalPlayer) LocalPlayerID.Value = -1;

            AllPlayers.Remove(this);
        }

        private void UpdateLocalPlayerID()
        {
            if (IsLocalPlayer) LocalPlayerID.Value = playerId.Value; 
        }

        private void OnPlayerIdChanged(int previousID, int newId)
        {
            UpdateName();
            UpdateLocalPlayerID();
            
            if (IsServer) ServerPlayersByIDs.Remove(previousID);
            if (IsServer) ServerPlayersByIDs.Add(newId, this);
        }
        
        public static Player GetPlayerWithId(int id)
        {
            return ServerPlayersByIDs.TryGetValue(id, out Player player) ? player : null;
        }

        private void UpdateName()
        {
            displayName = $"Player {playerId.Value} ({playerType.ToString()})";
            transform.name = displayName;
        }
        
        public enum PlayerType
        {
            Human,
            AI
        }
    }
}