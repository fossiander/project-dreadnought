﻿using System;
using _Game.Scripts.Utils;
using _Game.Scripts.Utils.UI_Utils;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Core
{
    public class OwnedObject : NetworkBehaviour
    {
        private readonly NetworkVariable<int> _ownerId = new NetworkVariable<int>();
        public int OwnerID => _ownerId.Value;

        private readonly LazyObjectFinder<AllianceManager> _allianceManager = new LazyObjectFinder<AllianceManager>();
        public static EventList<OwnedObject> AllOwnedObjects { get; } = new EventList<OwnedObject>();
        public static event Action<OwnedObject> OnObjectOwnershipChanged;

        public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();
            
            ExecuteOwnershipChange();
            
            _ownerId.OnValueChanged += OnOwnerChanged;
            Player.LocalPlayerID.OnValueChanged += OnLocalPlayerChanged;
            
            AllOwnedObjects.Add(this);
        }

        public override void OnNetworkDespawn()
        {
            base.OnNetworkDespawn();
            
            _ownerId.OnValueChanged -= OnOwnerChanged;
            Player.LocalPlayerID.OnValueChanged -= OnLocalPlayerChanged;

            AllOwnedObjects.Remove(this);
        }

        private void OnOwnerChanged(int previousValue, int newValue)
        {
            ExecuteOwnershipChange();
        }

        private void OnLocalPlayerChanged(int oldValue, int newValue)
        {
            ExecuteOwnershipChange();
        }
        
        public void SetOwner(int playerId)
        {
            _ownerId.Value = playerId;
        }

        private void ExecuteOwnershipChange()
        {
            UpdateColors();
            OnObjectOwnershipChanged?.Invoke(this);
        }
        
        private void UpdateColors()
        {
            Color color = _allianceManager.Get().GetPlayerColor(_ownerId.Value);
            
            if (TryGetComponent(out IColorChanger colorChanger))
            {
                colorChanger.SetColor(color);
            }
        }

        public bool IsOwnedBy(Player player)
        {
            if (player == null) return false;

            return _ownerId.Value == player.playerId.Value;
        }
    }
}