﻿using System;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Core
{
    public class OwnedObjectOwnershipUpdater : NetworkBehaviour
    {
        public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();
            
            if (!IsServer) return;
            
            UpdateObjectOwnerships();
            Player.AllPlayers.OnListChanged += UpdateObjectOwnerships;
            OwnedObject.AllOwnedObjects.OnItemAdded += UpdateOwnershipForObject;
            OwnedObject.OnObjectOwnershipChanged += UpdateOwnershipForObject;
        }

        public override void OnNetworkDespawn()
        {
            base.OnNetworkDespawn();
            
            if (!IsServer) return;
            
            Player.AllPlayers.OnListChanged -= UpdateObjectOwnerships;
            OwnedObject.AllOwnedObjects.OnItemAdded -= UpdateOwnershipForObject;
            OwnedObject.OnObjectOwnershipChanged -= UpdateOwnershipForObject;
        }
        
        private void UpdateObjectOwnerships()
        {
            foreach (OwnedObject ownedObject in OwnedObject.AllOwnedObjects)
            {
                UpdateOwnershipForObject(ownedObject);
            }
        }

        private void UpdateOwnershipForObject(OwnedObject ownedObject)
        {
            Player owner = Player.GetPlayerWithId(ownedObject.OwnerID);
            
            if (owner == null) ownedObject.NetworkObject.ChangeOwnership(NetworkManager.ServerClientId);
            else ownedObject.NetworkObject.ChangeOwnership(owner.OwnerClientId);
        }
    }
}