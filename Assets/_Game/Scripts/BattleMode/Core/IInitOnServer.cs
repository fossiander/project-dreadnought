﻿namespace BattleMode.Core
{
    public interface IInitOnServer
    {
        public void InitOnServer();
    }
}