using System;
using _Game.Scripts.Utils.UI_Utils;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Core
{
    public class BattleTimeManager : NetworkBehaviour
    {
        [SerializeField] private int minTimeExponent = 0;
        [SerializeField] private int maxTimeExponent = 3;
        [SerializeField] private float timeScaleBase = 4;

        private readonly NetworkVariable<int> _currentTimeExponent = new NetworkVariable<int>(0);
        private readonly NetworkVariable<float> _currentTimeScale = new NetworkVariable<float>(1f);
        private readonly NetworkVariable<bool> _isPlaying = new NetworkVariable<bool>(false);

        public int MinTimeExponent => minTimeExponent;
        public NetworkEventVariable<int> CurrentTimeExponent { get; }
        public NetworkEventVariable<bool> IsPlaying { get; }
        
        public TimeSpan ElapsedTimeSpan => TimeSpan.FromMinutes(Time.time);
        
        private BattleTimeManager()
        {
            CurrentTimeExponent = new NetworkEventVariable<int>(_currentTimeExponent);
            IsPlaying = new NetworkEventVariable<bool>(_isPlaying);
        }

        private void Start()
        {
            UpdateTimeScaleServer();
            UpdateTimeScale();

            _currentTimeScale.OnValueChanged += (_, _) => UpdateTimeScale();
            _isPlaying.OnValueChanged += (_, _) => UpdateTimeScale();
        }

        public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();
            
            if (IsServer) _isPlaying.Value = true;
            UpdateTimeScale();
        }

        [ServerRpc]
        public void SetTimeExponentServerRpc(int exponent)
        {
            SetExponent(exponent);
        }
    
        [ServerRpc]
        public void ChangeTimeExponentServerRpc(int change)
        {
            SetExponent(_currentTimeExponent.Value + change);
        }

        private void SetExponent(int exponent)
        {
            if (!IsServer) return;

            _currentTimeExponent.Value = Mathf.Clamp(exponent, minTimeExponent, maxTimeExponent);
            
            UpdateTimeScaleServer();
        }
        
        private void UpdateTimeScaleServer()
        {
            if (!IsServer) return;
            
            _currentTimeScale.Value = Mathf.Pow(timeScaleBase, _currentTimeExponent.Value);
            UpdateTimeScale();
        }

        private void UpdateTimeScale()
        {
            Time.timeScale = _isPlaying.Value ? _currentTimeScale.Value : 0;
        }
        
        [ServerRpc]
        public void TogglePlayingServerRpc()
        {
            SetIsPlaying(!_isPlaying.Value);
        }

        private void SetIsPlaying(bool isPlaying)
        {
            if (!IsServer) return;

            _isPlaying.Value = isPlaying;
            UpdateTimeScale();
        }
    }
}
