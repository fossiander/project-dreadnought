using System;
using System.Collections.Generic;
using _Game.Scripts.Utils;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Core
{
    public class AllianceManager : NetworkBehaviour
    {
        [SerializeField] private List<int> team1 = new List<int>();
        [SerializeField] private List<int> team2 = new List<int>();
        
        private readonly LazyObjectFinder<PlayerColorSettings> _playerColorSettings = new LazyObjectFinder<PlayerColorSettings>();
        
        public Relationship GetRelationshipWithPlayer(int playerID)
        {
            return GetRelationshipBetweenPlayers(Player.LocalPlayerID.Value, playerID);
        }

        public Relationship GetRelationshipBetweenPlayers(int basePlayerID, int otherPlayerID)
        {
            if (basePlayerID == otherPlayerID) return Relationship.Self;
            if (GetTeamForPlayer(basePlayerID) == 0 || GetTeamForPlayer(otherPlayerID) == 0) return Relationship.Neutral;
            if (GetTeamForPlayer(basePlayerID) == GetTeamForPlayer(otherPlayerID)) return Relationship.Ally;
            return Relationship.Enemy;
        }

        public Color GetPlayerColor(int playerID)
        {
            return _playerColorSettings.Get().GetColorForRelationship(GetRelationshipWithPlayer(playerID));
        }

        public int GetTeamForPlayer(int playerID)
        {
            if (team1.Contains(playerID)) return 1;
            if (team2.Contains(playerID)) return 2;
            return 0;
        }
    }
    
    public enum Relationship
    {
        Self,
        Ally,
        Enemy,
        Neutral
    }
}
