﻿using UnityEngine;

namespace BattleMode.Core
{
    [CreateAssetMenu(fileName = "New Ship Type", menuName = "Ship Type", order = 0)]
    public class ShipType : ScriptableObject
    {
        public string typeName = "New ship type";
        public string typeCode = "NEW";
        public ShipCategory category;
    }
}