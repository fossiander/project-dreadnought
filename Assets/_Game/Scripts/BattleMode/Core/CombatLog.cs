﻿using System.Collections.Generic;
using UnityEngine;

namespace BattleMode.Core
{
    public class CombatLog : MonoBehaviour
    {
        private readonly List<LogMessage> _messages = new List<LogMessage>();

        public void Log(string message)
        {
            _messages.Add(new LogMessage(Time.time, message));
            Debug.Log(message);
        }

        public void Log(string[] messages)
        {
            foreach (string message in messages)
            {
                Log(message);
            }
        }

        private struct LogMessage
        {
            public float time;
            public string message;

            public LogMessage(float time, string message)
            {
                this.time = time;
                this.message = message;
            }
        }
    }
}