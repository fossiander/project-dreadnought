using Shapes;
using UnityEngine;

namespace BattleMode.Core
{
    public class FlotillaColorChanger : MonoBehaviour, IColorChanger
    {
        [SerializeField] private ShapeRenderer[] shapeRenderers;
        
        public void SetColor(Color color)
        {
            foreach (var sr in shapeRenderers)
            {
                sr.Color = color;
            }
        }
    }
}
