﻿using UnityEngine;

namespace BattleMode.Core
{
    public interface IColorChanger
    {
        void SetColor(Color color);
    }
}