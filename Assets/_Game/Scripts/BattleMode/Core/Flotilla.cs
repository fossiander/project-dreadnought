﻿using System;
using System.Collections.Generic;
using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Combat;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Core
{
    public class Flotilla : NetworkBehaviour
    {
        private readonly NetworkVariable<FixedString64Bytes> _flotillaName = new NetworkVariable<FixedString64Bytes>();
        public NetworkEventStringVariable FlotillaName { get; }

        public EventList<Ship> Ships { get; } = new EventList<Ship>();

        public static EventList<Flotilla> AllFlotillas { get; } = new EventList<Flotilla>();

        public EventVariable<bool> IsUnderAttack { get; } = new EventVariable<bool>();
        
        public Flotilla()
        {
            FlotillaName = new NetworkEventStringVariable(_flotillaName, s => new FixedString64Bytes(s));
        }
        
        public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();
            
            _flotillaName.OnValueChanged += OnNameChanged;
            
            UpdateName();

            AllFlotillas.Add(this);
        }

        public override void OnNetworkDespawn()
        {
            base.OnNetworkDespawn();

            _flotillaName.OnValueChanged -= OnNameChanged;
            
            AllFlotillas.Remove(this);
        }

        private void OnNameChanged(FixedString64Bytes previousName, FixedString64Bytes newName)
        {
            UpdateName();
        }

        private void UpdateName()
        {
            if (_flotillaName.Value.ToString() == "") return;
            
            transform.name = _flotillaName.Value.ToString();
        }

        public void SetName(string flotillaName)
        {
            transform.name = flotillaName;
            _flotillaName.Value = new FixedString64Bytes(flotillaName);
        }

        public void AddShip(Ship ship)
        {
            Ships.Add(ship);
            ship.SetFlotilla(this);
            ship.GetComponent<ShipHealth>().OnShipDestroyed += OnShipDestroyed;
        }

        private void OnShipDestroyed(Ship ship)
        {
            Ships.Remove(ship); 

            if (Ships.Count == 0)
            {
                DestroyFlotilla();
            }
        }

        private void DestroyFlotilla()
        {
            Destroy(gameObject);
        }
    }
}