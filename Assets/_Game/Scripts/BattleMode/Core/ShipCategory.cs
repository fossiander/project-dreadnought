﻿using UnityEngine;

namespace BattleMode.Core
{
    [CreateAssetMenu(fileName = "New Ship Category", menuName = "Ships/Ship Category", order = 0)]
    public class ShipCategory : ScriptableObject
    {
        public string categoryName = "New ship category";
    }
}