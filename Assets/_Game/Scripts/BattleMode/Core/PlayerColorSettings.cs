using UnityEngine;

namespace BattleMode.Core
{
    public class PlayerColorSettings : MonoBehaviour
    {
        [SerializeField] private Color playerColor = Color.cyan;
        [SerializeField] private Color allyColor = Color.green;
        [SerializeField] private Color enemyColor = Color.red;
        [SerializeField] private Color neutralColor = Color.grey;
        [SerializeField] private Color unknownColor = Color.yellow;

        public Color GetColorForRelationship(Relationship relationship)
        {
            return relationship switch
            {
                Relationship.Self => playerColor,
                Relationship.Ally => allyColor,
                Relationship.Enemy => enemyColor,
                Relationship.Neutral => neutralColor,
                _ => unknownColor
            };
        }
    }
}
