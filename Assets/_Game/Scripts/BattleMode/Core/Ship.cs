﻿using System;
using _Game.Scripts.Utils.UI_Utils;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Core
{
    [System.Serializable]
    public class Ship : NetworkBehaviour
    {
       public string shipName;
       public string shipClassName;
       public ShipCategory shipCategory;
       public ShipType shipType;
       public Flotilla parentFlotilla;
       
       private readonly NetworkVariable<FixedString64Bytes> _shipName = new NetworkVariable<FixedString64Bytes>();
       public NetworkEventStringVariable ShipName { get; private set; }

       public void InitOnServer(Flotilla parentFlotilla, string shipName, ShipClass shipClass)
       {
           transform.name = shipName;
           this.parentFlotilla = parentFlotilla;
           this.shipName = shipName;
           _shipName.Value = shipName;
           ShipName = new NetworkEventStringVariable(_shipName, s => new FixedString64Bytes(s));

           shipClassName = shipClass.LongClassName;
           shipType = shipClass.shipType;
           shipCategory = shipClass.shipType.category;
       }

       public void SetFlotilla(Flotilla parentFlotilla)
        {
            this.parentFlotilla = parentFlotilla;
        }
    }
}