﻿using System;
using BattleMode.Combat;
using BattleMode.Combat.Weapons;
using BattleMode.UnitConversion;
using UnityEngine;

namespace BattleMode.Core
{
    [CreateAssetMenu(fileName = "New Ship Class", menuName = "Ships/Ship Class", order = 0)]
    public class ShipClass : ScriptableObject
    {
        public string className = "New ship class";
        public ShipType shipType = null;

        [Header("Movement & Evasion")]
        public LengthAmount averageSize = new LengthAmount(250f, LengthAmount.Unit.Meters);
        public AccelerationAmount thrustAcceleration = new AccelerationAmount(1, AccelerationAmount.Unit.G);
        public AccelerationAmount evasionAcceleration = new AccelerationAmount(0.5f, AccelerationAmount.Unit.G);

        [Header("Defense")]
        public float maxHull = 100;

        public float maxArmor = 50;
        public ArmorType armorType;
        public float armorStrength = 10f;


        [Header("Weaponry")]
        public ShipClassWeapons[] weapons = null;

        public string LongClassName => $"{className} class {shipType.typeName}";

        [System.Serializable]
        public struct ShipClassWeapons
        {
            public WeaponClass weaponClass;
            public int weaponCount;
        }
    }
}