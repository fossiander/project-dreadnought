using System;
using BattleMode.Core;
using BattleMode.Movement;
using UnityEngine;

namespace BattleMode.Spawning
{
    public class PathSpawner : MonoBehaviour
    {
        [SerializeField] private MoverPath pathPrefab = null;

        public void SpawnPath(MovableEntity reference)
        {
            MoverPath path = Instantiate(pathPrefab, transform);
        
            path.SetReference(reference);

            if (reference.TryGetComponent(out OwnedObject ownedObject))
            {
                path.SetColor(FindObjectOfType<AllianceManager>().GetPlayerColor(ownedObject.OwnerID));
            }
        }
    }
}
