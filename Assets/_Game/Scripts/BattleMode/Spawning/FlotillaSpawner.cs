using System;
using System.Collections.Generic;
using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Combat;
using BattleMode.Combat.Weapons;
using BattleMode.Core;
using BattleMode.Movement;
using BattleMode.UnitConversion;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Spawning
{
    public class FlotillaSpawner : NetworkBehaviour
    {
        [SerializeField] private Flotilla flotillaPrefab = null;
        [SerializeField] private Ship shipPrefab = null;

        public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();

            if (!IsServer) return;

            SpawnStartingFlotillas();
        }

        private void SpawnStartingFlotillas()
        {
            foreach (FlotillaSpawnPoint flotillaSpawnPoint in GetComponentsInChildren<FlotillaSpawnPoint>())
            {
                Flotilla spawnedFlotilla = SpawnFlotilla(
                    flotillaSpawnPoint.spawnedFlotilla,
                    flotillaSpawnPoint.transform.position,
                    flotillaSpawnPoint.velocity,
                    flotillaSpawnPoint.direction);

                SpawnShips(spawnedFlotilla, flotillaSpawnPoint.spawnedFlotilla.flotillaShips);

                InitializeFlotillaComponents(spawnedFlotilla);
                
                Destroy(flotillaSpawnPoint.gameObject);
            }
        }

        public Flotilla SpawnFlotilla(FlotillaSpawnDetails spawnDetails, Vector2 position, Vector2 velocity, Vector2 direction)
        {
            Flotilla spawnedFlotilla = Instantiate(flotillaPrefab);
            spawnedFlotilla.SetName(spawnDetails.flotillaName);

            OwnedObject ownedObject = spawnedFlotilla.GetComponent<OwnedObject>();
            ownedObject.SetOwner(spawnDetails.ownerID);

            MovableEntity mc = spawnedFlotilla.GetComponent<MovableEntity>();
            mc.SetMaxAcceleration(GetFlotillaAcceleration(spawnDetails.flotillaShips));
            mc.SetPositionVelocityAndDirection(position, velocity, direction);
            
            spawnedFlotilla.NetworkObject.Spawn();
            if (!spawnedFlotilla.NetworkObject.TrySetParent(NetworkObject)) 
                Debug.LogError($"Failed to set parent for flotilla {spawnDetails.flotillaName}!");
            
            return spawnedFlotilla;
        }

        private AccelerationAmount GetFlotillaAcceleration(ShipSpawnDetails[] flotillaShipDetails)
        {
            float lowestAcceleration = Mathf.Infinity;
            
            foreach (ShipSpawnDetails shipDetails in flotillaShipDetails)
            {
                lowestAcceleration = Mathf.Min(
                    lowestAcceleration, 
                    shipDetails.shipClass.thrustAcceleration.GetValueInCoreUnits());
            }

            return new AccelerationAmount(lowestAcceleration, AccelerationAmount.CORE_UNIT);
        }

        private void SpawnShips(Flotilla spawnedFlotilla, ShipSpawnDetails[] spawnDetailsArray)
        {
            foreach (ShipSpawnDetails spawnDetails in spawnDetailsArray)
            {
                Ship spawnedShip = Instantiate(shipPrefab, spawnedFlotilla.transform.position, spawnedFlotilla.transform.rotation);
                spawnedShip.InitOnServer(spawnedFlotilla, spawnDetails.shipName, spawnDetails.shipClass);

                ShipHealth shipHealth = spawnedShip.GetComponent<ShipHealth>();
                shipHealth.InitOnServer(
                    spawnDetails.shipClass.maxHull, 
                    spawnDetails.shipClass.maxArmor,
                    spawnDetails.shipClass.armorType,
                    spawnDetails.shipClass.armorStrength);

                ShipParameters shipParameters = spawnedShip.GetComponent<ShipParameters>();
                shipParameters.InitOnServer(
                    spawnDetails.shipClass.averageSize,
                    spawnDetails.shipClass.evasionAcceleration,
                    spawnDetails.shipClass.thrustAcceleration);

                ShipWeapons shipWeapons = spawnedShip.GetComponent<ShipWeapons>();
                shipWeapons.InitOnServer(spawnDetails.shipClass.weapons);
                
                spawnedFlotilla.AddShip(spawnedShip);
                
                spawnedShip.NetworkObject.Spawn();
                if (!spawnedShip.NetworkObject.TrySetParent(spawnedFlotilla.NetworkObject)) 
                    Debug.LogError($"Failed to set parent for ship {spawnDetails.shipName}!");
            }
        }

        private void InitializeFlotillaComponents(Flotilla flotilla)
        {
            foreach (IInitOnServer initComponent in flotilla.GetComponentsInChildren<IInitOnServer>())
            {
                initComponent.InitOnServer();
            }
        }
    }


    [System.Serializable]
    public class FlotillaSpawnDetails
    {
        public string flotillaName;
        public int ownerID;
        public ShipSpawnDetails[] flotillaShips;
    }
        
    [System.Serializable]
    public class ShipSpawnDetails
    {
        public string shipName;
        public ShipClass shipClass;
    }
}
