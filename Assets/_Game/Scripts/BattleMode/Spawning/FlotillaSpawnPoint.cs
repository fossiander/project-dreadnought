﻿using System;
using UnityEngine;

namespace BattleMode.Spawning
{
    public class FlotillaSpawnPoint : MonoBehaviour
    {
        public FlotillaSpawnDetails spawnedFlotilla;
        public Vector2 velocity;
        public Vector2 direction;

        private void OnValidate()
        {
            name = spawnedFlotilla != null ? $"Flotilla {spawnedFlotilla.flotillaName} spawner" : "Empty flotilla spawner";
        }
    }
}