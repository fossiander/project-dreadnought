﻿using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Spawning
{
    public class WreckSpawner : NetworkBehaviour
    {
        [SerializeField] private NetworkObject wreckPrefab;

        public void SpawnWreck(Vector2 position, Vector2 direction)
        {
            NetworkObject wreck = Instantiate(wreckPrefab);
            wreck.transform.position = position;
            wreck.transform.up = direction;
            wreck.Spawn();
        }
    }
}