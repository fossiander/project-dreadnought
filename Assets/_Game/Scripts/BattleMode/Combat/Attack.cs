﻿using System;
using BattleMode.Combat.Weapons;
using BattleMode.UnitConversion;
using UnityEngine;

namespace BattleMode.Combat
{
    public class Attack
    {
        public AttackableObject Target { get; }
        public WeaponClass WeaponClass { get; }
        public int NumAttacks { get; private set; }
        public float TotalDamage { get; private set; }
        public float HitProbability { get; }

        public Attack(AttackableObject target, WeaponClass weaponClass, int numAttacks, LengthAmount distanceToTarget)
        {
            Target = target;
            WeaponClass = weaponClass;
            NumAttacks = numAttacks;
            TotalDamage = weaponClass.damageMean * numAttacks;
            HitProbability = weaponClass.GetHitProbabilityPerShot(target.TargetParams, distanceToTarget);
        }

        public float RemoveAttack(int numAttacks)
        {
            if (numAttacks > NumAttacks) Debug.LogError($"More attacks removed ({numAttacks} than included in attack {NumAttacks}");

            float damageRemoved = WeaponClass.damageMean * numAttacks;
            
            NumAttacks -= numAttacks;
            TotalDamage -= damageRemoved;
            
            return damageRemoved;
        }
    }
}