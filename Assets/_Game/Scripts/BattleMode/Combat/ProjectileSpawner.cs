﻿using System.Collections.Generic;
using BattleMode.Combat.Projectiles;
using BattleMode.Core;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Combat
{
    public class ProjectileSpawner : NetworkBehaviour
    {
        [SerializeField] private Projectile directFireProjectilePrefab = null;
        [SerializeField] private Projectile missileProjectilePrefab = null;

        public void SpawnProjectile(AttackGroup attackGroup, Vector2 startPosition, Vector2 startVelocity)
        {
            Projectile projectilePrefab = GetProjectilePrefab(attackGroup.ProjectileType);
            
            Projectile projectileInstance = Instantiate(projectilePrefab, transform);
            projectileInstance.InitializeOnServer(attackGroup, startPosition, startVelocity);
            projectileInstance.NetworkObject.Spawn(true);

            projectileInstance.InitializeClientRpc();
            
            if (projectileInstance.TryGetComponent(out OwnedObject ownedObject)) ownedObject.SetOwner(attackGroup.OwnerID);
        }

        private Projectile GetProjectilePrefab(ProjectileType projectileType)
        {
            return projectileType == ProjectileType.Direct
                ? directFireProjectilePrefab
                : missileProjectilePrefab;
        }
    }
}