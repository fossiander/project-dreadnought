﻿using System.Collections.Generic;
using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Combat.Weapons;

namespace BattleMode.Combat
{
    public class WeaponGroup
    {
        private static readonly float[] MinHitProbabilityThresholds = new float[] {0.1f, 0.25f, 0.5f, 0.75f, 0.9f};
        private float _minHitProbability;

        public int WeaponGroupID { get; }
        public List<WeaponInstance> Weapons { get; } = new List<WeaponInstance>();
        public EventVariable<AttackableObject> ManualTarget { get; private set; } = new EventVariable<AttackableObject>();
        public EventVariable<string> WeaponGroupName { get; private set; } = new EventVariable<string>();
        public EventVariable<bool> IsAutoTargetingActive { get; private set; } = new EventVariable<bool>();
        public EventVariable<TargetMode> AutoTargetMode { get; private set; } = new EventVariable<TargetMode>();
        public EventVariable<HitProbabilityThreshold> ProbabilityThreshold { get; private set; } = new EventVariable<HitProbabilityThreshold>();

        public EventVariable<bool> IsSelected { get;  } = new EventVariable<bool>();

        public AttackHandler AttackHandler { get; }

        public WeaponGroup(
            AttackHandler attackHandler, 
            int id, 
            string weaponGroupName = "", 
            bool isAutoTargeting = true,
            TargetMode defaultTargetMode = TargetMode.PointDefenseOnly,
            HitProbabilityThreshold probabilityThreshold = HitProbabilityThreshold.Balanced,
            bool isSelected = false)
        {
            WeaponGroupID = id;
            AttackHandler = attackHandler;
            WeaponGroupName.Value = weaponGroupName == "" ? $"Weapon group {id}" : weaponGroupName;

            IsAutoTargetingActive.Value = isAutoTargeting;
            AutoTargetMode.Value = defaultTargetMode;
            IsSelected.Value = isSelected;
            
            SetHitProbabilityThreshold(probabilityThreshold);
        }

        public void SetManualTarget(AttackableObject target)
        {
            ManualTarget.Value = target;

            if (target == null) return;

            SetTargetForAllWeapons(target, true);
        }

        public void SetAutoTargetingActive(bool isActive)
        {
            IsAutoTargetingActive.Value = isActive;

            if (ManualTarget.Value != null) return;
            
            if (!isActive) ClearTargetForAllWeapons();
            else UpdateAutoTargeting();
        }

        public void SetAutoTargetMode(TargetMode targetMode)
        {
            AutoTargetMode.Value = targetMode;

            if (ManualTarget.Value != null || !IsAutoTargetingActive.Value) return;
            UpdateAutoTargeting();
        }

        private void SetTargetForAllWeapons(AttackableObject target, bool isManual)
        {
            foreach (WeaponInstance weapon in Weapons)
            {
                if (target != null && !weapon.IsValidTarget(target.AttackableType)) continue;
                    
                weapon.SetTarget(target, isManual);
            }
        }

        private void ClearTargetForAllWeapons()
        {
            foreach (WeaponInstance weapon in Weapons)
            {
                weapon.ClearTarget();
            }
        }

        public void UpdateAutoTargeting()
        {
            if (ManualTarget.Value != null) return;
            
            if (!IsAutoTargetingActive.Value && ManualTarget.Value == null)
            {
                ClearTargetForAllWeapons();
                return;
            }

            AttackableType attackableType = AutoTargetMode.Value switch
            {
                TargetMode.PointDefenseOnly => AttackableType.Projectile,
                TargetMode.ShipsOnly => AttackableType.Ship,
                _ => AttackableType.Any
            };
            
            AttackableObject newTarget = AttackHandler.GetClosestTarget(attackableType);
            
            SetTargetForAllWeapons(newTarget, false);
        }

        public void SetHitProbabilityThreshold(HitProbabilityThreshold hitProbabilityThreshold)
        {
            ProbabilityThreshold.Value = hitProbabilityThreshold;
            
            _minHitProbability = MinHitProbabilityThresholds[(int)hitProbabilityThreshold];
            
            ResetHitProbabilityThresholdForAllWeapons();
        }
        
        private void ResetHitProbabilityThresholdForAllWeapons()
        {
            foreach (WeaponInstance weapon in Weapons)
            {
                weapon.SetMinimumHitProbability(_minHitProbability);
            }
        }

        public void AddWeapon(WeaponInstance weapon)
        {
            Weapons.Add(weapon);
            
            weapon.SetMinimumHitProbability(_minHitProbability);
        }

        public enum TargetMode
        {
            PointDefenseOnly = 1,
            ShipsAndPointDefense = 2,
            ShipsOnly = 3
        }
        
        public enum HitProbabilityThreshold
        {
            VeryAggressive = 0,
            Aggressive = 1, 
            Balanced = 2,
            Conservative = 3,
            VeryConservative = 4
        }
    }
}