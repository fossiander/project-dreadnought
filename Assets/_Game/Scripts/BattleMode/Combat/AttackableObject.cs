﻿using System;
using System.Collections.Generic;
using _Game.Scripts.Utils;
using BattleMode.Core;
using BattleMode.Movement;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Combat
{
    public class AttackableObject : NetworkBehaviour
    {
        [SerializeField] private AttackableType attackableType;
        public AttackableType AttackableType => attackableType;

        private readonly LazyInit<Transform> _reference;
        private readonly LazyInit<OwnedObject> _ownedObject;
        public int OwnerID => _ownedObject.Get().OwnerID;

        private readonly LazyInit<MovableEntity> _movableEntity;
        public MovableEntity MovableEntity => _movableEntity.Get();
        public Vector2 Position => _movableEntity.Get().Position.Value;
        public TargetParams TargetParams => _targetParamsProvider.Get().GetTargetParams();
        
        private readonly LazyComponentGetter<ITargetParamsProvider> _targetParamsProvider;

        public static List<AttackableObject> AllAttackableObjects { get; } = new List<AttackableObject>();
        
        public AttackableObject()
        {
            _reference = new LazyInit<Transform>(GetReferenceTransform);
            _ownedObject = new LazyInit<OwnedObject>(GetOwnedObject);
            _movableEntity = new LazyInit<MovableEntity>(GetMovableEntity);
            _targetParamsProvider = new LazyComponentGetter<ITargetParamsProvider>(this);
        }

        private Transform GetReferenceTransform()
        {
            return attackableType == AttackableType.Ship ? transform.parent : transform;
        }
        
        private OwnedObject GetOwnedObject()
        {
            return _reference.Get().GetComponent<OwnedObject>();
        }
        
        private MovableEntity GetMovableEntity()
        {
            return _reference.Get().GetComponent<MovableEntity>();
        }

        private void OnEnable()
        {
            AllAttackableObjects.Add(this);
        }

        private void OnDisable()
        {
            AllAttackableObjects.Remove(this);
        }
    }

    public enum AttackableType
    {
        Ship,
        Projectile,
        Any
    }
}