﻿using BattleMode.UnitConversion;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Combat
{
    public class ShipParameters : NetworkBehaviour, ITargetParamsProvider
    {
        public NetworkVariable<LengthAmount> AverageSize { get; } = new NetworkVariable<LengthAmount>();
        public NetworkVariable<AccelerationAmount> EvasionAcceleration { get; } = new NetworkVariable<AccelerationAmount>();
        public NetworkVariable<AccelerationAmount> ThrustAcceleration { get; } = new NetworkVariable<AccelerationAmount>();

        public void InitOnServer(LengthAmount averageSize, AccelerationAmount evasionAcceleration, AccelerationAmount thrustAcceleration)
        {
            AverageSize.Value = averageSize;
            EvasionAcceleration.Value = evasionAcceleration;
            ThrustAcceleration.Value = thrustAcceleration;
        }

        public TargetParams GetTargetParams()
        {
            return new TargetParams()
            {
                averageSize = AverageSize.Value,
                evasionAcceleration = EvasionAcceleration.Value
            };
        }
    }
}