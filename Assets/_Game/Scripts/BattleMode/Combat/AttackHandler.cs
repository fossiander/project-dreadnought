﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Utils;
using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Combat.Projectiles;
using BattleMode.Combat.Weapons;
using BattleMode.Core;
using BattleMode.Movement;
using BattleMode.UnitConversion;
using BattleMode.Vision;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Combat
{
    public class AttackHandler : NetworkBehaviour, IInitOnServer
    {
        private readonly WaitForSeconds _targetRefreshWait = new WaitForSeconds(0.2f);

        [SerializeField] private MovableEntity movableEntity;
        [SerializeField] private OwnedObject ownedObject;

        [SerializeField] private WeaponGroup.TargetMode defaultTargetMode = WeaponGroup.TargetMode.PointDefenseOnly;
        [SerializeField] private WeaponGroup.HitProbabilityThreshold minimumHitProbabilityThreshold = WeaponGroup.HitProbabilityThreshold.Balanced;
        
        private readonly LazyObjectFinder<ProjectileSpawner> _projectileSpawner = new LazyObjectFinder<ProjectileSpawner>();
        private readonly LazyObjectFinder<VisibilityManager> _visibilityManager = new LazyObjectFinder<VisibilityManager>();
        
        private readonly List<WeaponInstance> _weapons = new List<WeaponInstance>();

        public EventList<WeaponGroup> WeaponGroups { get; }= new EventList<WeaponGroup>();

        private readonly NetworkVariable<bool> _isAttacking = new NetworkVariable<bool>();
        public NetworkEventVariable<bool> IsAttacking { get; }

        private AttackHandler()
        {
            IsAttacking = new NetworkEventVariable<bool>(_isAttacking);
        }
        
        public void InitOnServer()
        {
            GenerateWeaponGroups();
        }

        private void GenerateWeaponGroups()
        {
            foreach (var shipWeapons in GetComponentsInChildren<ShipWeapons>())
            {
                _weapons.AddRange(shipWeapons.Weapons);
            }

            Dictionary<WeaponCategory, List<WeaponInstance>> weaponsByCategory = new Dictionary<WeaponCategory, List<WeaponInstance>>();
            foreach (WeaponInstance weapon in _weapons)
            {
                if (!weaponsByCategory.ContainsKey(weapon.WeaponClass.weaponCategory)) 
                    weaponsByCategory.Add(
                        weapon.WeaponClass.weaponCategory,
                        new List<WeaponInstance>());
                
                weaponsByCategory[weapon.WeaponClass.weaponCategory].Add(weapon);
            }

            bool mainWeaponGroupSet = false;
            int i = 0;
            foreach (WeaponCategory weaponCategory in Enum.GetValues(typeof(WeaponCategory)))
            {
                if (!weaponsByCategory.ContainsKey(weaponCategory)) continue;
                
                string weaponGroupName;
                if (weaponCategory == WeaponCategory.Missile)
                {
                    weaponGroupName = "Missiles";
                }
                else if (!mainWeaponGroupSet)
                {
                    weaponGroupName = "Main Batteries";
                    mainWeaponGroupSet = true;
                }
                else if (weaponCategory == WeaponCategory.PointDefense)
                {
                    weaponGroupName = "Point Defense Weapons";
                }
                else weaponGroupName = "Secondary Batteries";

                WeaponGroup newWeaponGroup = new WeaponGroup(this, i, weaponGroupName, true, defaultTargetMode, minimumHitProbabilityThreshold, i == 0);
                foreach (WeaponInstance weapon in weaponsByCategory[weaponCategory])
                {
                    newWeaponGroup.AddWeapon(weapon);
                }
                
                WeaponGroups.Add(newWeaponGroup);
                i++;
            }
        }

        public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();
            
            StartCoroutine(CUpdateTargeting());
        }

        public override void OnNetworkDespawn()
        {
            base.OnNetworkDespawn();
            
            StopAllCoroutines();
        }

        private void Update()
        {
            if (!IsServer) return;

            UpdateWeaponAttacks();
            UpdateIsAttacking();
        }

        public void SetManualAttackTarget(AttackableObject target, IEnumerable<int> weaponGroups)
        {
            foreach (int weaponGroup in weaponGroups)
            {
                WeaponGroups[weaponGroup].SetManualTarget(target);                
            }
        }

        public void SetAutoTargetingActive(bool isActive, int[] weaponGroups)
        {
            foreach (int weaponGroup in weaponGroups)
            {
                WeaponGroups[weaponGroup].SetAutoTargetingActive(isActive);
            }
        }

        public void SetAutoTargetMode(WeaponGroup.TargetMode targetMode, IEnumerable<int> weaponGroups)
        {
            foreach (int weaponGroup in weaponGroups)
            {
                WeaponGroups[weaponGroup].SetAutoTargetMode(targetMode);
            }
        }
        
        public void SetHitProbabilityThreshold(WeaponGroup.HitProbabilityThreshold probabilityThreshold, IEnumerable<int> weaponGroups)
        {
            foreach (int weaponGroup in weaponGroups)
            {
                WeaponGroups[weaponGroup].SetHitProbabilityThreshold(probabilityThreshold);
            }
        }

        private void UpdateWeaponAttacks()
        {
            Vector2 startPosition = movableEntity.Position.Value;
            Vector2 startVelocity = movableEntity.Velocity.Value;

            Dictionary<(ProjectileType projectileType,AccelerationAmount accelerationAmount,MovableEntity movableEntity), Dictionary<(WeaponClass weaponClass,AttackableObject attackableObject), int>> weaponsAndTargetsByProjectileParameters = new();
            Dictionary<(ProjectileType projectileType,AccelerationAmount accelerationAmount,MovableEntity movableEntity), AttackGroup> attackGroupsByProjectileParameters = new();

            foreach (var weapon in _weapons)
            {
                if (!weapon.IsReloaded()) continue;
                if (weapon.Target == null) continue;
                if (!weapon.CanFireAtTarget(startPosition)) continue;

                weapon.FireWeapon(out int attackCount);

                var key1 = (weapon.WeaponClass.ProjectileType, ThrustAccelerationIn10KMPMM: weapon.WeaponClass.ThrustAcceleration, weapon.Target.MovableEntity);
                var key2 = (weapon.WeaponClass, weapon.Target);
                
                if (!weaponsAndTargetsByProjectileParameters.ContainsKey(key1))
                {
                    weaponsAndTargetsByProjectileParameters.Add(key1, new Dictionary<(WeaponClass, AttackableObject), int>());
                }

                if (!weaponsAndTargetsByProjectileParameters[key1].ContainsKey(key2))
                {
                    weaponsAndTargetsByProjectileParameters[key1][key2] = attackCount;
                }
                else
                {
                    weaponsAndTargetsByProjectileParameters[key1][key2] += attackCount;
                }
            }

            foreach (var key in weaponsAndTargetsByProjectileParameters.Keys)
            {
                attackGroupsByProjectileParameters.Add(key, new AttackGroup(ownedObject.OwnerID, key.projectileType, key.movableEntity, key.accelerationAmount));

                LengthAmount distanceToTarget = new LengthAmount(
                    Vector2.Distance(startPosition, key.movableEntity.Position.Value),
                    LengthAmount.CORE_UNIT);
                
                foreach (var keyValuePair in weaponsAndTargetsByProjectileParameters[key])
                {
                    if (keyValuePair.Value == 0) continue;

                    attackGroupsByProjectileParameters[key].AddAttack(
                        new Attack(
                            keyValuePair.Key.Item2,
                            keyValuePair.Key.Item1,
                            keyValuePair.Value,
                            distanceToTarget));
                }
            }

            foreach (var attackGroup in attackGroupsByProjectileParameters.Values)
            {
                _projectileSpawner.Get().SpawnProjectile(
                    attackGroup,
                    startPosition,
                    startVelocity);
            }
        }

        private void UpdateIsAttacking()
        {
            bool isAttacking = false;
            foreach (var weapon in _weapons)
            {
                if (weapon.Target == null) continue;
                if (!weapon.HasManualTarget && !weapon.CanFireAtTarget(movableEntity.Position.Value)) continue;
                    
                isAttacking = true;
                break;
            }

            _isAttacking.Value = isAttacking;
        }

        private IEnumerator CUpdateTargeting()
        {
            while (true)
            {
                foreach (WeaponGroup weaponGroup in WeaponGroups)
                {
                    weaponGroup.UpdateAutoTargeting();
                }

                yield return _targetRefreshWait;   
            }
        }

        public AttackableObject GetClosestTarget(AttackableType attackableType)
        {
            AttackableObject[] attackableObjects = _visibilityManager.Get().GetVisibleEnemies(attackableType, ownedObject.OwnerID);

            AttackableObject closestObject = attackableObjects
                .OrderBy(ao => (ao.Position - movableEntity.Position.Value).SqrMagnitude())
                .FirstOrDefault();
            
            return closestObject;
        }
    }
}