﻿using System;
using System.Collections.Generic;
using _Game.Scripts.Utils.Event_Utils;
using BattleMode.Combat.Weapons;
using BattleMode.Core;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Combat.Projectiles
{
    public abstract class Projectile : NetworkBehaviour, IColorChanger
    {
        private NetworkVariable<Vector2> _startPosition;
        private NetworkList<AttackValues> _attacks;

        public virtual void InitializeOnServer(AttackGroup attackGroup, Vector2 startPosition, Vector2 startVelocity)
        {
            _startPosition = new NetworkVariable<Vector2>();
            _startPosition.Value = startPosition;
            
            _attacks = new NetworkList<AttackValues>();
            foreach (Attack attack in attackGroup.AttackList)
            {
                _attacks.Add(new AttackValues()
                {
                    weaponClassID = attack.WeaponClass.GetUniqueID(),
                    attackCount = attack.NumAttacks
                });
            }
        }

        [ClientRpc]
        public void InitializeClientRpc()
        {
            InitializeOnClient();
            TriggerWeaponFiredEvent();
        }
        
        private void TriggerWeaponFiredEvent()
        {
            Dictionary<WeaponClass, int> shotsByWeaponClass = new Dictionary<WeaponClass, int>();

            foreach (AttackValues attackValues in _attacks)
            {
                WeaponClass weaponClass = WeaponClass.GetObjectWithID(attackValues.weaponClassID);
                if (!shotsByWeaponClass.ContainsKey(weaponClass))
                {
                    shotsByWeaponClass.Add(weaponClass, attackValues.attackCount);
                }
                else
                {
                    shotsByWeaponClass[weaponClass] += attackValues.attackCount;
                }
            }
            
            EventSystem.TriggerEvent(new WeaponFiredEvent()
            {
                startPosition = _startPosition.Value,
                shotsByWeaponClass = shotsByWeaponClass
            });
        }

        protected abstract void InitializeOnClient();
        public abstract void SetColor(Color color);

        [System.Serializable]
        public struct AttackValues : IEquatable<AttackValues>
        {
            public int weaponClassID;
            public int attackCount;

            public bool Equals(AttackValues other)
            {
                return weaponClassID == other.weaponClassID && attackCount == other.attackCount;
            }

            public override bool Equals(object obj)
            {
                return obj is AttackValues other && Equals(other);
            }

            public override int GetHashCode()
            {
                return HashCode.Combine(weaponClassID, attackCount);
            }
        }
    }
    
    public class WeaponFiredEvent : Event<WeaponFiredEvent>
    {
        public Vector2 startPosition;
        public Dictionary<WeaponClass, int> shotsByWeaponClass;
    }
}