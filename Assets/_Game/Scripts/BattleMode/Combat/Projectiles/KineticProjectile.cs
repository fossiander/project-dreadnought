﻿using _Game.Scripts.Utils;
using _Game.Scripts.Utils.Event_Utils;
using Shapes;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Combat.Projectiles
{
    public class KineticProjectile : Projectile
    {
        [SerializeField] private Line line;
        [SerializeField] private AnimationCurve fadeOutCurve = AnimationCurve.EaseInOut(0, 1, 1, 0);
        [SerializeField] private float lifeTime = 1f;
        [SerializeField] private SizeScaler lineWidthScaler;

        private readonly NetworkVariable<float> _remainingLifeTime = new NetworkVariable<float>();
        private readonly NetworkVariable<Vector2> _startPosition = new NetworkVariable<Vector2>();
        private readonly NetworkVariable<Vector2> _endPosition = new NetworkVariable<Vector2>();
        private readonly NetworkVariable<float> _totalDamage = new NetworkVariable<float>();
        private readonly NetworkVariable<float> _damageDone = new NetworkVariable<float>();

        private Color _color;

        private readonly LazyObjectFinder<CombatCalculator> _combatCalculator = new LazyObjectFinder<CombatCalculator>();

        public override void InitializeOnServer(AttackGroup attackGroup, Vector2 startPosition, Vector2 startVelocity)
        {
            base.InitializeOnServer(attackGroup, startPosition, startVelocity);
            
            SetNetworkVariables(attackGroup, startPosition);
            SetLineParameters();
            
            float damageDone = _combatCalculator.Get().HandleAttack(attackGroup);
            _damageDone.Value = damageDone;
        }

        protected override void InitializeOnClient()
        {
            SetLineParameters();
            
            TriggerMissileDestroyedEventOnClient(
                _endPosition.Value,
                _endPosition.Value - _startPosition.Value,
                _damageDone.Value);
        }

        private void SetNetworkVariables(AttackGroup attackGroup, Vector2 startPosition)
        {
            _startPosition.Value = startPosition;
            _endPosition.Value = attackGroup.Target.Position.Value;
            _remainingLifeTime.Value = lifeTime;
            _totalDamage.Value = attackGroup.TotalDamage;
        }

        private void SetLineParameters()
        {
            line.Start = _startPosition.Value;
            line.End = _endPosition.Value;
            line.Thickness = lineWidthScaler.GetOutput(_totalDamage.Value);
        }

        public override void SetColor(Color color)
        {
            _color = color;
            UpdateColor();
        }

        private void Update()
        {
            if (IsServer) UpdateServer();

            UpdateColor();
        }

        private void UpdateServer()
        {
            _remainingLifeTime.Value -= Time.deltaTime;

            if (_remainingLifeTime.Value <= 0)
            {
                NetworkObject.Despawn();
            }
        }

        private void UpdateColor()
        {
            float alpha = fadeOutCurve.Evaluate(1 - _remainingLifeTime.Value / lifeTime);
            line.Color = new Color(_color.r, _color.g, _color.b, alpha);
        }

        private void TriggerMissileDestroyedEventOnClient(Vector2 position, Vector2 direction, float damageDone)
        {
            EventSystem.TriggerEvent(new DirectProjectileHitEvent()
            {
                position = position,
                direction = direction,
                damageDone = damageDone
            });
        }
    }

    public class DirectProjectileHitEvent : Event<DirectProjectileHitEvent>
    {
        public Vector2 position;
        public Vector2 direction;
        public float damageDone;
    }
}