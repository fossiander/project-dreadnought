﻿using System;
using _Game.Scripts.Utils;
using _Game.Scripts.Utils.Event_Utils;
using BattleMode.Combat.Weapons;
using BattleMode.Core;
using BattleMode.Movement;
using BattleMode.Movement.Movements;
using BattleMode.UnitConversion;
using Shapes;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Combat.Projectiles
{
    public class MissileProjectile : Projectile, ITargetParamsProvider
    {
        [SerializeField] private Disc disc = null;
        [SerializeField] private MovableEntity movableEntity;
        [SerializeField] private float hitDistance = 0.01f;
        [SerializeField] private float lifeTime = 10f;
        [SerializeField] private SizeScaler discSizeScaler;

        private AttackGroup _attackGroup;
        public int NumMissiles => _attackGroup.TotalAttacks;

        private AccelerationAmount _acceleration;
        private Vector2 _velocity;
        private TrackingAcceleration _movement;

        private readonly NetworkVariable<float> _totalDamage = new NetworkVariable<float>();
        
        public override void InitializeOnServer(AttackGroup attackGroup, Vector2 startPosition, Vector2 startVelocity)
        {
            base.InitializeOnServer(attackGroup, startPosition, startVelocity);
            
            UpdateTotalDamage(attackGroup);
            InitializeMovement(attackGroup, startPosition, startVelocity);
            SetAttackGroup(attackGroup);

            SubscribeToDamageChanges();
            UpdateDiscThickness();

            foreach (IInitOnServer initOnServer in GetComponentsInChildren<IInitOnServer>())
            {
                initOnServer.InitOnServer();
            }
        }

        protected override void InitializeOnClient()
        {
            SubscribeToDamageChanges();
            UpdateDiscThickness();
        }

        private void SubscribeToDamageChanges()
        {
            _totalDamage.OnValueChanged += OnDamageChanged;
        }

        private void OnDamageChanged(float previousDamage, float newDamage)
        {
            UpdateDiscThickness();
        }

        private void UpdateTotalDamage(AttackGroup attackGroup)
        {
            _totalDamage.Value = attackGroup.TotalDamage;
        }

        private void UpdateDiscThickness()
        {
            disc.Thickness = discSizeScaler.GetOutput(_totalDamage.Value);
        }

        private void InitializeMovement(AttackGroup attackGroup, Vector2 startPosition, Vector2 startVelocity)
        {
            _acceleration = attackGroup.ThrustAcceleration;
            
            movableEntity.SetMaxAcceleration(_acceleration);
            movableEntity.SetPositionVelocityAndDirection(startPosition, startVelocity,
                (attackGroup.Target.Position.Value - startPosition));
            _movement = new TrackingAcceleration(movableEntity, attackGroup.Target, hitDistance, lifeTime);
            movableEntity.SetMovement(_movement);
        }

        private void SetAttackGroup(AttackGroup attackGroup)
        {
            _attackGroup = attackGroup;
        }

        public override void SetColor(Color color)
        {
            disc.Color = color;
        }

        private void Update()
        {
            if (IsServer) UpdateServer();
        }

        private void UpdateServer()
        {
            if (_movement.IsCompleted())
            {
                DestroySelf();
                return;
            }

            if (_movement.HasReachedTarget(Time.deltaTime))
            {
                FindObjectOfType<CombatCalculator>().HandleAttack(_attackGroup);
                DestroySelf();
            }
        }

        public (int, float) DestroyMissiles(int numDestroys)
        {
            (int missilesRemoved, float damageRemoved) = _attackGroup.RemoveRandomAttacks(numDestroys);
            UpdateTotalDamage(_attackGroup);
            TriggerMissileDestroyedEventClientRpc(transform.position, missilesRemoved, damageRemoved);
            
            if (_attackGroup.TotalAttacks <= 0) DestroySelf();

            return (missilesRemoved, damageRemoved);
        }

        private void DestroySelf()
        {
            TriggerMissileDestroyedEventClientRpc(transform.position, _attackGroup.TotalAttacks, _attackGroup.TotalDamage);
            Destroy(gameObject);
        }

        public TargetParams GetTargetParams()
        {
            //TODO: Do not use averages for hit probabilities of missiles           
            return new TargetParams()
            {
                evasionAcceleration = _attackGroup.AverageEvasionAccelerationInMPSS,
                averageSize = _attackGroup.AverageSizeInM
            };
        }

        [ClientRpc]
        private void TriggerMissileDestroyedEventClientRpc(Vector2 position, int numMissilesDestroyed, float damageRemoved)
        {
            EventSystem.TriggerEvent(new MissileDestroyedEvent()
            {
                position = position,
                numMissilesDestroyed = numMissilesDestroyed,
                damageRemoved = damageRemoved
            });
        }
    }

    public class MissileDestroyedEvent : Event<MissileDestroyedEvent>
    {
        public Vector2 position;
        public int numMissilesDestroyed;
        public float damageRemoved;
    }
}