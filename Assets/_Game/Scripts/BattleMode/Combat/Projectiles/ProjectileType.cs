﻿namespace BattleMode.Combat.Projectiles
{
    public enum ProjectileType
    {
        Direct,
        Missile
    }
}