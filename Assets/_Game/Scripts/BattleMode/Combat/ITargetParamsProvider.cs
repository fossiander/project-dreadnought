﻿using BattleMode.UnitConversion;
using UnityEngine;

namespace BattleMode.Combat
{
    public interface ITargetParamsProvider
    {
        TargetParams GetTargetParams();
    }

    public struct TargetParams
    {
        public LengthAmount averageSize;
        public AccelerationAmount evasionAcceleration;
    }
}