using System;
using _Game.Scripts.Utils;
using _Game.Scripts.Utils.Event_Utils;
using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Core;
using BattleMode.Spawning;
using Unity.Netcode;
using UnityEngine;

namespace BattleMode.Combat
{
    public class ShipHealth : NetworkBehaviour
    {
        [SerializeField] private Ship ship;
        private float _armorStrength;
        private ArmorType _armorType;

        public EventVariable<float> MaxArmor { get; } = new EventVariable<float>();
        public EventVariable<float> MaxHull { get; } = new EventVariable<float>();
        
        private readonly NetworkVariable<float> _armor = new NetworkVariable<float>();
        private readonly NetworkVariable<float> _hull = new NetworkVariable<float>();
        private readonly NetworkVariable<float> _timeOfLastDamage = new NetworkVariable<float>(Mathf.NegativeInfinity);

        private readonly LazyObjectFinder<WreckSpawner> _wreckSpawner = new LazyObjectFinder<WreckSpawner>();

        public float ArmorStrength => _armorStrength;
        public ArmorType ArmorType => _armorType;
        public float ArmorPercentage => _armor.Value / MaxArmor.Value;
        public float HullPercentage => _hull.Value / MaxHull.Value;
        public NetworkEventVariable<float> Armor { get; }
        public NetworkEventVariable<float> Hull { get; }
        public NetworkEventVariable<float> TimeOfLastDamage { get; }
        public Action<Ship> OnShipDestroyed { get; set; }

        public void InitOnServer(float maxHull, float maxArmor, ArmorType armorType, float armorStrength)
        {
            MaxHull.Value = maxHull;
            MaxArmor.Value = maxArmor;
            
            _armor.Value = MaxArmor.Value;
            _hull.Value = MaxHull.Value;
            
            _armorType = armorType;
            _armorStrength = armorStrength;
        }

        private ShipHealth()
        {
            Armor = new NetworkEventVariable<float>(_armor);
            Hull = new NetworkEventVariable<float>(_hull);
            TimeOfLastDamage = new NetworkEventVariable<float>(_timeOfLastDamage);
        }

        public void TakeArmorDamage(float damage)
        {
            if (damage > 0) _timeOfLastDamage.Value = Time.time;
            
            _armor.Value -= Mathf.Min(damage,_armor.Value);
            
            Debug.Log($"Ship armor decreased to {_armor.Value}.");
        }
        
        public bool TakeHullDamage(float damage)
        {
            if (damage > 0) _timeOfLastDamage.Value = Time.time;
            
            _hull.Value -= Mathf.Min(damage,_hull.Value);
            Debug.Log($"Ship hull decreased to {_hull.Value}.");

            if (_hull.Value <= float.Epsilon)
            {
                DestroyShip();
                return true;
            }

            return false;
        }

        private void DestroyShip()
        {
            SpawnWreck();
            Destroy(gameObject);
            OnShipDestroyed?.Invoke(ship);

            EventSystem.TriggerEvent(new ShipDestroyedEvent()
            {
                ship = ship,
                position = transform.position,
                direction = transform.up,
                shipTotalHullPoints = MaxHull.Value
            });
        }

        private void SpawnWreck()
        {
            _wreckSpawner.Get().SpawnWreck(transform.position, transform.up);
        }

        public float GetArmorPenetrationLevel()
        {
            return _armorType.GetPenetrationLevel(_armorStrength, ArmorPercentage);
        }
        
        public float GetArmorOverPenetrationLevel()
        {
            return _armorType.GetOverPenetrationLevel(_armorStrength, ArmorPercentage);
        }
    }

    public class ShipDestroyedEvent : Event<ShipDestroyedEvent>
    {
        public Ship ship;
        public Vector2 position;
        public Vector2 direction;
        public float shipTotalHullPoints;
    }
}
