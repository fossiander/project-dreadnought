using System;
using BattleMode.Combat.Projectiles;
using BattleMode.Combat.Weapons;
using BattleMode.Core;
using MathNet.Numerics.Distributions;
using UnityEngine;

namespace BattleMode.Combat
{
    public class CombatCalculator : MonoBehaviour
    {
        [SerializeField] private CombatLog combatLog = null;

        public float HandleAttack(AttackGroup attackGroup)
        {
            float totalDamage = 0;
            foreach (var attack in attackGroup.AttackList)
            {
                float damage = HandleAttack(attack, out string logMessage);
                combatLog.Log(logMessage);

                totalDamage += damage;
            }

            return totalDamage;
        }

        private float HandleAttack(Attack attack, out string logMessage)
        {
            if (attack.Target == null)
            {
                logMessage = "Attack results: No additional damage as target already destroyed.";
                return 0;
            }
            
            (int hits, int misses) = CalculateHitsAndMisses(attack);

            if (attack.Target.AttackableType == AttackableType.Projectile)
            {
                float damage = ApplyDamageToMissileProjectile(attack, hits, out logMessage);

                return damage;
            }
            else
            {
                float damage = CalculateShipDamage(attack, hits);
                (float armorDamage, float hullDamage, float overPenDamage) = CalculateShipArmorPenetration(attack, damage, hits);
                bool shipDestroyed = ApplyDamageToShip(attack, armorDamage, hullDamage);
                
                logMessage = CreateShipLogMessage(attack, hits, damage, armorDamage, hullDamage, overPenDamage, shipDestroyed);
                
                return armorDamage + hullDamage;
            }
        }

        private (int, int) CalculateHitsAndMisses(Attack attack)
        {
            int hits = Binomial.Sample(attack.HitProbability, attack.NumAttacks);
            int misses = attack.NumAttacks - hits;

            return (hits, misses);
        }

        private float CalculateShipDamage(Attack attack, int hits)
        {
            return (float)Normal.Sample(attack.WeaponClass.damageMean * hits, attack.WeaponClass.damageStDev * Mathf.Sqrt(hits));
        }

        private (float, float, float) CalculateShipArmorPenetration(Attack attack, float damage, int hits)
        {
            float totalArmorHit = 0;
            float totalHullHit = 0;
            float totalOverPen = 0;
            float totalPen = 0;

            float armorPenLevel = attack.Target.GetComponent<ShipHealth>().GetArmorPenetrationLevel();
            float armorOverPenLevel = attack.Target.GetComponent<ShipHealth>().GetArmorOverPenetrationLevel();

            for (int i = 0; i < hits; i++)
            {
                float pen = (float)Normal.Sample(attack.WeaponClass.penetrationMean, attack.WeaponClass.penetrationStDev);

                float armorHit = Mathf.Min(pen, armorPenLevel);
                float hullHit = Mathf.Min(pen, armorOverPenLevel) - armorHit;
                float overPen = pen - armorHit - hullHit;

                totalArmorHit += armorHit;
                totalHullHit += hullHit;
                totalOverPen += overPen;
                totalPen += pen;
            }

            float armorDamage = totalArmorHit / totalPen * damage;
            float hullDamage = totalHullHit / totalPen * damage;
            float overPenDamage = totalOverPen / totalPen * damage;

            return (armorDamage, hullDamage, overPenDamage);
        }

        private bool ApplyDamageToShip(Attack attack, float armorDamage, float hullDamage)
        {
            if (!attack.Target.TryGetComponent(out ShipHealth shipHealth))
            {
                Debug.LogError($"Attack target {attack.Target.name} did not contain a ShipHealth component!");
                return false;
            }
            
            shipHealth.TakeArmorDamage(armorDamage);
            bool shipDestroyed = shipHealth.TakeHullDamage(hullDamage);
            return shipDestroyed;
        }

        private float ApplyDamageToMissileProjectile(Attack attack, int hits, out string logMessage)
        {
            if (!attack.Target.TryGetComponent(out MissileProjectile missileProjectile))
            {
                logMessage = $"Error: Attack target {attack.Target.name} did not contain a MissileProjectile component!";
                Debug.LogError(logMessage);
                return 0;
            }

            if (missileProjectile.NumMissiles == 0)
            {
                logMessage = $"Attack results: All missiles already destroyed, no additional effect achieved.";
                return 0;
            }
            
            (int missilesDestroyed, float damageRemoved) = missileProjectile.DestroyMissiles(hits);
            logMessage = $"Attack results: {hits} of {attack.NumAttacks} shots hit, destroying {missilesDestroyed} missiles";

            return damageRemoved;
        }

        private string CreateShipLogMessage(Attack attack, int hits, float damage, float armorDamage, float hullDamage, float overPenDamage, bool shipDestroyed)
        {
            return $"Attack results: {hits} of {attack.NumAttacks} shots hit, dealing {armorDamage} damage to armor and {hullDamage} to hull; {overPenDamage} was lost due to over-penetration.{(shipDestroyed ? " Target was successfully destroyed." : "")}";
        }
    }
}
