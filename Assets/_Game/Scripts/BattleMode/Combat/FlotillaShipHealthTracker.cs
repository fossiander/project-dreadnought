﻿using System;
using _Game.Scripts.Utils;
using _Game.Scripts.Utils.UI_Utils;
using BattleMode.Core;
using UnityEngine;

namespace BattleMode.Combat
{
    public class FlotillaShipHealthTracker : MonoBehaviour
    {
        [SerializeField] private float timeSinceLastDamageWindow = 5f;
        
        public EventVariable<bool> IsTakingDamage { get; } = new EventVariable<bool>();

        private readonly LazyComponentGetter<Flotilla> _parentFlotilla;

        private readonly EventList<ShipHealth> _shipHealthList = new EventList<ShipHealth>();

        private FlotillaShipHealthTracker()
        {
            _parentFlotilla = new LazyComponentGetter<Flotilla>(this);
        }

        private void Awake()
        {
            CreateInitialShipHealthList();
            _parentFlotilla.Get().Ships.OnItemAdded += OnShipAdded;
            _parentFlotilla.Get().Ships.OnItemRemoved += OnShipRemoved;
        }

        private void Start()
        {
            UpdateShipTakingDamage();
        }

        private void OnShipAdded(Ship ship)
        {
            ShipHealth shipHealth = ship.GetComponent<ShipHealth>();
            _shipHealthList.Add(shipHealth);
            shipHealth.TimeOfLastDamage.OnValueChanged += OnShipTimeSinceLastDamageChanged;
        }

        private void OnShipRemoved(Ship ship)
        {
            ShipHealth shipHealth = ship.GetComponent<ShipHealth>();
            _shipHealthList.Remove(shipHealth);
            shipHealth.TimeOfLastDamage.OnValueChanged -= OnShipTimeSinceLastDamageChanged;
        }

        private void CreateInitialShipHealthList()
        {
            foreach (Ship ship in _parentFlotilla.Get().Ships)
            {
                ShipHealth shipHealth = ship.GetComponent<ShipHealth>();
                _shipHealthList.Add(shipHealth);
            }
        }

        private void OnShipTimeSinceLastDamageChanged(float oldValue, float newValue)
        {
            UpdateShipTakingDamage();
        }

        private void UpdateShipTakingDamage()
        {
            bool takingDamage = false;

            foreach (ShipHealth shipHealth in _shipHealthList)
            {
                if (Time.time <= shipHealth.TimeOfLastDamage.Value + timeSinceLastDamageWindow)
                {
                    takingDamage = true;
                    break;
                }
            }

            IsTakingDamage.Value = takingDamage;
        }
    }
}