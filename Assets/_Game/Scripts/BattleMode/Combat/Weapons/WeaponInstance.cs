﻿using BattleMode.UnitConversion;
using UnityEngine;

namespace BattleMode.Combat.Weapons
{
    public class WeaponInstance
    {
        public WeaponClass WeaponClass { get; }
        public AttackableObject Target { get; private set; }

        private float _timeOfNextShot = Mathf.NegativeInfinity;
        
        private float _minHitProbability = 0;
        
        public bool HasManualTarget { get; private set; }

        public WeaponInstance(WeaponClass weaponClass)
        {
            WeaponClass = weaponClass;
        }

        public bool IsReloaded()
        {
            return Time.time >= _timeOfNextShot;
        }
        
        public bool CanFireAtTarget(Vector2 startPosition)
        {
            if (Target == null) return false;
            if (!WeaponClass.IsValidTarget(Target)) return false;
            if (!WeaponClass.InRange(startPosition, Target)) return false;

            LengthAmount distanceToTarget = new LengthAmount(Vector2.Distance(startPosition, Target.Position),LengthAmount.CORE_UNIT);
            float hitProbability = GetHitProbabilityForTarget(Target, distanceToTarget);
            float minHitProbability = GetHitProbabilityThresholdForTarget(Target);
            
            return hitProbability >= minHitProbability;
        }
        
        public void SetTarget(AttackableObject target, bool isManual)
        {
            Target = target;
            HasManualTarget = isManual;
        }

        public void ClearTarget()
        {
            Target = null;
            HasManualTarget = false;
        }

        public void FireWeapon(out int attackCount)
        {
            attackCount = WeaponClass.shotsPerAttack;
            
            _timeOfNextShot = Time.time + WeaponClass.ReloadTime;
        }

        public bool IsValidTarget(AttackableType targetAttackableType)
        {
            return targetAttackableType switch
            {
                AttackableType.Projectile => WeaponClass.canAttackProjectiles,
                AttackableType.Ship => WeaponClass.canAttackShips,
                AttackableType.Any => true,
                _ => false
            };
        }
        
        private float GetHitProbabilityForTarget(AttackableObject target, LengthAmount distanceToTarget)
        {
            return target.AttackableType switch
            {
                AttackableType.Ship => WeaponClass.GetHitProbabilityPerShot(Target.TargetParams, distanceToTarget),
                AttackableType.Projectile => WeaponClass.GetProbabilityForAtLeastOneHit(Target.TargetParams, distanceToTarget),
                _ => 0f
            };
        }

        private float GetHitProbabilityThresholdForTarget(AttackableObject target)
        {
            return _minHitProbability;
        }

        public void SetMinimumHitProbability(float hitProbability)
        {
            _minHitProbability = hitProbability;
        }
    }
}