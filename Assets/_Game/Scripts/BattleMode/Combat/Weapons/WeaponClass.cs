﻿using System;
using _Game.Scripts.Utils;
using _Game.Scripts.Utils.Audio_Utils;
using BattleMode.Combat.Projectiles;
using BattleMode.UnitConversion;
using MathNet.Numerics.Distributions;
using UnityEngine;

namespace BattleMode.Combat.Weapons
{
    public abstract class WeaponClass : IndexedScriptableObject<WeaponClass>
    {
        public string weaponName;
        public WeaponCategory weaponCategory;
        public float damageMean = 10f;
        public float damageStDev = 2f;
        public float penetrationMean = 5f;
        public float penetrationStDev = 2f;
        public float range = 10f;
        public float reloadTime = 1f;
        public bool canAttackShips = true;
        public bool canAttackProjectiles = false;
        public int shotsPerAttack = 1;
        public AudioSound weaponFireAudioSound;

        public abstract ProjectileType ProjectileType { get; }
        public virtual LengthAmount Size { get; } = new LengthAmount(0, LengthAmount.Unit.Meters);
        public virtual AccelerationAmount ThrustAcceleration { get; } = new AccelerationAmount(0, AccelerationAmount.Unit.G);

        public virtual AccelerationAmount EvasionAcceleration { get; } = new AccelerationAmount(0, AccelerationAmount.Unit.G);

        public float ReloadTime => reloadTime;

        public bool IsValidTarget(AttackableObject target)
        {
            switch (target.AttackableType)
            {
                case AttackableType.Ship:
                    return canAttackShips;
                case AttackableType.Projectile:
                    return canAttackProjectiles;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
        public bool InRange(Vector2 startPosition, AttackableObject target)
        {
            return Vector2.Distance(startPosition, target.Position) <= range;
        }

        public abstract float GetHitProbabilityPerShot(TargetParams targetParams, LengthAmount distanceToTarget);

        public abstract float GetProbabilityForAtLeastOneHit(TargetParams targetParams, LengthAmount distanceToTarget);
    }

    public enum WeaponCategory
    {
        HeavyGun,
        MediumGun,
        LightGun,
        PointDefense,
        Missile
    }
}