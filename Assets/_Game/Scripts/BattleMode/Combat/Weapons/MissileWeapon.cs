﻿using BattleMode.Combat.Projectiles;
using BattleMode.Core;
using BattleMode.UnitConversion;
using UnityEngine;

namespace BattleMode.Combat.Weapons
{
    [CreateAssetMenu(fileName = "New MissileWeapon", menuName = "Weapons/Missile Weapon", order = 0)]
    public class MissileWeapon : WeaponClass
    {
        public AccelerationAmount thrustAcceleration = new AccelerationAmount(10, AccelerationAmount.Unit.G);
        public AccelerationAmount evasionAcceleration = new AccelerationAmount(0, AccelerationAmount.Unit.G);
        public LengthAmount averageSizeInM = new LengthAmount(0, LengthAmount.Unit.Meters);

        public override ProjectileType ProjectileType => ProjectileType.Missile;
        public override LengthAmount Size => averageSizeInM;
        public override AccelerationAmount ThrustAcceleration => thrustAcceleration;
        public override AccelerationAmount EvasionAcceleration => evasionAcceleration;

        public override float GetHitProbabilityPerShot(TargetParams targetParams, LengthAmount distanceToTarget)
        {
            return 1f;
        }

        public override float GetProbabilityForAtLeastOneHit(TargetParams targetTargetParams, LengthAmount distanceToTarget)
        {
            return 1f;
        }
    }
}