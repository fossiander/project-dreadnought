﻿using BattleMode.Combat.Projectiles;
using BattleMode.Core;
using BattleMode.UnitConversion;
using MathNet.Numerics.Distributions;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BattleMode.Combat.Weapons
{
    [CreateAssetMenu(fileName = "New KineticWeapon", menuName = "Weapons/Kinetic Weapon", order = 0)]
    public class KineticWeapon : WeaponClass
    {
        public VelocityAmount projectileSpeed = new VelocityAmount(50, VelocityAmount.Unit.KilometersPerSecond);
        public LengthAmount distanceToHit100MTargetAt95Pct = new LengthAmount(100, LengthAmount.Unit.Kilometers);

        private const float REFERENCE_TARGET_SIZE_IN_METERS = 100;
        private const float REFERENCE_SIGMA_VALUE = 1.96f;


        public override ProjectileType ProjectileType => ProjectileType.Direct;

        public override float GetHitProbabilityPerShot(TargetParams targetParams, LengthAmount distanceToTarget)
        {
            float chanceToHitPreEvasion = CalculateChanceToHitPreEvasion(targetParams, distanceToTarget);
            float evasionProbability = CalculateEvasionProbability(targetParams, distanceToTarget);

            return chanceToHitPreEvasion * (1 - evasionProbability);
        }

        public override float GetProbabilityForAtLeastOneHit(TargetParams targetParams, LengthAmount distanceToTarget)
        {
            float hitProbability = GetHitProbabilityPerShot(targetParams, distanceToTarget);

            if (float.IsNaN(hitProbability) || hitProbability <= 0) return 0;
            
            return 1 - (float)Binomial.CDF(hitProbability, shotsPerAttack, 0);
        }

        private float CalculateChanceToHitPreEvasion(TargetParams targetParams, LengthAmount distanceToTarget)
        {
            float sizeRatio = targetParams.averageSize.GetValueIn(LengthAmount.Unit.Meters) / 
                              REFERENCE_TARGET_SIZE_IN_METERS;
            float distanceRatio = distanceToHit100MTargetAt95Pct.GetValueInCoreUnits() / 
                                  distanceToTarget.GetValueInCoreUnits();

            float xSigma = sizeRatio * distanceRatio * REFERENCE_SIGMA_VALUE;
            float chanceToHitPreEvasion = 2 * (float)Normal.CDF(0, 1, xSigma) - 1;
            return chanceToHitPreEvasion;
        }

        private float CalculateEvasionProbability(TargetParams targetParams, LengthAmount distanceToTarget)
        {
            float travelTimeInMinutes = distanceToTarget.GetValueIn(LengthAmount.Unit.Kilometers) / 
                                        projectileSpeed.GetValueIn(VelocityAmount.Unit.KilometersPerMinute);
            float evasionMovementUntilHitInKM = 0.5f * 
                                            targetParams.evasionAcceleration.GetValueIn(AccelerationAmount.Unit.KilometersPerMinuteSquared) * 
                                            travelTimeInMinutes * travelTimeInMinutes;

            float evasionProbability = Mathf.Clamp01(2 * evasionMovementUntilHitInKM / targetParams.averageSize.GetValueIn(LengthAmount.Unit.Kilometers));
            return evasionProbability;
        }

        [Button]
        public void RunTestCalculations()
        {
            TargetParams testParams = new TargetParams()
            {
                evasionAcceleration = new AccelerationAmount(10, AccelerationAmount.Unit.MetersPerSecondSquared),
                averageSize = new LengthAmount(100, LengthAmount.Unit.Meters)
            };
            
            LengthAmount distanceToTarget = new LengthAmount(10, LengthAmount.Unit.Kilometers);

            float chanceToHit = CalculateChanceToHitPreEvasion(testParams, distanceToTarget);
            float evasionChance = CalculateEvasionProbability(testParams, distanceToTarget);
            float totalProbability = chanceToHit * (1 - evasionChance);
            
            Debug.Log($"{weaponName} | Total probability: {totalProbability:P1}, chance to hit of {chanceToHit:P1} vs. evasion chance of {evasionChance:P1}.");
        }
    }
}