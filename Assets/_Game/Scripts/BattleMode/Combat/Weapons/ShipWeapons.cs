using System.Collections.Generic;
using BattleMode.Core;
using UnityEngine;

namespace BattleMode.Combat.Weapons
{
    public class ShipWeapons : MonoBehaviour
    {
        public List<WeaponInstance> Weapons { get; } = new List<WeaponInstance>();

        public void InitOnServer(ShipClass.ShipClassWeapons[] shipClassWeapons)
        {
            foreach (ShipClass.ShipClassWeapons weaponSet in shipClassWeapons)
            {
                for (int i = 0; i < weaponSet.weaponCount; i++)
                {
                    Weapons.Add(new WeaponInstance(weaponSet.weaponClass));
                }
            }
        }
    }
}
