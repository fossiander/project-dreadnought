﻿using System.Collections.Generic;
using BattleMode.Combat.Projectiles;
using BattleMode.Combat.Weapons;
using BattleMode.Core;
using BattleMode.Movement;
using BattleMode.UnitConversion;
using UnityEngine;

namespace BattleMode.Combat
{
    public class AttackGroup
    {
        public int OwnerID { get; }
        public ProjectileType ProjectileType { get; }
        public MovableEntity Target { get; }
        public List<Attack> AttackList { get; } = new List<Attack>();
        public float TotalDamage { get; private set; }
        public int TotalAttacks { get; private set; }

        public LengthAmount AverageSizeInM => _totalSize.DivideAmountBy(TotalAttacks);
        public AccelerationAmount ThrustAcceleration { get; }
        public AccelerationAmount AverageEvasionAccelerationInMPSS => _totalEvasionAcceleration.DivideAmountBy(TotalAttacks);

        private LengthAmount _totalSize;
        private AccelerationAmount _totalEvasionAcceleration;

        public AttackGroup(int ownerID, ProjectileType projectileType, MovableEntity target, AccelerationAmount thrustAcceleration)
        {
            OwnerID = ownerID;
            ProjectileType = projectileType;
            Target = target;
            ThrustAcceleration = thrustAcceleration;
        }
        
        public void AddAttack(Attack attack)
        {
            if (attack.Target.MovableEntity != Target) 
                Debug.LogError($"Target {attack.Target.name} different from attack group target ({Target.name})");
            if (attack.WeaponClass.ProjectileType != ProjectileType) 
                Debug.LogError($"Attack projectile type {attack.WeaponClass.ProjectileType.ToString()} different from attack group target ({ProjectileType.ToString()})");
            
            AttackList.Add(attack);

            _totalSize = _totalSize.AddAmount(attack.WeaponClass.Size.MultiplyAmountWith(attack.NumAttacks));
            _totalEvasionAcceleration = _totalEvasionAcceleration.AddAmount(attack.WeaponClass.EvasionAcceleration.MultiplyAmountWith(attack.NumAttacks));
            
            TotalDamage += attack.TotalDamage;
            TotalAttacks += attack.NumAttacks;
        }

        public (int, float) RemoveRandomAttacks(int numAttacks)
        {
            if (TotalAttacks <= 0)
            {
                Debug.LogWarning("Trying to destroy missiles when there were none left");
                return (0, 0);
            }
            
            int totalRemovedAttacksCount = 0;
            float totalRemovedDamage = 0;
            
            List<int> attacksToRemove = new List<int>();
            Dictionary<Attack, int> attackIndicesToRemove = new Dictionary<Attack, int>();
            
            for (int i = 0; i < numAttacks; i++)
            {
                int index = Random.Range(0, TotalAttacks);

                if (attacksToRemove.Contains(index)) continue;

                Attack attackToRemove = GetAttackForIndex(index);
                if (!attackIndicesToRemove.ContainsKey(attackToRemove)) attackIndicesToRemove[attackToRemove] = 0;

                attackIndicesToRemove[attackToRemove]++;
                attacksToRemove.Add(index);
            }

            foreach (Attack attack in attackIndicesToRemove.Keys)
            {
                int numAttacksToRemove = attackIndicesToRemove[attack];
                float removedDamage = attack.RemoveAttack(numAttacksToRemove);
                
                totalRemovedAttacksCount += numAttacksToRemove;
                totalRemovedDamage += removedDamage;
            }
            
            TotalAttacks -= totalRemovedAttacksCount;
            TotalDamage -= totalRemovedDamage;

            return (totalRemovedAttacksCount, totalRemovedDamage);
        }

        private Attack GetAttackForIndex(int attackIndex)
        {
            if (attackIndex >= TotalAttacks) Debug.LogError($"Attack index of {attackIndex} is higher than total number of attacks ({TotalAttacks})");

            int startIndex = 0;
            int endIndex;
            foreach (Attack attack in AttackList)
            {
                endIndex = startIndex + attack.NumAttacks;

                if (attackIndex >= startIndex && attackIndex < endIndex) return attack;
                startIndex = endIndex;
            }

            return null;
        }
    }
}