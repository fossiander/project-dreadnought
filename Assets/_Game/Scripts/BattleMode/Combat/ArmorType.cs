﻿using UnityEngine;

namespace BattleMode.Combat
{
    [CreateAssetMenu(fileName = "New ArmorType", menuName = "ArmorTypes", order = 0)]
    public class ArmorType : ScriptableObject
    {
        private const float HULL_PENETRATION_LEVEL = 10f;
        
        public string armorName = "New armor";
        public float integrityPerStrengthPoint = 10f;
        public AnimationCurve hardnessCurve = new(new Keyframe(0, 1), new Keyframe(1, 0));

        public float GetPenetrationLevel(float armorStrength, float armorIntegrityPct)
        {
            return hardnessCurve.Evaluate(1 - armorIntegrityPct) * armorStrength;
        }
        
        public float GetOverPenetrationLevel(float armorStrength, float armorIntegrityPct)
        {
            return HULL_PENETRATION_LEVEL + GetPenetrationLevel(armorStrength, armorIntegrityPct);
        }
    }
}