﻿using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Utils.Event_Utils;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Events;

namespace BattleMode.Combat
{
    public class AttackCommandReceiver : NetworkBehaviour
    {
        [SerializeField] private AttackHandler attackHandler;
        public UnityEvent OnAttackCommandReceived { get; } = new UnityEvent();

        public void SetAttackTarget(NetworkBehaviourReference targetReference, int[] weaponGroups = null)
        {
            weaponGroups ??= GetSelectedWeaponGroups();
            
            SetAttackTargetServerRpc(targetReference, weaponGroups);
            SetAttackTargetLocal(targetReference, weaponGroups);
            TriggerAttackCommandReceivedEvent(targetReference);
        }

        [ServerRpc]
        private void SetAttackTargetServerRpc(NetworkBehaviourReference targetReference, int[] weaponGroups)
        {
            SetAttackTargetLocal(targetReference, weaponGroups);
        }

        private void SetAttackTargetLocal(NetworkBehaviourReference targetReference, int[] weaponGroups)
        {
            if (!targetReference.TryGet(out AttackableObject target))
                Debug.LogError($"No attackable object found for {targetReference}");

            attackHandler.SetManualAttackTarget(target, weaponGroups);
        }

        public void ClearAttackTarget(int[] weaponGroups = null)
        {
            weaponGroups ??= GetSelectedWeaponGroups();
            
            ClearAttackTargetServerRpc(weaponGroups);
            ClearAttackTargetLocal(weaponGroups);
        }
        
        [ServerRpc]
        private void ClearAttackTargetServerRpc(int[] weaponGroups)
        {
            ClearAttackTargetLocal(weaponGroups);
        }

        private void ClearAttackTargetLocal(int[] weaponGroups)
        {
            attackHandler.SetManualAttackTarget(null, weaponGroups);
        }
        
        public void SetAutoTargetingActive(bool isActive, int[] weaponGroups = null)
        {
            weaponGroups ??= GetSelectedWeaponGroups();

            SetAutoTargetingActiveServerRpc(isActive, weaponGroups);
            SetAutoTargetingActiveLocal(isActive, weaponGroups);
        }

        [ServerRpc]
        private void SetAutoTargetingActiveServerRpc(bool isActive, int[] weaponGroups)
        {
            SetAutoTargetingActiveLocal(isActive, weaponGroups);
        }

        private void SetAutoTargetingActiveLocal(bool isActive, int[] weaponGroups)
        {
            attackHandler.SetAutoTargetingActive(isActive, weaponGroups);
        }

        public void SetAutoTargetMode(WeaponGroup.TargetMode targetMode, int[] weaponGroups)
        {
            weaponGroups ??= GetSelectedWeaponGroups();
            
            SetAutoTargetModeServerRpc(targetMode, weaponGroups);
            SetAutoTargetModeLocal(targetMode, weaponGroups);
        }

        [ServerRpc]
        private void SetAutoTargetModeServerRpc(WeaponGroup.TargetMode targetMode, int[] weaponGroups)
        {
            SetAutoTargetModeLocal(targetMode, weaponGroups);
        }

        private void SetAutoTargetModeLocal(WeaponGroup.TargetMode targetMode, int[] weaponGroups)
        {
            attackHandler.SetAutoTargetMode(targetMode, weaponGroups);
        }
        
        public void SetAutoTargetAggressiveness(WeaponGroup.HitProbabilityThreshold probabilityThreshold, int[] weaponGroups)
        {
            weaponGroups ??= GetSelectedWeaponGroups();
            
            SetAutoTargetAggressivenessServerRpc(probabilityThreshold, weaponGroups);
            SetAutoTargetAggressivenessLocal(probabilityThreshold, weaponGroups);
        }

        [ServerRpc]
        private void SetAutoTargetAggressivenessServerRpc(WeaponGroup.HitProbabilityThreshold probabilityThreshold, int[] weaponGroups)
        {
            SetAutoTargetAggressivenessLocal(probabilityThreshold, weaponGroups);
        }

        private void SetAutoTargetAggressivenessLocal(WeaponGroup.HitProbabilityThreshold probabilityThreshold, int[] weaponGroups)
        {
            attackHandler.SetHitProbabilityThreshold(probabilityThreshold, weaponGroups);
        }

        private void TriggerAttackCommandReceivedEvent(NetworkBehaviourReference targetReference)
        {
            OnAttackCommandReceived?.Invoke();
            
            if (targetReference.TryGet(out NetworkBehaviour networkBehaviour))
            {
                EventSystem.TriggerEvent(new AttackCommandReceivedEvent()
                {
                    position = networkBehaviour.transform.position
                });
            }
        }
        
        private int[] GetSelectedWeaponGroups()
        {
            List<int> selectedWeaponGroups = new List<int>();
            
            foreach (WeaponGroup weaponGroup in attackHandler.WeaponGroups)
            {
                if (weaponGroup.IsSelected.Value)
                {
                    selectedWeaponGroups.Add(weaponGroup.WeaponGroupID);
                }
            }

            return selectedWeaponGroups.ToArray();
        }
    }

    public class AttackCommandReceivedEvent : Event<AttackCommandReceivedEvent>
    {
        public Vector2 position;
    }
}