﻿using System;
using _Game.Scripts.Utils;
using _Game.Scripts.Utils.Audio_Utils;
using BattleMode.Combat;
using BattleMode.Control;
using BattleMode.Core;
using BattleMode.Movement;
using BattleMode.UI;
using MathNet.Numerics;
using UnityEngine;

namespace BattleMode.Audio
{
    public class FlotillaVoice : MonoBehaviour
    {
        [SerializeField] private OwnedObject ownedObject = null;
        [SerializeField] private SelectableObject flotillaSelectableObject = null;
        [SerializeField] private MovementCommandReceiver flotillaMovementCommandReceiver = null;
        [SerializeField] private AttackCommandReceiver flotillaAttackCommandReceiver = null;
        [SerializeField] private AudioSound selectionSound = null;
        [SerializeField] private AudioSound movementConfirmationSound = null;
        [SerializeField] private AudioSound attackConfirmationSound = null;
        [SerializeField] private AudioSound stopConfirmationSound = null;
        

        private readonly LazyObjectFinder<AudioManager> _audioManager = new LazyObjectFinder<AudioManager>();

        private void OnEnable()
        {
            flotillaSelectableObject.OnSelected.AddListener(PlaySelectionSound);
            flotillaMovementCommandReceiver.OnMovementCommandReceived.AddListener(PlayMovementConfirmationSound);
            flotillaMovementCommandReceiver.OnStopCommandReceived.AddListener(PlayStopConfirmationSound);
            flotillaAttackCommandReceiver.OnAttackCommandReceived.AddListener(PlayAttackConfirmationSound);
        }

        private void OnDisable()
        {
            flotillaSelectableObject.OnSelected.RemoveListener(PlaySelectionSound);
            flotillaMovementCommandReceiver.OnMovementCommandReceived.RemoveListener(PlayMovementConfirmationSound);
            flotillaMovementCommandReceiver.OnStopCommandReceived.RemoveListener(PlayStopConfirmationSound);
            flotillaAttackCommandReceiver.OnAttackCommandReceived.RemoveListener(PlayAttackConfirmationSound);
        }

        private void PlaySelectionSound()
        {
            if (selectionSound == null) return;
            
            if (ownedObject.OwnerID != Player.LocalPlayerID.Value) return;
            
            _audioManager.Get().PlaySound2D(selectionSound);
        }

        private void PlayMovementConfirmationSound()
        {
            if (movementConfirmationSound == null) return;
            
            if (ownedObject.OwnerID != Player.LocalPlayerID.Value) return;
            
            _audioManager.Get().PlaySound2D(movementConfirmationSound);
        }

        private void PlayStopConfirmationSound()
        {
            if (stopConfirmationSound == null) return;
            
            if (ownedObject.OwnerID != Player.LocalPlayerID.Value) return;
            
            _audioManager.Get().PlaySound2D(stopConfirmationSound);
        }

        private void PlayAttackConfirmationSound()
        {
            if (attackConfirmationSound == null) return;

            if (ownedObject.OwnerID != Player.LocalPlayerID.Value) return;
            
            _audioManager.Get().PlaySound2D(attackConfirmationSound);
        }
    }
}