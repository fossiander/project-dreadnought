﻿using UnityEngine;

namespace _Game.Scripts.Utils
{
    public static class VectorUtils
    {
        public static Vector2 GetResidualAccelerationVector(Vector2 targetMovement, Vector2 existingVector, float acceleration, bool positive)
        {
            float scalar = GetResidualScalar(targetMovement, existingVector, acceleration, positive);
            return scalar * targetMovement;
        }

        /// <summary>
        /// Calculates the scalar of how much additional of "targetMovement" fits into a vector such that the total magnitude equals "acceleration"
        /// </summary>
        private static float GetResidualScalar(Vector2 targetMovement, Vector2 existingVector, float targetMagnitude, bool positive)
        {
            float sign = positive ? 1f : -1f;
            float component1 = targetMagnitude * targetMagnitude * targetMovement.SqrMagnitude();
            float component2 = targetMovement.y * existingVector.x - targetMovement.x * existingVector.y;
            float component3 = targetMovement.x * existingVector.x + targetMovement.y * existingVector.y;
            float component4 = targetMovement.SqrMagnitude();

            return sign * (Mathf.Sqrt(component1 - component2 * component2) - sign * component3) / component4;
        }

    }
}