﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;
using Object = UnityEngine.Object;

namespace _Game.Scripts.Utils
{
    public class SimpleComponentPool<T> : IObjectPool<T> where T : Component
    {
        private readonly ObjectPool<T> _internalPool;

        private readonly T _prefab;
        private readonly Transform _parentTransform;

        private readonly List<T> _activeObjects = new List<T>();

        public SimpleComponentPool(T prefab, Transform parentTransform, int defaultPoolSize, int maxPoolSize)
        {
            _prefab = prefab;
            _parentTransform = parentTransform;
            
            _internalPool = new ObjectPool<T>(
                CreatePoolItem,
                OnTakeFromPool,
                OnReturnToPool,
                OnDestroyPoolObject,
                true,
                defaultPoolSize,
                maxPoolSize);
        }
        
        public SimpleComponentPool(Func<T> createFunc, int defaultPoolSize, int maxPoolSize)
        {
            _internalPool = new ObjectPool<T>(
                createFunc,
                OnTakeFromPool,
                OnReturnToPool,
                OnDestroyPoolObject,
                true,
                defaultPoolSize,
                maxPoolSize);
        }

        private T CreatePoolItem()
        {
            return Object.Instantiate(_prefab, _parentTransform);
        }

        private void OnTakeFromPool(T obj)
        {
            obj.gameObject.SetActive(true);
        }

        private void OnReturnToPool(T obj)
        {
            obj.gameObject.SetActive(false);
        }

        private void OnDestroyPoolObject(T obj)
        {
            Object.Destroy(obj.gameObject);
        }

        public T Get()
        {
            T obj = _internalPool.Get();
            _activeObjects.Add(obj);
            return obj;
        }

        public PooledObject<T> Get(out T v)
        {
            PooledObject<T> pObj = _internalPool.Get(out T obj);
            _activeObjects.Add(obj);
            v = obj;
            return pObj;
        }

        public void Release(T element)
        {
            _activeObjects.Remove(element);
            _internalPool.Release(element);
        }

        public void Clear()
        {
            ReleaseAll();
            _internalPool.Clear();
        }

        public void ReleaseAll()
        {
            foreach (T obj in _activeObjects)
            {
                _internalPool.Release(obj);
            }
            _activeObjects.Clear();
        }

        public int CountInactive => _internalPool.CountInactive;
    }
}