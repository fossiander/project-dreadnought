﻿using System;

namespace _Game.Scripts.Utils
{
    public class LazyInit<T> 
    {
        private bool _isInit = false;
        private T _reference;

        private readonly Func<T> _initFunc;
        
        public LazyInit(Func<T> initFunc)
        {
            _initFunc = initFunc;
        }

        public T Get()
        {
            if (!_isInit) Init();

            return _reference;
        }

        public void Init()
        {
            if (_isInit) return;

            _reference = _initFunc();
        }
    }
}