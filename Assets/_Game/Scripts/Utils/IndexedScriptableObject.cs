﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Game.Scripts.Utils
{
    public class IndexedScriptableObject<T> : ScriptableObject where T : IndexedScriptableObject<T>
    {
        [SerializeField] private int uniqueId = -1;
        
        private void OnValidate()
        {
            T obj = GetObjectWithID(uniqueId);
            if (uniqueId == -1 || (obj != null && obj != this))
            {
                GenerateNewUniqueID();
            }
        }
        
        [Button]
        private void GenerateNewUniqueID()
        {
            uniqueId = Random.Range(1, int.MaxValue);
            
            T obj = GetObjectWithID(uniqueId);
            
            if (obj != null && obj != this) Debug.LogError("Duplicative object ID, regenerate object ID once more!");
        }
        
        public static T GetObjectWithID(int id)
        {
            T[] items = Resources.FindObjectsOfTypeAll<T>();

            foreach (T item in items)
            {
                if (item.uniqueId == id) return item;
            }

            return null;
        }

        public int GetUniqueID() => uniqueId;
    }
}