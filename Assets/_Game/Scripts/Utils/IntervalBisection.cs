﻿#pragma warning disable 0162

using System;
using UnityEngine;

namespace _Game.Scripts.Utils
{
    public static class IntervalBisection
    {
        private const float TARGET_ERROR = 0.0001f;
        private const int MAX_ITERATIONS = 100;
        private const bool LOG_SUCCESS = false;
        private const bool LOG_DETAIL = false;

        public delegate float EstimatorFunction<in TInput,TOutput>(float value, TInput inputParams, out TOutput outputParams);

        public static TOutput Estimate<TInput, TOutput>(TInput inputParams, EstimatorFunction<TInput, TOutput> function, float valueLowerBound, float valueUpperBound)
        {
            return Estimate(inputParams, function, valueLowerBound, valueUpperBound, MidPointIntersection);
        }

        public static TOutput Estimate<TInput,TOutput>(TInput inputParams, EstimatorFunction<TInput,TOutput> function, float valueLowerBound, float valueUpperBound, Func<float,float,float,float,float> intersectionWeightingFunction)
        {
            TOutput outputParams;

            float errLowerBound = function(valueLowerBound, inputParams, out outputParams);
            if (Mathf.Abs(errLowerBound) < TARGET_ERROR) return outputParams;

            float errUpperBound = function(valueUpperBound, inputParams, out outputParams);
            if (Mathf.Abs(errUpperBound) < TARGET_ERROR) return outputParams;

            if (errLowerBound * errUpperBound > TARGET_ERROR)
            {
                Debug.LogWarning("Invalid outside bounds of optimization!");
            }

            for (int i = 0; i < MAX_ITERATIONS; i++)
            {
                float valueMid = intersectionWeightingFunction(valueLowerBound, valueUpperBound, errLowerBound, errUpperBound);
                float errMid = function(valueMid, inputParams, out outputParams);

                if (errLowerBound * errMid < 0)
                {
                    errUpperBound = errMid;
                    valueUpperBound = valueMid;
                }
                else
                {
                    errLowerBound = errMid;
                    valueLowerBound = valueMid;
                }

                if (LOG_DETAIL)
                {
                    Debug.Log($"Iteration {i}: value {valueMid}, error {errMid}");   
                }

                if (Mathf.Abs(errMid) < TARGET_ERROR)
                {
                    if (LOG_SUCCESS) Debug.Log($"Good enough solution found after {i + 1} iterations.");
                    break;
                }

                if (i == MAX_ITERATIONS - 1)
                    Debug.LogWarning($"No good solution was found after {MAX_ITERATIONS} iterations.");
            }
            
            return outputParams;
        }

        public static float MidPointIntersection(float value1, float value2, float error1, float error2)
        {
            return 0.5f * (value1 + value2);
        }

        private const float MIN_WEIGHT = 0.1f;
        public static float ErrorWeightedIntersection(float value1, float value2, float error1, float error2)
        {
            float weight = Mathf.Clamp(Mathf.Abs(error2) / (Mathf.Abs(error1) + Mathf.Abs(error2)), MIN_WEIGHT, 1 - MIN_WEIGHT);

            return weight * value1 + (1 - weight) * value2;
        }
    }
}