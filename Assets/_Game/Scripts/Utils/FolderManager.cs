﻿using UnityEngine;

namespace _Game.Scripts.Utils
{
    public class FolderManager : MonoBehaviour
    {
        public Transform ProjectileFolder => GetOrInitialise(ref _projectileFolder, "Projectiles");
        Transform _projectileFolder;
        
        public Transform PathFolder => GetOrInitialise(ref _pathFolder, "Paths");
        private Transform _pathFolder;

        private Transform GetOrInitialise(ref Transform transformReference, string folderName)
        {
            if (transformReference == null) transformReference = new GameObject(folderName).transform;
            return transformReference;
        }
    }
}