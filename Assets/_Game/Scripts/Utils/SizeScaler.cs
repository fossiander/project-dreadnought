﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Game.Scripts.Utils
{
    [System.Serializable]
    public class SizeScaler
    {
        [SerializeField] private ScalerType scalerType = ScalerType.Log10;
        
        [ShowIf(nameof(IsMinMax))][InlineProperty] [SerializeField] private MinMaxFloat inputValueRange = new MinMaxFloat(0, 1);
        [ShowIf(nameof(IsMinMax))][InlineProperty] [SerializeField] private MinMaxFloat outputValueRange = new MinMaxFloat(0, 1);
        [ShowIf(nameof(IsMinMax))][InlineProperty] [SerializeField] private AnimationCurve conversionCurve = AnimationCurve.Linear(0,0,1,1);

        [ShowIf(nameof(IsLogarithmic))][SerializeField] private float logBaseInput = 10;
        [ShowIf(nameof(IsLogarithmic))][SerializeField] private float logBaseOutput = 0.2f;

        private bool IsMinMax => scalerType == ScalerType.MinMax;
        private bool IsLogarithmic() => scalerType is ScalerType.Log2 or ScalerType.Log10;

        public float GetOutput(float inputValue)
        {
            switch (scalerType)
            {
                case ScalerType.MinMax:
                    return outputValueRange.GetValueAtPercentage(conversionCurve.Evaluate(inputValueRange.GetPercentageOfValue(inputValue)));
                case ScalerType.Log10:
                    return inputValue <= 0 ? 0 : logBaseOutput * Mathf.Log10(10 * inputValue / logBaseInput);
                case ScalerType.Log2:
                    return inputValue <= 0 ? 0 : logBaseOutput * Mathf.Log(2 * inputValue / logBaseInput, 2);
                default:
                    throw new ArgumentOutOfRangeException(); 
            }
        }

        enum ScalerType
        {
            MinMax,
            Log10,
            Log2
        }
        
    }
}