﻿using UnityEngine;

namespace _Game.Scripts.Utils
{
    public class LazyComponentGetter<T> 
    {
        private bool _isInit = false;
        private T _reference;

        private readonly MonoBehaviour _baseMonoBehaviour;
        private readonly bool _inChildren;

        public LazyComponentGetter(MonoBehaviour baseMonoBehaviour, bool inChildren = false)
        {
            _baseMonoBehaviour = baseMonoBehaviour;
            _inChildren = inChildren;
        }

        public T Get()
        {
            if (!_isInit) Init();

            return _reference;
        }

        public void Init()
        {
            if (_isInit) return;

            _reference = !_inChildren ? _baseMonoBehaviour.GetComponent<T>() : _baseMonoBehaviour.GetComponentInChildren<T>();
        }
    }
}