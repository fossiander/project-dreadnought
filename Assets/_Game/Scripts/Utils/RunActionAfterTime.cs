﻿using System;
using UnityEngine;

namespace _Game.Scripts.Utils
{
    public class RunActionAfterTime : MonoBehaviour
    {
        [SerializeField] private float delay = 2f;
        [SerializeField] private bool useUnscaledTime = false;

        private bool _isActive = false;
        private Action _actionAfterTime;
        private float _timeOfAction;
        
        public void SetAction(Action action)
        {
            _isActive = true;
            _actionAfterTime = action;
            _timeOfAction = useUnscaledTime ? Time.unscaledTime + delay : Time.time + delay;
        }

        private void Update()
        {
            if (!_isActive) return;
            
            if ((useUnscaledTime ? Time.unscaledTime : Time.time) >= _timeOfAction)
            {
                _actionAfterTime?.Invoke();
            }
        }
    }
}