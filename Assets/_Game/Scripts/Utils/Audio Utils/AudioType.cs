﻿namespace _Game.Scripts.Utils.Audio_Utils
{
    public enum AudioType
    {
        Effect,
        UI,
        Voice,
        Music,
        Master
    }
}