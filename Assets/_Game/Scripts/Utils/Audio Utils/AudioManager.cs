using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Pool;

namespace _Game.Scripts.Utils.Audio_Utils
{
    public class AudioManager : MonoBehaviour
    {
        [SerializeField] private AudioMixer masterMixer = null;
        [SerializeField] private AudioMixerGroup musicMixer = null;
        [SerializeField] private AudioMixerGroup effectMixer = null;
        [SerializeField] private AudioMixerGroup voiceMixer = null;
        [SerializeField] private AudioMixerGroup uiMixer = null;

        [SerializeField] private float musicFadeInDuration = 3f;
        [SerializeField] private AnimationCurve musicFadeInCurve = new(new Keyframe(0,0), new Keyframe(1,1));

        [SerializeField] private float musicFadeOutDuration = 3f;
        [SerializeField] private AnimationCurve musicFadeOutCurve = new(new Keyframe(0,1), new Keyframe(1,0));

        [SerializeField] private int defaultPoolSize = 10;
        [SerializeField] private int maxPoolSize = 100;

        [FoldoutGroup("Example sounds")] [SerializeField] private AudioSound masterExampleSound;
        [FoldoutGroup("Example sounds")] [SerializeField] private AudioSound musicExampleSound;
        [FoldoutGroup("Example sounds")] [SerializeField] private AudioSound effectExampleSound;
        [FoldoutGroup("Example sounds")] [SerializeField] private AudioSound voiceExampleSound;
        [FoldoutGroup("Example sounds")] [SerializeField] private AudioSound uiExampleSound;

        private readonly Dictionary<AudioSound, float> _timeNextPlayableBySound = new Dictionary<AudioSound, float>();
        private readonly ObjectPool<AudioPlayer> _audioPlayerPool;

        private AudioPlayer _musicAudioPlayer = null;

        private const float MIN_VOLUME = -30f;
        private const float MAX_VOLUME = 0f;

        private float _nextExampleSoundTime = Mathf.NegativeInfinity;
        private const float EXAMPLE_SOUND_DELAY = 0.05f;
        
        private AudioManager()
        {
            _audioPlayerPool = new ObjectPool<AudioPlayer>(
                CreatePoolItem, 
                OnTakeFromPool, 
                OnReturnToPool, 
                OnDestroyPoolObject,
                true,
                defaultPoolSize,
                maxPoolSize);
        }

        private void Start()
        {
            LoadAudioMixerVolumeFromPlayerPrefs();
        }

        private AudioPlayer CreatePoolItem()
        {
            AudioPlayer audioPlayer = new GameObject("Audio Source").AddComponent<AudioPlayer>();
            audioPlayer.SetAudioSource(audioPlayer.gameObject.AddComponent<AudioSource>());
            audioPlayer.transform.parent = transform;
            return audioPlayer;
        }

        private void OnTakeFromPool(AudioPlayer obj)
        {
            obj.gameObject.SetActive(true);
        }

        private void OnReturnToPool(AudioPlayer obj)
        {
            obj.gameObject.SetActive(false);
        }

        private void OnDestroyPoolObject(AudioPlayer obj)
        {
            Destroy(obj.gameObject);
        }

        public void PlaySound2D(AudioSound audioSound, float pitchAdjustment = 0, int numRepeats = 0)
        {
            PlaySound(audioSound, pitchAdjustment, null, null, numRepeats);
        }
        
        public void PlaySound3D(AudioSound audioSound, float pitchAdjustment, Vector3 position, int numRepeats = 0)
        {
            PlaySound(audioSound, pitchAdjustment, position, null, numRepeats);
        }

        public void PlaySoundTracking3D(AudioSound audioSound, float pitchAdjustment, Transform trackedTransform, int numRepeats = 0)
        {
            PlaySound(audioSound, pitchAdjustment, null, trackedTransform, numRepeats);
        }

        private void PlaySound(AudioSound audioSound, float pitchAdjustment, Vector3? position, Transform trackedTransform, int numRepeats)
        {
            if (!IsSoundPlayable(audioSound)) return;

            AudioPlayer audioPlayer = _audioPlayerPool.Get();

            int repeatCount = Math.Min(numRepeats, audioSound.maxRepeats);

            audioPlayer.PlayAudioClip(
                audioSound.GetRandomClip(), 
                GetAudioMixerGroupForSoundType(audioSound.audioType), 
                audioSound.GetRandomPitch() * (1 + pitchAdjustment), 
                audioSound.volume, 
                () => _audioPlayerPool.Release(audioPlayer),
                false,
                0f,
                position,
                trackedTransform,
                repeatCount,
                audioSound.repeatSoundDelay);
            
            if (audioSound.minDelayBetweenSounds > 0)
            {
                float delay = audioSound.minDelayBetweenSounds;
                if (repeatCount > 0) delay += repeatCount * audioSound.repeatSoundDelay;
                
                _timeNextPlayableBySound[audioSound] = Time.unscaledTime + delay;
            }
        }

        private bool IsSoundPlayable(AudioSound audioSound)
        {
            if (audioSound == null) return false;
            if (audioSound.minDelayBetweenSounds == 0) return true;
            if (!_timeNextPlayableBySound.ContainsKey(audioSound)) return true;
            return Time.unscaledTime >= _timeNextPlayableBySound[audioSound];
        }

        private AudioMixerGroup GetAudioMixerGroupForSoundType(AudioType audioType)
        {
            return audioType switch
            {
                AudioType.Effect => effectMixer,
                AudioType.UI => uiMixer,
                AudioType.Voice => voiceMixer,
                AudioType.Music => musicMixer,
                AudioType.Master => null,
                _ => throw new ArgumentOutOfRangeException(nameof(audioType), audioType, null)
            };
        }

        public void PlayMusicClip(AudioClip audioClip, bool looping, float volume)
        {
            AudioPlayer audioPlayer = _audioPlayerPool.Get();

            AudioMixerGroup audioMixerGroup = GetAudioMixerGroupForSoundType(AudioType.Music);
            float pitch = 1;

            audioPlayer.PlayAudioClip(audioClip, audioMixerGroup, pitch, volume, () =>
            {
                _audioPlayerPool.Release(audioPlayer);
            }, looping, 0);

            if (_musicAudioPlayer != null)
            {
                FadeOutCurrentMusic();
                _musicAudioPlayer.FadeIn(musicFadeInDuration, musicFadeInCurve);
            }

            _musicAudioPlayer = audioPlayer;
        }

        private void FadeOutCurrentMusic()
        {
            _musicAudioPlayer.FadeOut(musicFadeOutDuration, musicFadeOutCurve);
        }

        public bool IsMusicPlaying() => _musicAudioPlayer != null;

        public void SetAudioMixerVolume(AudioType audioType, float percentValue)
        {
            float value = MIN_VOLUME + percentValue * (MAX_VOLUME - MIN_VOLUME);
            masterMixer.SetFloat($"{audioType.ToString()}Volume", value);
        }

        public float GetVolumePercentage(AudioType audioType)
        {
            string key = $"{audioType.ToString()}Volume";
            masterMixer.GetFloat(key, out float value);
            float pctValue = (value - MIN_VOLUME) / (MAX_VOLUME - MIN_VOLUME);
            return pctValue;
        }

        public void SaveAudioMixerVolumeToPlayerPrefs()
        {
            foreach (AudioType audioType in Enum.GetValues(typeof(AudioType)))
            {
                string key = $"{audioType.ToString()}Volume";
                masterMixer.GetFloat(key, out float value);
                PlayerPrefs.SetFloat(key, value);
            }
            PlayerPrefs.Save();
        }

        public void LoadAudioMixerVolumeFromPlayerPrefs()
        {
            Debug.Log("Loading audio settings");
            foreach (AudioType audioType in Enum.GetValues(typeof(AudioType)))
            {
                string key = $"{audioType.ToString()}Volume";
                if (!PlayerPrefs.HasKey(key)) Debug.LogWarning($"Key {key} does not exist, default value was used!");
                float value = PlayerPrefs.GetFloat(key, 0f);
                masterMixer.SetFloat(key, value);
            }
        }

        public void PlayExampleSound(AudioType audioType)
        {
            AudioSound audioSoundToPlay = audioType switch
            {
                AudioType.Master => masterExampleSound,
                AudioType.Music => musicExampleSound,
                AudioType.Effect => effectExampleSound,
                AudioType.Voice => voiceExampleSound,
                AudioType.UI => uiExampleSound,
                _ => throw new ArgumentOutOfRangeException(nameof(audioType), audioType, null)
            };

            if (audioSoundToPlay != null && Time.unscaledTime >= _nextExampleSoundTime)
            {
                PlaySound2D(audioSoundToPlay);
                float delay = EXAMPLE_SOUND_DELAY + audioSoundToPlay.GetAudioLength();
                _nextExampleSoundTime = Time.unscaledTime + delay;
            }
        }
    }
}
