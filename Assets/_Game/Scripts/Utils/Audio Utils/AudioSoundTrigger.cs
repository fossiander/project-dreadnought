﻿using System;
using UnityEngine;

namespace _Game.Scripts.Utils.Audio_Utils
{
    public class AudioSoundTrigger : MonoBehaviour
    {
        [SerializeField] private AudioSound audioSound = null;
        [SerializeField] private bool playOnEnable = true;
        [SerializeField] private bool play3dSound = true;
        [SerializeField] private bool trackTransform = false;

        private readonly LazyObjectFinder<AudioManager> _audioManager = new LazyObjectFinder<AudioManager>();

        private void OnEnable()
        {
            if (!playOnEnable) return;

            PlaySound();
        }

        public void PlaySound(int numRepeats = 0)
        {
            if (audioSound == null) return;
            
            if (!play3dSound) _audioManager.Get().PlaySound2D(audioSound, 0, numRepeats);
            else if (!trackTransform) _audioManager.Get().PlaySound3D(audioSound, 0, transform.position, numRepeats);
            else _audioManager.Get().PlaySoundTracking3D(audioSound, 0, transform, numRepeats);
        }

        public void SetAudioSound(AudioSound sound)
        {
            audioSound = sound;
        }
    }
}