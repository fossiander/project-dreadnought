﻿using UnityEngine;

namespace _Game.Scripts.Utils.Audio_Utils
{
    [CreateAssetMenu(fileName = "New MusicTrackList", menuName = "Audio/Music Track List", order = 0)]
    public class MusicTrackList : ScriptableObject
    {
        [Range(0,1)] public float volume = 1;
        public MusicClip[] musicClips;
        
        public MusicClip GetRandomMusicClip()
        {
            return musicClips[Random.Range(0, musicClips.Length)];
        }
    }
    
    [System.Serializable]
    public struct MusicClip
    {
        public AudioClip audioClip;
        public bool looping;
    }
}