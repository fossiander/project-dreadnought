﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Audio;

namespace _Game.Scripts.Utils.Audio_Utils
{
    public class AudioPlayer : MonoBehaviour
    {
        private const float SPATIAL_BLEND_3D = 1f;
        
        private AudioSource _audioSource;

        private bool _trackTransform;
        private Transform _transformToTrack;

        private Action _onComplete;
        private float _timeToStopLooping;
        private bool _isLooping;

        private float _baseVolume;

        private bool _isFadingIn;
        private float _fadeInStartTime;
        private float _fadeInEndTime;
        private AnimationCurve _fadeInCurve;
        
        private bool _isFadingOut;
        private float _fadeOutStartTime;
        private float _fadeOutEndTime;
        private AnimationCurve _fadeOutCurve;

        private bool _runningRepeats = false;

        public void SetAudioSource(AudioSource audioSource)
        {
            _audioSource = audioSource;
        }

        private void Update()
        {
            if ((!_audioSource.isPlaying && !_runningRepeats) ||
                (_isLooping && Time.unscaledTime >= _timeToStopLooping)) 
                _onComplete?.Invoke();

            if (_isFadingIn)
            {
                UpdateFadeIn();

                if (Time.unscaledTime >= _fadeInEndTime) CompleteFadeIn();
            }
            
            if (_isFadingOut)
            {
                UpdateFadeOut();

                if (Time.unscaledTime >= _fadeOutEndTime) CompleteFadeOut();
            }

            if (_trackTransform)
            {
                transform.position = _transformToTrack.position;
            }
        }

        public void PlayAudioClip(AudioClip audioClip, AudioMixerGroup audioMixerGroup, float pitch, float volume, Action onComplete, bool looping = false, float loopStopTime = 0, Vector3? position = null, Transform trackedTransform = null, int numRepeats = 0, float repeatDelay = 0f)
        {
            bool is3d = trackedTransform != null || position != null;
            
            if (trackedTransform != null)
            {
                _trackTransform = true;
                _transformToTrack = trackedTransform;
                transform.position = trackedTransform.position;
            }
            else
            {
                _trackTransform = false;
                transform.position = position ?? Vector3.zero;
            }

            _audioSource.playOnAwake = false;
            _audioSource.loop = looping;
            _audioSource.clip = audioClip;
            _audioSource.outputAudioMixerGroup = audioMixerGroup;
            _audioSource.pitch = pitch;
            _audioSource.volume = volume;
            _audioSource.spatialBlend = is3d ? SPATIAL_BLEND_3D : 0;
            _onComplete = onComplete;
            
            _isLooping = looping;
            if (looping) _timeToStopLooping = loopStopTime > 0 ? Time.unscaledTime + loopStopTime : Mathf.Infinity;

            _baseVolume = volume;

            _audioSource.Play();

            if (numRepeats > 0)
            {
                _runningRepeats = true;
                StartCoroutine(CRepeatSounds(audioClip, numRepeats, repeatDelay));
            }
            else
            {
                _runningRepeats = false;
            }
        }

        private IEnumerator CRepeatSounds(AudioClip audioClip, int numRepeats, float repeatDelay)
        {
            WaitForSecondsRealtime delayWait = new WaitForSecondsRealtime(repeatDelay);

            for (int i = 0; i < numRepeats; i++)
            {
                yield return delayWait;
                _audioSource.PlayOneShot(audioClip);
            }

            yield return new WaitForSecondsRealtime(audioClip.length / _audioSource.pitch);
            _runningRepeats = false;
        }

        public void FadeIn(float fadeDuration, AnimationCurve fadeInCurve)
        {
            if (_isFadingOut) return;
            
            _isFadingIn = true;
            _fadeInStartTime = Time.unscaledTime;
            _fadeInEndTime = _fadeOutStartTime + fadeDuration;
            _fadeInCurve = fadeInCurve;
            UpdateFadeIn();
        }

        private void UpdateFadeIn()
        {
            float fadePercentage = Mathf.Clamp01((Time.unscaledTime - _fadeInStartTime) / (_fadeInEndTime - _fadeInStartTime));
            _audioSource.volume = _baseVolume * _fadeInCurve.Evaluate(fadePercentage);
        }

        private void CompleteFadeIn()
        {
            _isFadingIn = false;
        }

        public void FadeOut(float fadeDuration, AnimationCurve fadeOutCurve)
        {
            _baseVolume = _audioSource.volume; 
            _isFadingIn = false;
            
            _isFadingOut = true;
            _fadeOutStartTime = Time.unscaledTime;
            _fadeOutEndTime = _fadeOutStartTime + fadeDuration;
            _fadeOutCurve = fadeOutCurve;
            UpdateFadeOut();
        }

        private void UpdateFadeOut()
        {
            float fadePercentage = Mathf.Clamp01((Time.unscaledTime - _fadeOutStartTime) / (_fadeOutEndTime - _fadeOutStartTime));
            _audioSource.volume = _baseVolume * _fadeOutCurve.Evaluate(fadePercentage);
        }

        private void CompleteFadeOut()
        {
            _isFadingOut = false;
            _audioSource.Stop();
            _onComplete?.Invoke();
        }
    }
}