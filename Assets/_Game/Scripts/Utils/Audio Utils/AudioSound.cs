﻿using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Game.Scripts.Utils.Audio_Utils
{
    [CreateAssetMenu(fileName = "New AudioSound", menuName = "Audio/Audio Sound", order = 0)]
    public class AudioSound : ScriptableObject
    {
        public AudioType audioType;
        public AudioClip[] audioClips;
        [Range(-3,3)] public float basePitch = 1f;
        [Range(0, 0.5f)] public float pitchVariationPercentage = 0f;
        [Range(0, 10)] public float minDelayBetweenSounds = 0f;
        [Range(0, 1)] public float volume = 1f;
        public float repeatSoundDelay = 0.05f;
        [Range(1,10)] public int maxRepeats = 1;

        public AudioClip GetRandomClip()
        {
            return audioClips[Random.Range(0, audioClips.Length)];
        }

        public float GetRandomPitch()
        {
            return basePitch * (1 + Random.Range(-pitchVariationPercentage, pitchVariationPercentage));
        }

        public float GetAudioLength()
        {
            float returnValue = audioClips.Aggregate(Mathf.Infinity, (current, clip) => Mathf.Min(current, clip.length));

            returnValue /= basePitch;

            return returnValue;
        }
    }
}