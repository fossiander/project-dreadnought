using System;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Game.Scripts.Utils.Audio_Utils
{
    public class MusicPlayer : MonoBehaviour
    {
        [SerializeField] private bool autoPlay = true;
        [SerializeField] private MusicTrackList musicTrackList;
        
        private readonly LazyObjectFinder<AudioManager> _audioManager = new LazyObjectFinder<AudioManager>();

        private void Update()
        {
            if (!autoPlay) return;
            if (!_audioManager.Get().IsMusicPlaying()) PlayNextMusicClip(); 
        }

        [Button]
        private void PlayNextMusicClip()
        {
            MusicClip musicClip = musicTrackList.GetRandomMusicClip();
            _audioManager.Get().PlayMusicClip(musicClip.audioClip, musicClip.looping, musicTrackList.volume);
        }
    }
    
}
