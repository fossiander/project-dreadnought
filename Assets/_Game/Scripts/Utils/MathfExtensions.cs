﻿using UnityEngine;

namespace _Game.Scripts.Utils
{
    public static partial class MathfExtensions
    {
        public static float RoundToDigits(float value, int digits)
        {
            float scalar = Mathf.Pow(10, digits);
            return Mathf.Round(value * scalar) / scalar;
        }
    }
}