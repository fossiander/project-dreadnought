using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UIFloatValueTextSetter : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI textMesh = null;
        [SerializeField] private string prefix;
        [SerializeField] private string suffix;

        private string _format;
        private IEventVariable<float> _valueVariable;
        
        public void SetReference(IEventVariable<float> valueVariable, string format)
        {
            if (_valueVariable != null) _valueVariable.OnValueChanged -= OnValueChanged;
            
            _valueVariable = valueVariable;
            _valueVariable.OnValueChanged += OnValueChanged;

            _format = format;
            
            UpdateText();
        }
        
        private void OnValueChanged(float oldValue, float newValue)
        {
            UpdateText();
        }

        private void UpdateText()
        {
            float roundedValue = MathfExtensions.RoundToDigits(_valueVariable.Value, 5);
            
            textMesh.text = $"{prefix}{roundedValue.ToString(_format)}{suffix}";
        }
    }
}
