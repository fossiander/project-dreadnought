using System;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UIFloatValueBarSetter : MonoBehaviour
    {
        [SerializeField] private Image fillImage = null;

        private IEventVariable<float> _currentValueVariable;
        private IEventVariable<float> _maxValueVariable;
        
        public void SetReferences(IEventVariable<float> currentValueVariable, IEventVariable<float> maxValueVariable)
        {
            if (_currentValueVariable != null) _currentValueVariable.OnValueChanged -= OnCurrentValueChanged;
            if (_maxValueVariable != null) _maxValueVariable.OnValueChanged -= OnMaxValueChanged;
            
            _currentValueVariable = currentValueVariable;
            _maxValueVariable = maxValueVariable;
            
            _currentValueVariable.OnValueChanged += OnCurrentValueChanged;
            _maxValueVariable.OnValueChanged += OnMaxValueChanged;
            
            UpdateFill();
        }
        
        private void OnCurrentValueChanged(float oldValue, float newValue)
        {
            UpdateFill();
        }
        
        private void OnMaxValueChanged(float oldValue, float newValue)
        {
            UpdateFill();
        }

        private void UpdateFill()
        {
            fillImage.fillAmount = _currentValueVariable.Value / _maxValueVariable.Value;
        }
    }
}
