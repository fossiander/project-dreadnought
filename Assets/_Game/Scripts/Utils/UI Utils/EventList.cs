﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class EventList<T> : IList<T>
    {
        public event Action OnListChanged;
        public event Action<T> OnItemAdded;
        public event Action<T> OnItemRemoved; 
        public event Action OnListCleared;
        public event Action<T,T> OnItemChanged;

        private readonly List<T> _underlyingList = new List<T>();
        
        public IEnumerator<T> GetEnumerator()
        {
            return _underlyingList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            _underlyingList.Add(item);
            OnItemAdded?.Invoke(item);
            OnListChanged?.Invoke();
        }

        public void Clear()
        {
            _underlyingList.Clear();
            OnListCleared?.Invoke();
            OnListChanged?.Invoke();
        }

        public bool Contains(T item)
        {
           return _underlyingList.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _underlyingList.CopyTo(array,arrayIndex);
        }

        public bool Remove(T item)
        {
            bool removalSuccessful = _underlyingList.Remove(item);

            if (removalSuccessful)
            {
                OnItemRemoved?.Invoke(item);
                OnListChanged?.Invoke();
            }
            
            return removalSuccessful;
        }

        public int Count => _underlyingList.Count;
        public bool IsReadOnly => false;
        
        public int IndexOf(T item)
        {
            return _underlyingList.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            _underlyingList.Insert(index, item);
            OnItemAdded?.Invoke(item);
            OnListChanged?.Invoke();
        }

        public void RemoveAt(int index)
        {
            T item = _underlyingList[index];
            _underlyingList.RemoveAt(index);
            OnItemRemoved?.Invoke(item);
            OnListChanged?.Invoke();
        }

        public T this[int index]
        {
            get => _underlyingList[index];
            set
            {
                T oldValue = _underlyingList[index];
                _underlyingList[index] = value;
                OnItemChanged?.Invoke(oldValue, value);
                OnListChanged?.Invoke();
            }
        }
    }
}