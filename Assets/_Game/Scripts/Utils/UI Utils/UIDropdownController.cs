using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UIDropdownController : MonoBehaviour
    {
        [SerializeField] private TMP_Dropdown dropdown;

        private EventList<object> _options;
        private EventVariable<object> _currentValue;
        private readonly List<string> _optionLabels = new List<string>();
    
        private void OnValidate()
        {
            if (dropdown == null) dropdown = GetComponent<TMP_Dropdown>();
        }

        private void Awake()
        {
            dropdown.onValueChanged.AddListener(OnDropdownValueChanged);
        }

        public void SetReference(EventList<object> options, EventVariable<object> currentValue)
        {
            if (_options != null) _options.OnListChanged -= OnListChanged;
            if (_currentValue != null) _currentValue.OnValueChanged -= OnValueChanged;
        
            _options = options;
            _currentValue = currentValue;
            
            UpdateOptions();
            UpdateDropdownValue();

            _options.OnListChanged += OnListChanged;
            _currentValue.OnValueChanged += OnValueChanged;
        }

        private void OnListChanged()
        {
            UpdateOptions();
            UpdateDropdownValue();
        }

        private void OnValueChanged(object oldValue, object newValue)
        {
            UpdateDropdownValue();
        }

        private void UpdateOptions()
        {
            dropdown.ClearOptions();
            _optionLabels.Clear();

            foreach (object option in _options)
            {
                _optionLabels.Add(option.ToString());
            }
        
            dropdown.AddOptions(_optionLabels);
        }

        private void UpdateDropdownValue()
        {
            int selectedIndex = -1;

            for (int i = 0; i < _options.Count; i++)
            {
                if (_options[i].Equals(_currentValue.Value))
                {
                    selectedIndex = i;
                    break;
                }
            }

            if (dropdown.value != selectedIndex) dropdown.value = selectedIndex;
        }
    
        private void OnDropdownValueChanged(int newValue)
        {
            if (_currentValue.Value.Equals(_options[newValue])) return;

            _currentValue.Value = _options[newValue];
        }
    }
}
