﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UIButtonTintHandler : MonoBehaviour, IButtonStateHandler
    {
        [SerializeField] private Image targetGraphic;
        [SerializeField] private bool changeAlpha = true;
        [SerializeField][ColorPalette("DreadnoughtUI")] private Color normalColor = Color.white;
        [SerializeField][ColorPalette("DreadnoughtUI")] private Color hoverColor = Color.white;
        [SerializeField][ColorPalette("DreadnoughtUI")] private Color pressedColor = Color.white;
        [SerializeField][ColorPalette("DreadnoughtUI")] private Color disabledColor = Color.white;

        private void OnValidate()
        {
            if (targetGraphic == null) targetGraphic = GetComponent<Image>();
        }

        public void SetState(UIButtonState state)
        {
            if (targetGraphic == null) return;
            
            Color newTintColor;

            switch (state)
            {
                case UIButtonState.Normal:
                    newTintColor = normalColor;
                    break;
                case UIButtonState.Hover:
                    newTintColor = hoverColor;
                    break;
                case UIButtonState.Pressed:
                    newTintColor = pressedColor;
                    break;
                case UIButtonState.Disabled:
                    newTintColor = disabledColor;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, "Invalid buttons tate");
            }

            if (!changeAlpha) newTintColor.a = targetGraphic.color.a;

            targetGraphic.color = newTintColor;
        }
    }
}