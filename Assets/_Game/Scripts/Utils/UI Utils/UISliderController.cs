using System;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UISliderController : MonoBehaviour
    {
        [SerializeField] private Slider slider;

        private EventVariable<float> _reference;


        private void OnValidate()
        {
            if (slider == null) slider = GetComponent<Slider>();
        }

        private void Awake()
        {
            slider.onValueChanged.AddListener(OnSliderValueChanged);
        }

        public void SetReference(EventVariable<float> reference, float minValue, float maxValue)
        {
            if (_reference != null) _reference.OnValueChanged -= OnReferenceValueChanged;
            _reference = reference;

            slider.minValue = minValue;
            slider.maxValue = maxValue;
            
            UpdateSliderValue();
            
            _reference.OnValueChanged += OnReferenceValueChanged;
        }

        private void OnSliderValueChanged(float newValue)
        {
            _reference.Value = newValue;
        }

        private void OnReferenceValueChanged(float oldValue, float newValue)
        {
            if (Math.Abs(slider.value - newValue) > Mathf.Epsilon) UpdateSliderValue();
        }

        private void UpdateSliderValue()
        {
            slider.value = _reference.Value;
        }
    }
}
