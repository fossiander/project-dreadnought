using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UIButton : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        private IButtonStateHandler[] _buttonStateHandlers;
        private UIButtonState _buttonState = UIButtonState.Normal;
        
        [SerializeField] private bool isEnabled = true;
        private bool _isPressed = false;
        private bool _isPointerOverButton = false;

        [FoldoutGroup("Actions")] public UnityEvent<bool, bool> leftClickAction;
        [FoldoutGroup("Actions")] public UnityEvent<bool, bool> leftDoubleClickAction;
        [FoldoutGroup("Actions")] public UnityEvent<bool, bool> rightClickAction;
        [FoldoutGroup("Actions")] public UnityEvent enterAction;
        [FoldoutGroup("Actions")] public UnityEvent exitAction;

        private void Awake()
        {
            _buttonStateHandlers = GetComponentsInChildren<IButtonStateHandler>(true);
        }

        private void Start()
        {
            UpdateButtonState();
        }

        private void SetEnabled(bool enabled)
        {
            isEnabled = enabled;
            UpdateButtonState();
        }

        public void SetPressed(bool pressed)
        {
            _isPressed = pressed;
            UpdateButtonState();
        }
        
        public void OnPointerClick(PointerEventData eventData)
        {
            switch (eventData.button)
            {
                case PointerEventData.InputButton.Right:
                    rightClickAction?.Invoke(IsShiftDown(), IsCtrlDown());
                    return;
                case PointerEventData.InputButton.Left when eventData.clickCount > 1:
                    leftDoubleClickAction?.Invoke(IsShiftDown(), IsCtrlDown());
                    return;
                case PointerEventData.InputButton.Left:
                    leftClickAction?.Invoke(IsShiftDown(), IsCtrlDown());
                    break;
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _isPointerOverButton = true;
            UpdateButtonState();
            
            if (isEnabled) enterAction?.Invoke();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _isPointerOverButton = false;
            UpdateButtonState();
            
            if (isEnabled) exitAction?.Invoke();
        }

        private void UpdateButtonState()
        {
            if (!isEnabled)
            {
                SetButtonState(UIButtonState.Disabled);
                return;
            }

            if (_isPressed)
            {
                SetButtonState(UIButtonState.Pressed);
                return;
            }

            if (_isPointerOverButton)
            {
                SetButtonState(UIButtonState.Hover);
                return;
            }
            
            SetButtonState(UIButtonState.Normal);
        }

        private void SetButtonState(UIButtonState buttonState)
        {
            _buttonState = buttonState;
            UpdateButtonStateHandlers();
        }

        private void UpdateButtonStateHandlers()
        {
            foreach (IButtonStateHandler buttonStateHandler in _buttonStateHandlers)
            {
                buttonStateHandler.SetState(_buttonState);
            }
        }

        private bool IsShiftDown()
        {
            return Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift);
        }
        
        private bool IsCtrlDown()
        {
            return Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl) ||
                   Input.GetKeyDown(KeyCode.LeftCommand) || Input.GetKeyDown(KeyCode.RightCommand);
        }
    }
}
