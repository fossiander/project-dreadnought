﻿using System;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class NetworkEventStringVariable : IEventVariable<string> 
    {
        private readonly NetworkVariable<FixedString64Bytes> _networkVariableReference;

        public event ValueChangedEventAction<string> OnValueChanged;
        public string Value
        {
            get => _networkVariableReference.Value.ToString();
            set => _networkVariableReference.Value = _setterFunc(value);
        }

        private readonly Func<string,FixedString64Bytes> _setterFunc;

        public NetworkEventStringVariable(NetworkVariable<FixedString64Bytes> networkVariableReference, Func<string,FixedString64Bytes> setterFunc)
        {
            _networkVariableReference = networkVariableReference;
            _setterFunc = setterFunc;

            _networkVariableReference.OnValueChanged += OnNetworkVariableChanged;
        }

        ~NetworkEventStringVariable()
        {
            _networkVariableReference.OnValueChanged -= OnNetworkVariableChanged;
        }

        private void OnNetworkVariableChanged(FixedString64Bytes previousValue, FixedString64Bytes newValue)
        {
            OnValueChanged?.Invoke(previousValue.ToString(), newValue.ToString());
        }
    }
}