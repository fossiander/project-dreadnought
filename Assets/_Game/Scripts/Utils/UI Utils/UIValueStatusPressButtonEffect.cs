using System;
using UnityEngine;

namespace _Game.Scripts.Utils.UI_Utils
{
    public abstract class UIValueStatusPressButtonEffect<T> : UIValueStatusEffectTrigger<T> where T : unmanaged
    {
        [SerializeField] private UIButton targetButton = null;
        protected override void TriggerEffect(bool effectActive)
        {
            targetButton.SetPressed(effectActive);
        }
    }
}
