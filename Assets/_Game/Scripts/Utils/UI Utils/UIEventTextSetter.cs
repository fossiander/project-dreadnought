using UnityEngine;
using TMPro;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UIEventTextSetter : MonoBehaviour
    {
        private readonly LazyComponentGetter<TextMeshProUGUI> _textMeshGetter;

        private IEventVariable<string> _stringEventVariable;
        
        public UIEventTextSetter()
        {
            _textMeshGetter = new LazyComponentGetter<TextMeshProUGUI>(this);
        }

        public void SetReference(IEventVariable<string> referenceVariable)
        {
            if (_stringEventVariable != null) _stringEventVariable.OnValueChanged -= OnValueChanged;
            _stringEventVariable = referenceVariable;
            _stringEventVariable.OnValueChanged += OnValueChanged;
            UpdateTextMesh();
        }

        private void OnValueChanged(string oldValue, string newValue)
        {
            UpdateTextMesh();
        }

        private void UpdateTextMesh()
        {
            _textMeshGetter.Get().text = _stringEventVariable?.Value;
        }
    }
}
