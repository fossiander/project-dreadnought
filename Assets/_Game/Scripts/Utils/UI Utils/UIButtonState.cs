﻿namespace _Game.Scripts.Utils.UI_Utils
{
    public enum UIButtonState
    {
        Normal,
        Hover,
        Pressed,
        Disabled
    }
}