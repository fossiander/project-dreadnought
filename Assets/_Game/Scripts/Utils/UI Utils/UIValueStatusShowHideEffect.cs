using System;
using UnityEngine;

namespace _Game.Scripts.Utils.UI_Utils
{
    public abstract class UIValueStatusShowHideEffect<T> : UIValueStatusEffectTrigger<T> where T : unmanaged
    {
        protected override void TriggerEffect(bool effectActive)
        {
            gameObject.SetActive(effectActive);
        }
    }
}
