using UnityEngine;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UICheckBox : MonoBehaviour
    {
        [SerializeField] private UIButton uiButtonChecked = null;
        [SerializeField] private UIButton uiButtonUnchecked = null;
        [SerializeField] private UIBoolStatusShowHide checkedVisual = null;
        [SerializeField] private UIBoolStatusShowHide uncheckedVisual = null;
        [SerializeField] private UIBoolStatusShowHide backGroundHighlight = null;

        private IEventVariable<bool> _referenceVariable;

        public void SetReference(IEventVariable<bool> referenceVariable)
        {
            if (_referenceVariable != null)
            {
                uiButtonChecked.leftClickAction.RemoveListener(OnLeftClickChecked);
                uiButtonUnchecked.leftClickAction.RemoveListener(OnLeftClickUnchecked);
            }

            _referenceVariable = referenceVariable;
            
            checkedVisual.SetReference(referenceVariable,true);
            uncheckedVisual.SetReference(referenceVariable,false);

            if (backGroundHighlight != null) backGroundHighlight.SetReference(referenceVariable, true);
            
            uiButtonChecked.leftClickAction.AddListener(OnLeftClickChecked);
            uiButtonUnchecked.leftClickAction.AddListener(OnLeftClickUnchecked);
        }

        private void OnLeftClickChecked(bool shift, bool ctrl)
        {
            _referenceVariable.Value = false;
        }
        
        private void OnLeftClickUnchecked(bool shift, bool ctrl)
        {
            _referenceVariable.Value = true;
        }
    }
}
