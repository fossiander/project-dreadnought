using System;
using System.Collections;
using _Game.Scripts.Utils.Audio_Utils;
using UnityEngine;
using AudioType = _Game.Scripts.Utils.Audio_Utils.AudioType;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UISettingsMenu : MonoBehaviour
    {
        [SerializeField] private Transform mainMenu;
        [SerializeField] private UIDropdownController resolutionDropDown;
        [SerializeField] private UIToggleController fullscreenToggle;
        [SerializeField] private UISliderController masterVolumeSlider;
        [SerializeField] private UISliderController musicVolumeSlider;
        [SerializeField] private UISliderController effectsVolumeSlider;
        [SerializeField] private UISliderController voiceVolumeSlider;
        [SerializeField] private UISliderController uiVolumeSlider;

        private readonly EventList<object> _resolutions = new EventList<object>();
        private readonly EventVariable<object> _currentResolution = new EventVariable<object>();
        private readonly EventVariable<bool> _isFullscreen = new EventVariable<bool>();

        private readonly EventVariable<float> _masterVolume = new EventVariable<float>();
        private readonly EventVariable<float> _musicVolume = new EventVariable<float>();
        private readonly EventVariable<float> _effectsVolume = new EventVariable<float>();
        private readonly EventVariable<float> _voiceVolume = new EventVariable<float>();
        private readonly EventVariable<float> _uiVolume = new EventVariable<float>();

        private readonly LazyObjectFinder<AudioManager> _audioManager = new LazyObjectFinder<AudioManager>();

        private readonly LazyObjectFinder<UIDialogBoxManager> _dialogBoxManager = new LazyObjectFinder<UIDialogBoxManager>(); 

        private Resolution _previousResolution;
        private bool _previousFullscreen;
        private UIDialogBox _activeDialogBox;

        private const int REVERT_SETTINGS_DELAY_IN_SECONDS = 15;

        private void OnEnable()
        {
            _resolutions.Clear();
            foreach (Resolution resolution in Screen.resolutions)
            {
                _resolutions.Add(resolution);
            }
            _currentResolution.Value = Screen.currentResolution;
            resolutionDropDown.SetReference(_resolutions, _currentResolution);

            _isFullscreen.Value = Screen.fullScreen;
            fullscreenToggle.SetReference(_isFullscreen);

            _audioManager.Get().LoadAudioMixerVolumeFromPlayerPrefs();

            _masterVolume.Value = _audioManager.Get().GetVolumePercentage(AudioType.Master);
            masterVolumeSlider.SetReference(_masterVolume, 0, 1);
            _masterVolume.OnValueChanged += OnMasterVolumeChanged;
            
            _musicVolume.Value = _audioManager.Get().GetVolumePercentage(AudioType.Music);
            musicVolumeSlider.SetReference(_musicVolume, 0, 1);
            _musicVolume.OnValueChanged += OnMusicVolumeChanged;

            _effectsVolume.Value = _audioManager.Get().GetVolumePercentage(AudioType.Effect);
            effectsVolumeSlider.SetReference(_effectsVolume, 0, 1);
            _effectsVolume.OnValueChanged += OnEffectVolumeChanged;

            _voiceVolume.Value = _audioManager.Get().GetVolumePercentage(AudioType.Voice);
            voiceVolumeSlider.SetReference(_voiceVolume, 0, 1);
            _voiceVolume.OnValueChanged += OnVoiceVolumeChanged;

            _uiVolume.Value = _audioManager.Get().GetVolumePercentage(AudioType.UI);
            uiVolumeSlider.SetReference(_uiVolume, 0, 1);
            _uiVolume.OnValueChanged += OnUIVolumeChanged;
        }

        private void OnDisable()
        {
            _masterVolume.OnValueChanged -= OnMasterVolumeChanged;
            _musicVolume.OnValueChanged -= OnMusicVolumeChanged;
            _effectsVolume.OnValueChanged -= OnEffectVolumeChanged;
            _voiceVolume.OnValueChanged -= OnVoiceVolumeChanged;
            _uiVolume.OnValueChanged -= OnUIVolumeChanged;
        }

        public void CancelSettingsChange()
        {
            _audioManager.Get().LoadAudioMixerVolumeFromPlayerPrefs();
            gameObject.SetActive(false);
            mainMenu.gameObject.SetActive(true);
        }

        public void ConfirmSettingsChange()
        {
            _previousResolution = Screen.currentResolution;
            _previousFullscreen = Screen.fullScreen;
            
            Resolution selectedResolution = (Resolution)_currentResolution.Value;
            Screen.SetResolution(selectedResolution.width, selectedResolution.height, _isFullscreen.Value, selectedResolution.refreshRate);

            _activeDialogBox = _dialogBoxManager.Get().OpenDialogBox($"Do you want to keep these settings?\n(Settings will be automatically reverted in {REVERT_SETTINGS_DELAY_IN_SECONDS} seconds)", 
                new UIDialogBox.DialogBoxButton("No", RevertSettings),
                new UIDialogBox.DialogBoxButton("Yes", SaveSettings));

            StartCoroutine(CRevertSettingsAfterDelay());
        }

        private IEnumerator CRevertSettingsAfterDelay()
        {
            var waitFor1Second = new WaitForSecondsRealtime(1f);
            int secondsTillClosing = REVERT_SETTINGS_DELAY_IN_SECONDS;
            for (int i = 0; i < REVERT_SETTINGS_DELAY_IN_SECONDS; i++)
            {
                yield return waitFor1Second;
                secondsTillClosing--;
                _activeDialogBox.SetBody($"Do you want to keep these settings?\n(Settings will be automatically reverted in {secondsTillClosing} seconds)");
            }
            RevertSettings();
            _activeDialogBox.Hide();
        }

        private void RevertSettings()
        {
            StopAllCoroutines();
            Screen.SetResolution(_previousResolution.width, _previousResolution.height, _previousFullscreen, _previousResolution.refreshRate);
        }

        private void SaveSettings()
        {
            StopAllCoroutines();
            _audioManager.Get().SaveAudioMixerVolumeToPlayerPrefs();
            gameObject.SetActive(false);
            mainMenu.gameObject.SetActive(true);
        }

        private void OnMasterVolumeChanged(float oldValue, float newValue)
        {
            _audioManager.Get().SetAudioMixerVolume(AudioType.Master, newValue);
            _audioManager.Get().PlayExampleSound(AudioType.Master);
        }
        
        private void OnMusicVolumeChanged(float oldValue, float newValue)
        {
            _audioManager.Get().SetAudioMixerVolume(AudioType.Music, newValue);
            _audioManager.Get().PlayExampleSound(AudioType.Music);
        }
        
        private void OnEffectVolumeChanged(float oldValue, float newValue)
        {
            _audioManager.Get().SetAudioMixerVolume(AudioType.Effect, newValue);
            _audioManager.Get().PlayExampleSound(AudioType.Effect);
        }
        
        private void OnVoiceVolumeChanged(float oldValue, float newValue)
        {
            _audioManager.Get().SetAudioMixerVolume(AudioType.Voice, newValue);
            _audioManager.Get().PlayExampleSound(AudioType.Voice);
        }
        
        private void OnUIVolumeChanged(float oldValue, float newValue)
        {
            _audioManager.Get().SetAudioMixerVolume(AudioType.UI, newValue);
            _audioManager.Get().PlayExampleSound(AudioType.UI);
        }
    }
}