﻿using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UIToggleController : MonoBehaviour
    {
        [SerializeField] private Toggle toggle;

        private EventVariable<bool> _reference;
    
        private void OnValidate()
        {
            if (toggle == null) toggle = GetComponent<Toggle>();
        }

        private void Awake()
        {
            toggle.onValueChanged.AddListener(OnToggleValueChanged);
        }

        public void SetReference(EventVariable<bool> reference)
        {
            if (_reference != null) _reference.OnValueChanged -= OnReferenceValueChanged;
            _reference = reference;
        
            _reference.OnValueChanged += OnReferenceValueChanged;
        }

        private void OnToggleValueChanged(bool newValue)
        {
            _reference.Value = newValue;
        }

        private void OnReferenceValueChanged(bool oldValue, bool newValue)
        {
            if (toggle.isOn != newValue)
                UpdateToggleToReference();
        }

        private void UpdateToggleToReference()
        {
            toggle.isOn = _reference.Value;
        }
    }
}