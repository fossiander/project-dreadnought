﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UIWindowManager : MonoBehaviour
    {
        private readonly Dictionary<Type,UIWindow> _uiWindows = new Dictionary<Type, UIWindow>();

        private void Awake()
        {
            foreach (UIWindow uiWindow in GetComponentsInChildren<UIWindow>(true))
            {
                if (_uiWindows.ContainsKey(uiWindow.GetType())) Debug.LogError($"Duplicative window of type {uiWindow.GetType()}");

                _uiWindows[uiWindow.GetType()] = uiWindow;
            }
        }

        public T GetWindow<T>() where T : UIWindow
        {
            _uiWindows.TryGetValue(typeof(T), out UIWindow uiWindow);

            return uiWindow as T;
        }

        public T OpenWindow<T>() where T : UIWindow
        {
            if (_uiWindows.TryGetValue(typeof(T), out UIWindow uiWindow))
            {
                uiWindow.Open();
                return uiWindow as T;
            }
            else
            {
                Debug.LogError($"No window of type {typeof(T)} found!");
                return null;
            }
        }
    }
}