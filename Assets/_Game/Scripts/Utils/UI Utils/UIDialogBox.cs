﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Pool;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UIDialogBox : MonoBehaviour
    {
        [SerializeField] private Transform titleContainer;
        [SerializeField] private TextMeshProUGUI titleTextMesh;
        [SerializeField] private TextMeshProUGUI bodyTextMesh;
        [SerializeField] private UIButton xButton;
        [SerializeField] private UIButton buttonPrefab;
        [SerializeField] private Transform buttonContainer;

        private SimpleComponentPool<UIButton> _buttonPool;
        
        private void Awake()
        {
            _buttonPool = new SimpleComponentPool<UIButton>(buttonPrefab, buttonContainer, 3, 10);
            foreach (UIButton uiButton in buttonContainer.GetComponentsInChildren<UIButton>())
            {
                Destroy(uiButton.gameObject);
                uiButton.transform.SetParent( null, false);
            }
        }

        public void ShowWithTitle(string title, string body, Action xButtonAction, DialogBoxButton[] dialogBoxButtonParams)
        {
            titleContainer.gameObject.SetActive(title != "");
            
            xButton.leftClickAction.RemoveAllListeners();
            if (xButtonAction != null) xButton.leftClickAction.AddListener((_,_) => xButtonAction());
            xButton.gameObject.SetActive(xButtonAction != null);
            
            titleTextMesh.text = title;
            bodyTextMesh.text = body;

            HideButtons();
            foreach (DialogBoxButton buttonParam in dialogBoxButtonParams)
            {
                AddButton(buttonParam.description, buttonParam.buttonClickAction);
            }

            transform.SetAsLastSibling();
            gameObject.SetActive(true);
        }

        public void Show(string body, DialogBoxButton[] dialogBoxButtonParams = null)
        {
            ShowWithTitle("", body, null, dialogBoxButtonParams);
        }

        private void HideButtons()
        {
            _buttonPool.ReleaseAll();
        }

        public void SetBody(string body)
        {
            bodyTextMesh.text = body;
        }

        private void AddButton(string description, Action buttonAction)
        {
            UIButton button = _buttonPool.Get();
            button.transform.SetAsLastSibling();
            
            button.GetComponentInChildren<TextMeshProUGUI>().text = description;
            button.leftClickAction.RemoveAllListeners();
            button.leftClickAction.AddListener((_,_) => buttonAction?.Invoke());
            button.leftClickAction.AddListener((_,_) => Hide());
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
        
        public struct DialogBoxButton
        {
            public string description;
            public Action buttonClickAction;

            public DialogBoxButton(string description, Action buttonClickAction)
            {
                this.description = description;
                this.buttonClickAction = buttonClickAction;
            }
        }
    }
}