﻿using System;

namespace _Game.Scripts.Utils.UI_Utils
{
    public interface IEventVariable<T>
    {
        event ValueChangedEventAction<T> OnValueChanged;
        T Value { get; set; }
    }
    
    public delegate void ValueChangedEventAction<T>(T oldValue, T newValue);
}