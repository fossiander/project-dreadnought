﻿using UnityEngine;

namespace _Game.Scripts.Utils.UI_Utils
{
    public abstract class UIListItem<T> : MonoBehaviour where T : class
    {
        public abstract void SetReference(T reference);
    }
}