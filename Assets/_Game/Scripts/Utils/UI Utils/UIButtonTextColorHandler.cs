﻿using System;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UIButtonTextColorHandler : MonoBehaviour, IButtonStateHandler
    {
        [SerializeField] private TextMeshProUGUI targetTextMesh;
        [SerializeField] private bool changeAlpha = true;
        [SerializeField][ColorPalette("DreadnoughtUI")] private Color normalColor = Color.white;
        [SerializeField][ColorPalette("DreadnoughtUI")] private Color hoverColor = Color.white;
        [SerializeField][ColorPalette("DreadnoughtUI")] private Color pressedColor = Color.white;
        [SerializeField][ColorPalette("DreadnoughtUI")] private Color disabledColor = Color.white;

        public void SetState(UIButtonState state)
        {
            if (targetTextMesh == null) return;
            
            Color newTintColor;

            switch (state)
            {
                case UIButtonState.Normal:
                    newTintColor = normalColor;
                    break;
                case UIButtonState.Hover:
                    newTintColor = hoverColor;
                    break;
                case UIButtonState.Pressed:
                    newTintColor = pressedColor;
                    break;
                case UIButtonState.Disabled:
                    newTintColor = disabledColor;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, "Invalid buttons tate");
            }

            if (!changeAlpha) newTintColor.a = targetTextMesh.color.a;

            targetTextMesh.color = newTintColor;
        }
    }
}