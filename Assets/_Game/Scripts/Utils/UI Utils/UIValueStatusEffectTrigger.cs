using System;
using UnityEngine;

namespace _Game.Scripts.Utils.UI_Utils
{
    public abstract class UIValueStatusEffectTrigger<T> : MonoBehaviour where T : unmanaged
    {
        private IEventVariable<T> _referenceVariable;
        private T _condition;

        public void SetReference(IEventVariable<T> referenceVariable, T condition)
        {
            if (_referenceVariable != null)
            {
                _referenceVariable.OnValueChanged -= OnValueChanged;
            }
            _referenceVariable = referenceVariable;
            _referenceVariable.OnValueChanged += OnValueChanged;
            _condition = condition;
            
            UpdateEffect();
        }

        private void OnValueChanged(T oldValue, T newValue)
        {
            UpdateEffect();
        }

        private void UpdateEffect()
        {
            TriggerEffect(_referenceVariable.Value.Equals(_condition));
        }

        protected abstract void TriggerEffect(bool effectActive);
    }
}
