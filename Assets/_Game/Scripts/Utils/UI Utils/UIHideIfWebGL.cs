﻿using UnityEngine;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UIHideIfWebGL : MonoBehaviour
    {
        private void Start()
        {
            if (Application.platform == RuntimePlatform.WebGLPlayer) gameObject.SetActive(false);
        }
    }
}