﻿using System;
using _Game.Scripts.Utils.Audio_Utils;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UIButtonSoundEffects : MonoBehaviour
    {
        [SerializeField] private UIButton referenceButton = null;
        
        [FoldoutGroup("Sounds")] [SerializeField] private AudioSound soundOnLeftClick = null;
        [FoldoutGroup("Sounds")]  [Range(-0.5f, 0.5f)] [SerializeField] private float leftClickPitchAdjustment = 0;
        [FoldoutGroup("Sounds")] [SerializeField] private AudioSound soundOnDoubleLeftClick = null;
        [FoldoutGroup("Sounds")]  [Range(-0.5f, 0.5f)] [SerializeField] private float leftDoubleClickPitchAdjustment = 0;

        [FoldoutGroup("Sounds")] [SerializeField] private AudioSound soundOnRightClick = null;
        [FoldoutGroup("Sounds")]  [Range(-0.5f, 0.5f)] [SerializeField] private float rightClickPitchAdjustment = 0;

        [FoldoutGroup("Sounds")] [SerializeField] private AudioSound soundOnEnter = null;
        [FoldoutGroup("Sounds")]  [Range(-0.5f, 0.5f)] [SerializeField] private float enterPitchAdjustment = 0;

        [FoldoutGroup("Sounds")] [SerializeField] private AudioSound soundOnExit = null;
        [FoldoutGroup("Sounds")]  [Range(-0.5f, 0.5f)] [SerializeField] private float exitPitchAdjustment = 0;


        private readonly LazyObjectFinder<AudioManager> _audioManager = new LazyObjectFinder<AudioManager>();

        private void OnEnable()
        {
            if (soundOnLeftClick != null) referenceButton.leftClickAction.AddListener(PlayLeftClickSound);
            if (soundOnDoubleLeftClick != null) referenceButton.leftDoubleClickAction.AddListener(PlayLeftDoubleClickSound);
            if (soundOnRightClick != null) referenceButton.rightClickAction.AddListener(PlayRightClickSound);
            if (soundOnEnter != null) referenceButton.enterAction.AddListener(PlayEnterSound);
            if (soundOnExit != null)  referenceButton.exitAction.AddListener(PlayExitSound);
        }
        
        private void OnDisable()
        {
            if (soundOnLeftClick != null) referenceButton.leftClickAction.RemoveListener(PlayLeftClickSound);
            if (soundOnDoubleLeftClick != null) referenceButton.leftDoubleClickAction.RemoveListener(PlayLeftDoubleClickSound);
            if (soundOnRightClick != null) referenceButton.rightClickAction.RemoveListener(PlayRightClickSound);
            if (soundOnEnter != null) referenceButton.enterAction.RemoveListener(PlayEnterSound);
            if (soundOnExit != null)  referenceButton.exitAction.RemoveListener(PlayExitSound);
        }

        private void OnValidate()
        {
            if (referenceButton == null) referenceButton = GetComponent<UIButton>();
        }

        private void PlayLeftClickSound(bool shift, bool ctrl)
        {
            PlaySound(soundOnLeftClick, leftClickPitchAdjustment);
        }
        
        private void PlayLeftDoubleClickSound(bool shift, bool ctrl)
        {
            PlaySound(soundOnDoubleLeftClick, leftDoubleClickPitchAdjustment);
        }
        
        private void PlayRightClickSound(bool shift, bool ctrl)
        {
            PlaySound(soundOnRightClick, rightClickPitchAdjustment);
        }
        
        private void PlayEnterSound()
        {
            PlaySound(soundOnEnter, enterPitchAdjustment);
        }
        
        private void PlayExitSound()
        {
            PlaySound(soundOnExit, exitPitchAdjustment);
        }

        private void PlaySound(AudioSound audioSound, float pitchAdjustment)
        {
            _audioManager.Get().PlaySound2D(audioSound, pitchAdjustment);
        }
    }
}