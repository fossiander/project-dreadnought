﻿using UnityEngine;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UIButtonHoverShowHide : MonoBehaviour, IButtonStateHandler
    {
        public void SetState(UIButtonState state)
        {
            gameObject.SetActive(state == UIButtonState.Hover);
        }
    }
}