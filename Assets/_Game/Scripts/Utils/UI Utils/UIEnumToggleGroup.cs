﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace _Game.Scripts.Utils.UI_Utils
{
    public abstract class UIEnumToggleGroup<T> : MonoBehaviour where T : unmanaged, Enum
    {
        [SerializeField] private UIButton[] toggleButtons = null;
        
        private UnityAction<bool,bool>[] _leftButtonClickActions;
        private UnityAction<bool,bool>[] _rightButtonClickActions;

        public void SetReference(IEventVariable<T> reference)
        {
            if (toggleButtons.Length != Enum.GetValues(typeof(T)).Length)
                Debug.LogError("Number of buttons inconsistent with number of enum options");
            
            _leftButtonClickActions ??= new UnityAction<bool,bool>[toggleButtons.Length];
            _rightButtonClickActions ??= new UnityAction<bool,bool>[toggleButtons.Length];

            for (int i = 0; i < toggleButtons.Length; i++)
            {
                if (_leftButtonClickActions[i] != null)
                {
                    toggleButtons[i].leftClickAction.RemoveListener(_leftButtonClickActions[i]);
                    _leftButtonClickActions[i] = null;
                }
                
                if (_rightButtonClickActions[i] != null)
                {
                    toggleButtons[i].rightClickAction.RemoveListener(_rightButtonClickActions[i]);
                    _rightButtonClickActions[i] = null;
                }
            }
            
            int j = 0;
            foreach (T tValue in Enum.GetValues(typeof(T)))
            {
                _leftButtonClickActions[j] = (shift, ctrl) => LeftButtonAction(tValue, shift, ctrl);
                toggleButtons[j].leftClickAction.AddListener(_leftButtonClickActions[j]);
                
                _rightButtonClickActions[j] = (shift, ctrl) => RightButtonAction(tValue, shift, ctrl);
                toggleButtons[j].rightClickAction.AddListener(_rightButtonClickActions[j]);


                if (toggleButtons[j].TryGetComponent(out UIValueStatusPressButtonEffect<T> pressButtonEffect))
                {
                    pressButtonEffect.SetReference(reference, tValue);
                }
                
                if (toggleButtons[j].TryGetComponent(out UIValueStatusShowHideEffect<T> showHideEffect))
                {
                    showHideEffect.SetReference(reference, tValue);
                }

                j++;
            }
        }

        protected abstract void LeftButtonAction(T value, bool shift, bool ctrl);
        protected abstract void RightButtonAction(T value, bool shift, bool ctrl);
    }
}