﻿using System;
using Unity.Netcode;
using UnityEngine;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class NetworkEventVariable<T> : IEventVariable<T> where T : unmanaged
    {
        private readonly NetworkVariable<T> _networkVariableReference;

        public event ValueChangedEventAction<T> OnValueChanged;
        public T Value
        {
            get => _networkVariableReference.Value;
            set => _networkVariableReference.Value = value;
        }

        public NetworkEventVariable(NetworkVariable<T> networkVariableReference)
        {
            _networkVariableReference = networkVariableReference;

            _networkVariableReference.OnValueChanged += OnNetworkVariableChanged;
        }

        ~NetworkEventVariable()
        {
            _networkVariableReference.OnValueChanged -= OnNetworkVariableChanged;

        }

        private void OnNetworkVariableChanged(T previousValue, T newValue)
        {
            OnValueChanged?.Invoke(previousValue, newValue);
        }
    }
}