﻿namespace _Game.Scripts.Utils.UI_Utils
{
    public interface IButtonStateHandler
    {
        void SetState(UIButtonState state);
    }
}