﻿using System;
using UnityEngine;

namespace _Game.Scripts.Utils.UI_Utils
{
    [System.Serializable]
    public class EventVariable<T> : IEventVariable<T>
    {
        public event ValueChangedEventAction<T> OnValueChanged;

        public EventVariable()
        {
        }

        public EventVariable(T startingValue)
        {
            Value = startingValue;
        }

        public T Value
        {
            get => value;
            set
            {
                T oldValue = this.value;
                this.value = value;
                if (!value.Equals(oldValue)) OnValueChanged?.Invoke(oldValue,value);
            }
        }

        [SerializeField] private T value;
    }
}