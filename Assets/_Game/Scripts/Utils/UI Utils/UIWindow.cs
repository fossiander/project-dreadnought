﻿using UnityEngine;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UIWindow : MonoBehaviour
    {
        public virtual void Close()
        {
            gameObject.SetActive(false);
        }
        
        public virtual void Open()
        {
            gameObject.SetActive(true);
        }
    }
}