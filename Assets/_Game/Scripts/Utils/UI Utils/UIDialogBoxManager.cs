﻿using System;
using UnityEngine;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UIDialogBoxManager : MonoBehaviour
    {
        [SerializeField] private UIDialogBox dialogBoxPrefab = null;

        [Header("Pooling")]
        [SerializeField] private int defaultPoolSize = 5;
        [SerializeField] private int maxPoolSize = 20;

        private SimpleComponentPool<UIDialogBox> _dialogBoxPool;

        private void Awake()
        {
            _dialogBoxPool = new SimpleComponentPool<UIDialogBox>(dialogBoxPrefab, transform, defaultPoolSize, maxPoolSize);
        }
        
        public UIDialogBox OpenDialogBoxWithTitle(string title, string body, Action xButtonAction, params UIDialogBox.DialogBoxButton[] buttons)
        {
            UIDialogBox dialogBox = _dialogBoxPool.Get();
            dialogBox.ShowWithTitle(title, body, xButtonAction, buttons);
            return dialogBox;
        }

        public UIDialogBox OpenDialogBox(string body, params UIDialogBox.DialogBoxButton[] dialogBoxParams)
        {
            UIDialogBox dialogBox = _dialogBoxPool.Get();
            dialogBox.Show(body, dialogBoxParams);
            return dialogBox;
        }
    }
}