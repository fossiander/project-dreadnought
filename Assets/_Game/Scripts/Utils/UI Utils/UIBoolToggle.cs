using System;
using UnityEngine;
using UnityEngine.Events;

namespace _Game.Scripts.Utils.UI_Utils
{
    public class UIBoolToggle : MonoBehaviour
    {
        [SerializeField] private UIButton toggleOn = null;
        [SerializeField] private UIButton toggleOff = null;

        private UnityAction<bool, bool> _leftButtonClickActionOn;
        private UnityAction<bool, bool> _leftButtonClickActionOff;

        public void SetReference(IEventVariable<bool> reference, Action<bool, bool, bool> onToggleAction)
        {
            if (_leftButtonClickActionOn != null)
            {
                toggleOn.leftClickAction.RemoveListener(_leftButtonClickActionOn);
            }

            if (_leftButtonClickActionOff != null)
            {
                toggleOff.leftClickAction.RemoveListener(_leftButtonClickActionOff);
            }

            _leftButtonClickActionOn = (shift, ctrl) => onToggleAction(true, shift, ctrl);
            toggleOn.leftClickAction.AddListener(_leftButtonClickActionOn);

            _leftButtonClickActionOff = (shift, ctrl) => onToggleAction(false, shift, ctrl);
            toggleOff.leftClickAction.AddListener(_leftButtonClickActionOff);

            if (toggleOn.TryGetComponent(out UIBoolStatusShowHide showHideEffectOn))
            {
                showHideEffectOn.SetReference(reference, true);
            }

            if (toggleOff.TryGetComponent(out UIBoolStatusShowHide showHideEffectOff))
            {
                showHideEffectOff.SetReference(reference, false);
            }
        }
    }
}
