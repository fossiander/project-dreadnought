﻿using System;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Pool;

namespace _Game.Scripts.Utils.UI_Utils
{
    public abstract class UIList<T> : MonoBehaviour where T : class
    {
        [SerializeField] private UIListItem<T> listItemPrefab = null;

        [Header("Pooling")]
        [SerializeField] private int defaultPoolSize = 10;
        [SerializeField] private int maxPoolSize = 100;

        private readonly ObjectPool<UIListItem<T>> _listItemPool;
        private EventList<T> _eventListReference;

        private bool _hasBeenCleared = false;

        public List<UIListItem<T>> ActiveListItems { get; } = new List<UIListItem<T>>();

        protected UIList()
        {
            _listItemPool = new ObjectPool<UIListItem<T>>(
                CreatePoolItem, 
                OnTakeFromPool, 
                OnReturnToPool, 
                OnDestroyPoolObject,
                true,
                defaultPoolSize,
                maxPoolSize);
        }

        private void Awake()
        {
            ClearBeforeFirstUse();
        }

        private void ClearBeforeFirstUse()
        {
            if (_hasBeenCleared) return;
            
            foreach (var uiListItem in GetComponentsInChildren<UIListItem<T>>())
            {
                Destroy(uiListItem.gameObject);
                uiListItem.transform.SetParent(null, false);
            }

            _hasBeenCleared = true;
        }

        protected void SetReference(EventList<T> reference)
        {
            if (_eventListReference != null) _eventListReference.OnListChanged -= RefreshList;

            _eventListReference = reference;
            _eventListReference.OnListChanged += RefreshList;
            
            RefreshList();
        }

        private void RefreshList()
        {
            ClearBeforeFirstUse();

            foreach (var uiListItem in GetComponentsInChildren<UIListItem<T>>())
            {
                _listItemPool.Release(uiListItem);
                ActiveListItems.Remove(uiListItem);
            }

            foreach (T item in _eventListReference)
            {
                var newListItem = _listItemPool.Get();
                newListItem.SetReference(item);
                ActiveListItems.Add(newListItem);
            }
        }

        private UIListItem<T> CreatePoolItem()
        {
            return Instantiate(listItemPrefab, transform);
        }

        private void OnTakeFromPool(UIListItem<T> obj)
        {
            obj.gameObject.SetActive(true);
        }

        private void OnReturnToPool(UIListItem<T> obj)
        {
            obj.gameObject.SetActive(false);
        }

        private void OnDestroyPoolObject(UIListItem<T> obj)
        {
            Destroy(obj.gameObject);
        }
    }
}