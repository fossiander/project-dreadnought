﻿using System;

namespace _Game.Scripts.Utils
{
    public static class EnumUtils
    {
        public static T NextItem<T>(this T enumItem) where T : Enum
        {
            int index = GetEnumIndex(enumItem);
            int nextIndex = (index + 1) % GetOptionCount<T>();
            return GetEnumArray<T>()[nextIndex];
        }
        
        public static T PreviousItem<T>(this T enumItem) where T : Enum
        {
            int index = GetEnumIndex(enumItem);
            int nextIndex = (index - 1) % GetOptionCount<T>();
            return GetEnumArray<T>()[nextIndex];
        }
        
        public static int GetEnumIndex<T>(this T enumItem) where T : Enum
        {
            int i = 0;
            foreach (T tValue in Enum.GetValues(typeof(T)))
            {
                if (tValue.Equals(enumItem)) return i;
                i++;
            }

            return -1;
        }
        
        private static T[] GetEnumArray<T>() where T : Enum
        {
            T[] returnArray = new T[GetOptionCount<T>()];

            int i = 0;
            foreach (T tValue in Enum.GetValues(typeof(T)))
            {
                returnArray[i] = tValue;
                i++;
            }

            return returnArray;
        }
        
        private static int GetOptionCount<T>() where T : Enum
        {
            return Enum.GetValues(typeof(T)).Length;
        }
    }
}