﻿using System.Collections.Generic;
using Shapes;
using UnityEngine;

namespace _Game.Scripts.Utils
{
    public class LineChart : MonoBehaviour
    {
        [SerializeField] private Transform pointsParent = null;
        [SerializeField] private Color[] pointColors;
        [SerializeField] private Transform pathParent = null;
        [SerializeField] private Color[] lineColors;
        [SerializeField] private float scaleFactor = 100;
        [SerializeField] private int bubbleFrequency = 1;

        private readonly List<Vector2[]> _paths = new List<Vector2[]>();
        private readonly List<Vector2> _points = new List<Vector2>();
        private readonly MinMaxValues _minMaxValues = new MinMaxValues();
    
        public void Clear()
        {
            DeletePathsAndPoints();
            _points.Clear();
            _paths.Clear();
            
            _minMaxValues.Reset();
        }
        
        private void DeletePathsAndPoints()
        {
            for (int i = pathParent.childCount - 1; i >= 0; i--)
            {
                DestroyImmediate(pathParent.GetChild(i).gameObject);
            }
            for (int i = pointsParent.childCount - 1; i >= 0; i--)
            {
                DestroyImmediate(pointsParent.GetChild(i).gameObject);
            }
        }
        
        public void AddPoint(Vector2 point)
        {
            _points.Add(point);
            _minMaxValues.Update(point);
        
            DeletePathsAndPoints();
            CreatePathsAndPoints();
        }

        public void AddPath(Vector2[] path)
        {
            _paths.Add(path);
            _minMaxValues.Update(path);
        
            DeletePathsAndPoints();
            CreatePathsAndPoints();
        }

        private void CreatePathsAndPoints()
        {
            for (int i = 0; i < _paths.Count; i++)
            {
                DrawChartPath(_paths[i], lineColors[i]);
            }
            
            for (int i = 0; i < _points.Count; i++)
            {
                DrawChartPoint(_points[i], pointColors[i]);
            }
        }

        private void DrawChartPoint(Vector2 point, Color color)
        {
            Vector2 pointScaled = _minMaxValues.ScaleVector(point, scaleFactor);
            
            GameObject dotGO = new GameObject("Point " + pointsParent.childCount);
            dotGO.transform.SetParent(pointsParent.transform);
            dotGO.transform.position = pointScaled;
            Disc disc = dotGO.AddComponent<Disc>();
            disc.Color = color;
            disc.Radius = 1f;
        }

        private void DrawChartPath(Vector2[] path, Color color)
        {
            GameObject pathGO = new GameObject("Path " + pathParent.childCount);
            pathGO.transform.SetParent(pathParent);
            Polyline polyline = pathGO.AddComponent<Polyline>();
            polyline.Thickness = 0.25f;
            polyline.Joins = PolylineJoins.Round;
            polyline.Color = color;
            polyline.Closed = false;
            polyline.points.Clear();

            for (int i = 0; i < path.Length; i++)
            {
                Vector2 point = _minMaxValues.ScaleVector(path[i], scaleFactor);
            
                polyline.AddPoint(point);

                if (i % bubbleFrequency != 0) continue;

                GameObject dotGO = new GameObject("Point " + i);
                dotGO.transform.SetParent(pathGO.transform);
                dotGO.transform.position = point;
                Disc disc = dotGO.AddComponent<Disc>();
                disc.Color = color;
                disc.Radius = 0.5f;
            }
        }

        private class MinMaxValues
        {
            private float _minX;
            private float _maxX;
            private float _minY;
            private float _maxY;

            public MinMaxValues()
            {
                Reset();
            }

            public void Reset()
            {
                _minX = float.PositiveInfinity;
                _minY = float.PositiveInfinity;
                _maxX = float.NegativeInfinity;
                _maxY = float.NegativeInfinity;
            }

            public void Update(Vector2 value)
            {
                _minX = Mathf.Min(_minX, value.x);
                _maxX = Mathf.Max(_maxX, value.x);
                _minY = Mathf.Min(_minY, value.y);
                _maxY = Mathf.Max(_maxY, value.y);
            }

            public void Update(Vector2[] values)
            {
                foreach (var value in values)
                {
                    Update(value);
                }
            }

            public Vector2 ScaleVector(Vector2 vector, float scaleFactor = 1)
            {
                Vector2 minVector = new Vector2(_minX, _minY);
                Vector2 scaler = new Vector2(_maxX - _minX, _maxY - _minY);
                if (scaler.x == 0) scaler.x = 1;
                if (scaler.y == 0) scaler.y = 1;

                return (vector - minVector) / scaler * scaleFactor;
            }

            public Vector2[] ScalePath(Vector2[] path, float scaleFactor = 1)
            {
                for (int i = 0; i < path.Length; i++)
                {
                    path[i] = ScaleVector(path[i], scaleFactor);
                }

                return path;
            }
        }
    }
}