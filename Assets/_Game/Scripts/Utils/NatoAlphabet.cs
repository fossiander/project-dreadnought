﻿namespace _Game.Scripts.Utils
{
    public static class Alphabet
    {
        public static readonly string[] Nato = {
            "Alpha", 
            "Bravo", 
            "Charlie", 
            "Delta", 
            "Echo", 
            "Foxtrot", 
            "Golf", 
            "Hotel", 
            "India", 
            "Juliet", 
            "Kilo", 
            "Lima", 
            "Mike", 
            "November", 
            "Oscar", 
            "Papa", 
            "Quebec", 
            "Romeo", 
            "Sierra", 
            "Tango", 
            "Uniform", 
            "Victor", 
            "Whiskey", 
            "X-ray", 
            "Yankee", 
            "Zulu"
        };
    }
}