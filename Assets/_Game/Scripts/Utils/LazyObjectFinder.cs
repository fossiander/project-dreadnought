﻿using UnityEngine;

namespace _Game.Scripts.Utils
{
    public class LazyObjectFinder<T> where T : Component
    {
        private static T _reference;
        
        public T Get()
        {
            if (_reference == null) Init();

            return _reference;
            
        }

        public void Init()
        {
            if (_reference != null) return;
            
            _reference = Object.FindObjectOfType<T>();
        }
    }
}