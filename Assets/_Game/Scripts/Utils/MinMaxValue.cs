﻿using UnityEngine;

namespace _Game.Scripts.Utils
{
    [System.Serializable]
    public class MinMaxFloat
    {
        public float min;
        public float max;

        public MinMaxFloat(float min, float max)
        {
            this.min = min;
            this.max = max;
        }

        public float GetRandom()
        {
            return Random.Range(min, max);
        }

        public float GetPercentageOfValue(float value)
        {
            if (max - min == 0) return 0;

            return (value - min) / (max - min);
        }

        public float GetValueAtPercentage(float percentage)
        {
            return percentage * (max - min) + min;
        }
    }
}