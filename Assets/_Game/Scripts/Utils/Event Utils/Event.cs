﻿using System;

namespace _Game.Scripts.Utils.Event_Utils
{
    public abstract class Event<T> where T : Event<T>
    {
        private bool _hasBeenTriggered;
        
        private static event Action<T> EventListeners;
        
        public static void AddListener(Action<T> eventListener) {
            EventListeners += eventListener;
        }

        public static void RemoveListener(Action<T> eventListener) {
            EventListeners -= eventListener;
        }

        public void Trigger() {
            if (_hasBeenTriggered) return;
            
            _hasBeenTriggered = true;
            EventListeners?.Invoke(this as T);
        }
    }

    public static class EventSystem
    {
        public static void TriggerEvent<T>(Event<T> eventToBeTriggered) where T : Event<T>
        {
            eventToBeTriggered.Trigger();
        }
    }
}