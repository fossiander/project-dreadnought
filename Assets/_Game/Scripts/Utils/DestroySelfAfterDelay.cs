﻿using System;
using UnityEngine;

namespace _Game.Scripts.Utils
{
    public class DestroySelfAfterDelay : MonoBehaviour
    {
        [SerializeField] private float delay = 0;

        private void Start()
        {
            Destroy(gameObject, delay);
        }
    }
}