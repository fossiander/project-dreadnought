using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RescaleWithCamera : MonoBehaviour
{
    private float _baseOrthographicSize;
    private Camera _mainCamera;

    private void Awake()
    {
        _mainCamera = Camera.main;
        _baseOrthographicSize = _mainCamera.orthographicSize;
    }

    private void LateUpdate()
    {
        transform.localScale = Vector3.one * _mainCamera.orthographicSize / _baseOrthographicSize;
    }
}
